package tests;

import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.ReporterType;

import pages.MDP.CoreNavigation;
import pages.MDP.CoreAPIs;
import pages.MDP.CoreIntroduction;
import pages.login.MDPLoginPage;
import org.testng.ITestResult;
import org.testng.annotations.*;

import utils.Logging;
import utils.PropertyFileReader;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.xml.XmlTest;
import utils.Screenshot;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class BaseTest {

	public WebDriver driver = null;
	public static ExtentReports report;
	public static ExtentTest test;
	public static String timeout;
	public static String username;
	public static String password;
	public String globalPropertyFile = "global.properties";
	public String onboardingDetailsFile = "onboardinguserdetails.properties";
	public static BaseTest base = null;

	@BeforeTest(alwaysRun = true)
	public void beforeClass(XmlTest xmlTest) {
		String date = new SimpleDateFormat("yyyy-MM-dd-hh_mm_ss").format(new Date());
		String reportFilePath = System.getProperty("user.dir") + "\\Reports\\" + xmlTest.getParameter("reportfile")
				+ "_" + date + ".html";
		Logging.info("Test report will be stored in: " + reportFilePath);
		report = new ExtentReports(reportFilePath);
		//test = new ExtentTest("fyl", "ccccccc");
		timeout = xmlTest.getParameter("timeout");
		username = xmlTest.getParameter("username");
		password = xmlTest.getParameter("password");
	}

	@BeforeMethod(alwaysRun = true)
	public void beforeMethod(XmlTest xmlTest, Method method) throws MalformedURLException {
		String exBrowser = xmlTest.getParameter("browser");
		String environment = xmlTest.getParameter("environment");
		String suiteExecutionType = xmlTest.getParameter("suiteExecutionType");
		String hub = xmlTest.getParameter("hub");
		String browser = xmlTest.getParameter("browser");
		String url = PropertyFileReader.getProperty(globalPropertyFile, environment);
		// launchBrowser(exBrowser,url);
		this.setBaseInstance(suiteExecutionType, hub, browser);
		BaseTest.base.startSession(url);
		BaseTest.base.getDriver().manage().window().maximize();
		// get class name of current Test and print it in extent report
		String className = method.getDeclaringClass().toString();
		int index1 = className.lastIndexOf('.') + 1;
		//test = report.startTest(className.substring(index1));

	}

	@AfterMethod(alwaysRun = true)
	public void afterMethod(ITestResult iTestResult) {

		if (iTestResult.getStatus() == ITestResult.FAILURE) {
			// String screenshot = Screenshot.capture(driver, iTestResult.getName());
			// test.log(LogStatus.FAIL, test.addBase64ScreenShot(screenshot));
			test.log(LogStatus.FAIL, iTestResult.getThrowable());
		} else if (iTestResult.getStatus() == ITestResult.SKIP) {
			test.log(LogStatus.SKIP, "Test skipped " + iTestResult.getThrowable());
		} else {
			test.log(LogStatus.PASS, "Test passed");
		}

		BaseTest.base.getDriver().quit();
		Logging.info("Quit the browser");
		report.endTest(test);
		report.flush();

	}

	@AfterTest(alwaysRun = true)
	public void afterTest() {
		report.close();
	}

	private void launchBrowser(String brw, String url) {

		if (brw.equalsIgnoreCase("firefox")) {

			if (System.getProperty("os.name").contains("Mac")) {
				System.setProperty("webdriver.gecko.driver",
						System.getProperty("user.dir") + "/src/test/resources/drivers/geckodriver");
			} else if (System.getProperty("os.name").contains("Windows")) {
				System.setProperty("webdriver.gecko.driver",
						System.getProperty("user.dir") + "\\src\\test\\resources\\drivers\\geckodriver.exe");
			} else {
				Logging.info(
						"Please run firefox either on Mac or Windows operating system. You are trying to run on "
								+ System.getProperty("os.name"));
			}
			driver = new FirefoxDriver();
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		} else if (brw.equalsIgnoreCase("chrome")) {
			Logging.info("Running system is: " + System.getProperty("os.name"));
			if (System.getProperty("os.name").contains("Mac")) {
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "/src/test/resources/drivers/chromedriver");
			} else if (System.getProperty("os.name").contains("Windows")) {
				System.setProperty("webdriver.chrome.driver",
						System.getProperty("user.dir") + "\\src\\test\\resources\\drivers\\chromedriver.exe");
			} else {
				Logging.info(
						"Please run chrome either on Mac or Windows operating system. You are trying to run on "
								+ System.getProperty("os.name"));
			}
			driver = new ChromeDriver();
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		} else if (brw.equalsIgnoreCase("Safari")) {

			if (System.getProperty("os.name").contains("Mac")) {
				driver = new SafariDriver();
				driver.get(url);
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			} else {
				Logging.info("Please run Safari on Mac operating system");
			}

		} else if (brw.equalsIgnoreCase("IE")) {

			if (System.getProperty("os.name").contains("Windows")) {
				System.setProperty("webdriver.ie.driver",
						System.getProperty("user.dir") + "\\src\\test\\resources\\drivers\\IEDriverServer.exe");
				driver = new InternetExplorerDriver();
				driver.get(url);
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			} else {
				Logging.info("Please run IE on Windows operating system. You are trying to run on "
						+ System.getProperty("os.name"));
			}

		} else if (brw.equalsIgnoreCase("Edge")) {
			if (System.getProperty("os.name").contains("Windows")) {
				System.setProperty("webdriver.edge.driver",
						System.getProperty("user.dir") + "\\src\\test\\resources\\drivers\\MicrosoftWebDriver.exe");
				driver = new EdgeDriver();
				driver.get(url);
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			} else {
				Logging.info("Please run Edge on Windows operating system. You are trying to run on "
						+ System.getProperty("os.name"));
			}
		} else {
			Logging.info("Specify Browser name correctly in XML file");
		}
	}

	// #########################################################################
	// Core Page Objects
	// #########################################################################

	public CoreNavigation coreNavigation = new CoreNavigation();
	public CoreAPIs coreAPIs = new CoreAPIs();
	public CoreIntroduction coreIntroduction = new CoreIntroduction();

	public void setBaseInstance(String SuiteExecutionType, String hub, String browser) throws MalformedURLException {
		if (SuiteExecutionType.equalsIgnoreCase("grid")) {
			base = new SeleniumGrid(hub, browser);
		} else {
			base = new SeleniumLocal(browser);
		}
	}

	/*
	 * Starts a WebDriver session. Should be overridden in driver instantiation
	 * classes.
	 */
	protected void startSession(String url) {
		// To be overridden.
	}

	public WebDriver getDriver() {
		return null;
	}
}
