package tests;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Reporter;

import utils.Logging;

public class SeleniumGrid extends BaseTest {

	public static InheritableThreadLocal<RemoteWebDriver> driver = new InheritableThreadLocal<RemoteWebDriver>();
	// public static InheritableThreadLocal<Selenium> selenium = new
	// InheritableThreadLocal<Selenium>();
	private String url;

	public WebDriver getDriver() {
		WebDriver webDriver = driver.get();
		return webDriver;
	}

	/*************************************************************************
	 * @Method Name : SeleniumGrid (constructor)
	 * @Author : Joey Fang
	 * @Created On : 07/30/2018
	 * @Description : This method is to create RemoteWebDriver and Selenium
	 *              instance.
	 * @parameter 1 : browser: Browser name on which suite to be executed e.g
	 *            Firefox
	 * @parameter 2 : url: application URL on which suite to be executed
	 *************************************************************************/
	public SeleniumGrid(String hub, String browser) throws MalformedURLException {

		DesiredCapabilities capability = new DesiredCapabilities();
		capability.setBrowserName(browser);

		if (browser.equalsIgnoreCase("internet explorer")) {
			capability = DesiredCapabilities.internetExplorer();
			capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, false);
			capability.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
			capability.setCapability(InternetExplorerDriver.NATIVE_EVENTS, true);
			capability.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, false);
		}

		if (browser.equalsIgnoreCase("chrome")) {
			// set download path, disable the download popup
			String downloadFilepath = "C:\\Automation";
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("download.default_directory", downloadFilepath);
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--disable-extensions");
			options.setExperimentalOption("prefs", chromePrefs);

			capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capability.setCapability(ChromeOptions.CAPABILITY, options);
		}

		if (browser.equalsIgnoreCase("edge")) {
			capability = DesiredCapabilities.edge();
			capability.setBrowserName("MicrosoftEdge");
		}

		// Throw exception when Css Tracker enalbed on Edge test as it's not supported
		// yet.
		if (browser.equalsIgnoreCase("edge")) {
			throw new IllegalStateException("Css Tracker feature is not supported on Edge test currently.");
		}

		// Use polymorphism to instantiate Remote Webdriver.

		SeleniumGrid.driver.set(new RemoteWebDriver(new URL(hub), capability));

		Logging.info("SeleniumGrid Capabilities is: "+SeleniumGrid.driver.get().getCapabilities());
	}

	@Override
	protected void startSession(String url) {
		// Get and print out WebDriver session info.
		// String ipOfNode = getIPOfNode(SeleniumGrid.driver.get());
		String sessionData = getDriver().toString();
		// Logging.info("STARTED SESSION ON: " + ipOfNode + " using " + sessionData);

		// Navigating to test site.
		// Logging.info("Navigating to: " + url);
		SeleniumGrid.driver.get().get(url);
		driver.get().manage().window().maximize();
	}

}
