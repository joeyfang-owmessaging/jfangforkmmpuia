package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.Reporter;

import utils.Logging;

public class SeleniumLocal extends BaseTest {

	public static WebDriver driver = null;
	public static FirefoxProfile profile;
	private String url;

	public WebDriver getDriver() {
		return driver;
	}

	/*************************************************************************
	 * @Method Name : SeleniumLocal (constructor)
	 * @Author : Joey Fang
	 * @Created On : 07/30/2018
	 * @Description : This method is to create WebDriver and Selenium instance as
	 *              per browser type.
	 * @parameter 1 : browser: Browser name on which suite to be possible values
	 *            are: Firefox, Chrome and internet explorer
	 * @parameter 2 : url: application URL on which suite to be executed
	 *************************************************************************/
	public SeleniumLocal(String browser) {
		if (browser.equalsIgnoreCase("firefox")) {
			driver = new FirefoxDriver();
		} else if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + "\\src\\test\\resources\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browser.equalsIgnoreCase("safari")) {
			driver = new SafariDriver();
		} else {
			driver = new InternetExplorerDriver();
		}
	}

	@Override
	protected void startSession(String url) {
		Logging.info("Navigating to: " + url);
		// Navigating to test site.
		SeleniumLocal.driver.get(url);
	}
}
