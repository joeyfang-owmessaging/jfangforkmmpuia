package tests.MDP.APIs;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;


import pages.login.MDPLoginPage;
import tests.BaseTest;
import utils.Logging;

public class Error_001_ContentVerify extends BaseTest {
	@Test(groups = { "sanity", "Error_001_ContentVerify" })
	public void Error_001_ContentVerify() {

		test = report.startTest("Error_001_ContentVerify");	
		MDPLoginPage loginPage = new MDPLoginPage(driver, test);
		
		loginPage.loginMDPPortal();
		test.log(LogStatus.INFO, "Logged in the MDP portal");

		coreNavigation.clickAPIsTab();
		test.log(LogStatus.INFO, "Clicked the APIs tab");
		
		coreAPIs.clickErrorSection();
		test.log(LogStatus.INFO, "Clicked the Errors subtab");
		
		test.log(LogStatus.INFO, "Prepare to verify the Errors header");
		Assert.assertTrue(coreAPIs.verifyErrorsHeaderLoaded(30));
		test.log(LogStatus.PASS, "The header in Errors displays correctly");

		
		test.log(LogStatus.INFO, "Prepare to verify the warning content in Errors");
		ArrayList<String> errorsWarningExpect = new ArrayList<String>();
		errorsWarningExpect.add("This error section is stored in a separate file in includes /_errors.md. Slate allows you to optionally separate out your docs into many files...just save them to the includes folder and add them to the top of your index.md's frontmatter. Files are included in the order listed.");
		
		ArrayList<String> errorsWarningActual = new ArrayList<String>();
		errorsWarningActual = coreAPIs.getAccountAPIsContentsList("error","warning");

		Assert.assertTrue(coreAPIs.verifyAPIsArrayListContentSame(errorsWarningExpect, errorsWarningActual));
		test.log(LogStatus.PASS, "The warning in Errors display correctly");
		
		test.log(LogStatus.INFO, "Prepare to verify the subheader content in Errors");
		ArrayList<String> errorsSubheaderExpect = new ArrayList<String>();
		errorsSubheaderExpect.add("Account Manager");
		errorsSubheaderExpect.add("createOrganisation:");
		errorsSubheaderExpect.add("publishOrganisation:");
		errorsSubheaderExpect.add("getOrganisationAccounts:");
		errorsSubheaderExpect.add("getOrganisation:");
		
		ArrayList<String> errorsSubheaderActual = new ArrayList<String>();
		errorsSubheaderActual = coreAPIs.getAccountAPIsContentsList("error","subheader");
		
		Assert.assertTrue(coreAPIs.verifyAPIsArrayListContentSame(errorsSubheaderExpect, errorsSubheaderActual));
		test.log(LogStatus.PASS, "The subheader in Errors display correctly");
				
		
		test.log(LogStatus.INFO, "Prepare to verify the values in createOrganisation table");
		ArrayList<String> createOrganisationTableExpect = new ArrayList<String>();
		createOrganisationTableExpect.add("200");
		createOrganisationTableExpect.add("-");
		createOrganisationTableExpect.add("-");
		createOrganisationTableExpect.add("Success");
		
		createOrganisationTableExpect.add("400");
		createOrganisationTableExpect.add("106");
		createOrganisationTableExpect.add("Transition Action not valid");
		createOrganisationTableExpect.add("Transition Action not valid");
		
		createOrganisationTableExpect.add("400");
		createOrganisationTableExpect.add("2000");
		createOrganisationTableExpect.add("Input jason file is missing");
		createOrganisationTableExpect.add("No input json file attached.");
		
		createOrganisationTableExpect.add("500");
		createOrganisationTableExpect.add("101");
		createOrganisationTableExpect.add("Exception message");
		createOrganisationTableExpect.add("FileNotFoundException OrganizationNotFoundException");
		
		createOrganisationTableExpect.add("500");
		createOrganisationTableExpect.add("105");
		createOrganisationTableExpect.add("Exception message");
		createOrganisationTableExpect.add("Any Exception");
		
		ArrayList<String> createOrganisationTableActual = new ArrayList<String>();
		createOrganisationTableActual = coreAPIs.getErrorsCreateOrganisationTable();
		
		Assert.assertTrue(coreAPIs.verifyAPIsArrayListContentSame(createOrganisationTableExpect, createOrganisationTableActual));
		test.log(LogStatus.PASS, "The values in createOrganisation table are correctly");
		
		
		test.log(LogStatus.INFO, "Prepare to verify the values in publishOrganisation table");
		ArrayList<String> publishOrganisationFirstExpect = new ArrayList<String>();
		publishOrganisationFirstExpect.add("200");
		publishOrganisationFirstExpect.add("-");
		publishOrganisationFirstExpect.add("-");
		publishOrganisationFirstExpect.add("Success");
		
		publishOrganisationFirstExpect.add("400");
		publishOrganisationFirstExpect.add("106");
		publishOrganisationFirstExpect.add("Transition Action not valid");
		publishOrganisationFirstExpect.add("Transition Action not valid");
		
		publishOrganisationFirstExpect.add("400");
		publishOrganisationFirstExpect.add("2000");
		publishOrganisationFirstExpect.add("Input jason file is missing");
		publishOrganisationFirstExpect.add("No input json file attached.");
		
		publishOrganisationFirstExpect.add("500");
		publishOrganisationFirstExpect.add("101");
		publishOrganisationFirstExpect.add("Exception message");
		publishOrganisationFirstExpect.add("FileNotFoundException OrganizationNotFoundException");
		
		publishOrganisationFirstExpect.add("500");
		publishOrganisationFirstExpect.add("105");
		publishOrganisationFirstExpect.add("Exception message");
		publishOrganisationFirstExpect.add("Any Exception");
		
		ArrayList<String> publishOrganisationFirstActual = new ArrayList<String>();
		publishOrganisationFirstActual = coreAPIs.getErrorsPublishOrganisationFirstTable();
		
		Assert.assertTrue(coreAPIs.verifyAPIsArrayListContentSame(publishOrganisationFirstExpect, publishOrganisationFirstActual));

		ArrayList<String> publishOrganisationSecondExpect = new ArrayList<String>();
		publishOrganisationSecondExpect.add("200");
		publishOrganisationSecondExpect.add("-");
		publishOrganisationSecondExpect.add("-");
		publishOrganisationSecondExpect.add("Success");
		
		publishOrganisationSecondExpect.add("400");
		publishOrganisationSecondExpect.add("2001");
		publishOrganisationSecondExpect.add("Organisation is not in approved state");
		publishOrganisationSecondExpect.add("Organisation should be approved before publishing");
		
		publishOrganisationSecondExpect.add("400");
		publishOrganisationSecondExpect.add("2002");
		publishOrganisationSecondExpect.add("Organisation Name is required");
		publishOrganisationSecondExpect.add("Organisation Name is required");
		
		publishOrganisationSecondExpect.add("500");
		publishOrganisationSecondExpect.add("105");
		publishOrganisationSecondExpect.add("Exception message");
		publishOrganisationSecondExpect.add("Any Exception");
		
		publishOrganisationSecondExpect.add("400");
		publishOrganisationSecondExpect.add("603");
		publishOrganisationSecondExpect.add("ContentType not supported from WIT");
		publishOrganisationSecondExpect.add("ContentTypeNotSupportedException");
		
		publishOrganisationSecondExpect.add("400");
		publishOrganisationSecondExpect.add("601");
		publishOrganisationSecondExpect.add("ImsUri already exists");
		publishOrganisationSecondExpect.add("ImsUriExistsException");
		
		publishOrganisationSecondExpect.add("400");
		publishOrganisationSecondExpect.add("602");
		publishOrganisationSecondExpect.add("WIT:Bad Request");
		publishOrganisationSecondExpect.add("BadRequestException");
		
		publishOrganisationSecondExpect.add("400");
		publishOrganisationSecondExpect.add("604");
		publishOrganisationSecondExpect.add("WIT NotAuthorizedException");
		publishOrganisationSecondExpect.add("NotAuthorizedException");
		
		publishOrganisationSecondExpect.add("400");
		publishOrganisationSecondExpect.add("2005");
		publishOrganisationSecondExpect.add("No Organisation found");
		publishOrganisationSecondExpect.add("When no oganisation exists for a given name");
		
		ArrayList<String> publishOrganisationSecondActual = new ArrayList<String>();
		publishOrganisationSecondActual = coreAPIs.getErrorsPublishOrganisationSecondTable();
		
		Assert.assertTrue(coreAPIs.verifyAPIsArrayListContentSame(publishOrganisationSecondExpect, publishOrganisationSecondActual));
		test.log(LogStatus.PASS, "The values in publishOrganisation table are correctly");
		
		
		test.log(LogStatus.INFO, "Prepare to verify the values in getOrganisationAccounts table");
		ArrayList<String> getOrganisationAccountsTableExpect = new ArrayList<String>();
		getOrganisationAccountsTableExpect.add("200");
		getOrganisationAccountsTableExpect.add("-");
		getOrganisationAccountsTableExpect.add("-");
		getOrganisationAccountsTableExpect.add("Success");
		
		getOrganisationAccountsTableExpect.add("404");
		getOrganisationAccountsTableExpect.add("2011");
		getOrganisationAccountsTableExpect.add("No accounts found for the organisation id");
		getOrganisationAccountsTableExpect.add("When organisation has no accounts");
		
		getOrganisationAccountsTableExpect.add("500");
		getOrganisationAccountsTableExpect.add("105");
		getOrganisationAccountsTableExpect.add("Exception message");
		getOrganisationAccountsTableExpect.add("Any Exception");
				
		ArrayList<String> getOrganisationAccountsTableActual = new ArrayList<String>();
		getOrganisationAccountsTableActual = coreAPIs.getErrorsGetOrganisationAccountsTable();
		
		Assert.assertTrue(coreAPIs.verifyAPIsArrayListContentSame(getOrganisationAccountsTableExpect, getOrganisationAccountsTableActual));
		test.log(LogStatus.PASS, "The values in getOrganisationAccounts table are correctly");
		
		
		test.log(LogStatus.INFO, "Prepare to verify the values in getOrganisation table");
		ArrayList<String> getOrganisationTableExpect = new ArrayList<String>();
		getOrganisationTableExpect.add("200");
		getOrganisationTableExpect.add("-");
		getOrganisationTableExpect.add("-");
		getOrganisationTableExpect.add("Success");
		
		getOrganisationTableExpect.add("500");
		getOrganisationTableExpect.add("105");
		getOrganisationTableExpect.add("Exception message");
		getOrganisationTableExpect.add("Any Exception");
		
		getOrganisationTableExpect.add("404");
		getOrganisationTableExpect.add("2011");
		getOrganisationTableExpect.add("No Organisation found");
		getOrganisationTableExpect.add("-");
				
		ArrayList<String> getOrganisationTableActual = new ArrayList<String>();
		getOrganisationTableActual = coreAPIs.getErrorsGetOrganisationTable();
		
		Assert.assertTrue(coreAPIs.verifyAPIsArrayListContentSame(getOrganisationTableExpect, getOrganisationTableActual));
		test.log(LogStatus.PASS, "The values in getOrganisation table are correctly");
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
