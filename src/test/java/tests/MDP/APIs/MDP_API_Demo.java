package tests.MDP.APIs;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import pages.login.MDPLoginPage;
import tests.BaseTest;

public class MDP_API_Demo extends BaseTest {
	@Test(groups = { "sanity", "MDP_API_Demo" })
	public void MDP_API_Demo() {

		test = report.startTest("MDP_API_Demo");
		
		MDPLoginPage loginPage = new MDPLoginPage(driver, test);

		loginPage.loginMDPPortal();

		coreNavigation.clickAPIsTab();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		coreAPIs.clickAuthenticationSection();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		coreAPIs.clickAccountMangerSection();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		coreAPIs.clickCampaignManagerSection();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		coreAPIs.clickCouponsSection();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		coreAPIs.clickSurveysSection();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		coreAPIs.clickErrorSection();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		coreAPIs.scrollToAPIsTopPage();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		coreAPIs.scrollToAPIsBottomPage();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		coreAPIs.clickAccountMangerSection();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Assert.assertTrue(coreAPIs.verifyAccountManagerHeaderLoaded(30));

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		test.log(LogStatus.PASS, "MDP API Demo successfully");
	}

}
