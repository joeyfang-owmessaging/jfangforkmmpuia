package tests.MDP.APIs;

import java.util.ArrayList;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import pages.login.MDPLoginPage;
import tests.BaseTest;

public class Authentication_003_LongScroll extends BaseTest {
	@Test(groups = { "sanity", "Authentication_003_LongScroll" })
	public void Authentication_003_LongScroll() {
		test = report.startTest("Authentication_003_LongScroll");

		MDPLoginPage loginPage = new MDPLoginPage(driver, test);

		loginPage.loginMDPPortal();
		test.log(LogStatus.INFO, "Logged in the MDP portal");

		coreNavigation.clickAPIsTab();
		test.log(LogStatus.INFO, "Clicked the APIs tab");
		coreAPIs.clickAuthenticationSection();
		test.log(LogStatus.INFO, "Clicked the Authentication subtab");

		coreAPIs.scrollToAPIsBottomPage();
		test.log(LogStatus.INFO, "Scrolled to the bottom of the APIs page");
		
		test.log(LogStatus.INFO, "Prepare to verify if bottom page elements display");
		Assert.assertTrue(coreAPIs.verifyAPIsPageBottomElementDisplay());
		test.log(LogStatus.PASS, "The bottom page elements display correctly");
		

		coreAPIs.scrollToAPIsTopPage();
		test.log(LogStatus.INFO, "Scrolled to the top of the APIs page");
		
		test.log(LogStatus.INFO, "Prepare to verify if top page elements display");
		Assert.assertTrue(coreAPIs.verifyAPIsPageTopElementDisplay());
		test.log(LogStatus.PASS, "The top page elements display correctly");
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
