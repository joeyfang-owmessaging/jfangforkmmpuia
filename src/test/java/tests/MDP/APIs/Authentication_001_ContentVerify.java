package tests.MDP.APIs;

import java.util.ArrayList;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import pages.login.MDPLoginPage;
import tests.BaseTest;
import utils.Logging;

public class Authentication_001_ContentVerify extends BaseTest {
	@Test(groups = { "sanity", "Authentication_001_ContentVerify" })
	public void Authentication_001_ContentVerify() {

		ArrayList<String> authenticationParagraphExpect = new ArrayList<String>();
		authenticationParagraphExpect
				.add("Kittn uses API keys to allow access to the API. You can register a new Kittn API key at our");
		authenticationParagraphExpect.add(
				"Kittn expects for the API key to be included in all API requests to the server in a header that looks like the following:");

		for (int i = 0; i < 2; i++) {
			Logging.info("Array list for headerExpect is: " + authenticationParagraphExpect.get(i));
		}

		String authenticationMessage = "Authorization: meowmeowmeow";
		String authenticationWarning = "You must replace meowmeowmeow with your personal API key.";

		test = report.startTest("Authentication_001_ContentVerify");

		MDPLoginPage loginPage = new MDPLoginPage(driver, test);

		loginPage.loginMDPPortal();

		coreNavigation.clickAPIsTab();

		ArrayList<String> authenticationParagraphActual = new ArrayList<String>();

		test.log(LogStatus.INFO, "Prepare to verify the Authentication paragraph content");
		for (int i = 2; i <= 3; i++) {
			authenticationParagraphActual.add(i - 2, coreAPIs.getAuthenticationParagraphContent(String.valueOf(i)));
			Logging.info(
					"Array list authenticationParagraphActual is: " + authenticationParagraphActual.get(i - 2));
		}
		for (int i = 0; i <= 1; i++) {
			Assert.assertTrue(coreAPIs.verifyAuthenticationContainsContent(authenticationParagraphExpect.get(i),
					authenticationParagraphActual.get(i)));

		}
		test.log(LogStatus.PASS, "The Authentication paragraph content verified pass");

		test.log(LogStatus.INFO, "Prepare to verify the Authentication message content");
		Assert.assertTrue(coreAPIs.verifyAuthenticationContainsContent(authenticationMessage,
				coreAPIs.getAuthenticationMessageContent()));
		test.log(LogStatus.PASS, "The Authentication paragraph content message pass");

		test.log(LogStatus.INFO, "Prepare to verify the Authentication warning content");
		Assert.assertTrue(coreAPIs.verifyAuthenticationContainsContent(authenticationWarning,
				coreAPIs.getAuthenticationWarningContent()));
		test.log(LogStatus.PASS, "The Authentication warning content verified pass");
	}

}