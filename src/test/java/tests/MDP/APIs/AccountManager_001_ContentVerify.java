package tests.MDP.APIs;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import pages.login.MDPLoginPage;
import tests.BaseTest;

public class AccountManager_001_ContentVerify extends BaseTest {
	@Test(groups = { "sanity", "AccountManager_001_ContentVerify" })
	public void AccountManager_001_ContentVerify() {

		test = report.startTest("AccountManager_001_PageInfoDisplay");
		
		MDPLoginPage loginPage = new MDPLoginPage(driver, test);
		
		
		ArrayList<String> accountManagerSubheaderExpect = new ArrayList<String>();
		accountManagerSubheaderExpect.add("Get All Kittens");
		accountManagerSubheaderExpect.add("HTTP Request");
		accountManagerSubheaderExpect.add("Query Parameters");
		accountManagerSubheaderExpect.add("HTTP Request");
		accountManagerSubheaderExpect.add("URL Parameters");

		ArrayList<String> accountManagerMessageExpect = new ArrayList<String>();
		accountManagerMessageExpect.add("GET http://example.com/api/kittens");
		accountManagerMessageExpect.add("GET http://example.com/kittens/<ID> </ID>");
		
		ArrayList<String> accountManagerWarningExpect = new ArrayList<String>();
		accountManagerWarningExpect.add("Remember — a happy kitten is an authenticated kitten!");
		accountManagerWarningExpect.add("Inside HTML code blocks like this one, you can't use Markdown, so use <code> blocks to denote code.");

		ArrayList<String> QueryParametersTableExpect = new ArrayList<String>();
		QueryParametersTableExpect.add("include_cats");
		QueryParametersTableExpect.add("false");
		QueryParametersTableExpect.add("If set to true, the result will also include cats.");
		QueryParametersTableExpect.add("available");
		QueryParametersTableExpect.add("true");
		QueryParametersTableExpect.add("If set to false, the result will include kittens that have already been adopted.");
		 
		ArrayList<String> URLParametersTableExpect = new ArrayList<String>();
		URLParametersTableExpect.add("ID");
		URLParametersTableExpect.add("The ID of the kitten to retrieve");		
		
		loginPage.loginMDPPortal();

		coreNavigation.clickAPIsTab();
		coreAPIs.clickAccountMangerSection();
		
		test.log(LogStatus.INFO, "Prepare to verify the Account Manager header");
		Assert.assertTrue(coreAPIs.verifyAccountManagerHeaderLoaded(30));
		test.log(LogStatus.PASS, "The header in Account Management displays correctly");

		
		ArrayList<String> accountManagerSubheaderActual = new ArrayList<String>();
		accountManagerSubheaderActual = coreAPIs.getAccountAPIsContentsList("accountManager","subheader");
		
		test.log(LogStatus.INFO, "Prepare to verify the subheader content in Account Manager");
		Assert.assertTrue(coreAPIs.verifyAPIsArrayListContentSame(accountManagerSubheaderExpect, accountManagerSubheaderActual));
		test.log(LogStatus.PASS, "The subheader in Account Manager display correctly");
		
		ArrayList<String> accountManagerMessageActual = new ArrayList<String>();
		accountManagerMessageActual = coreAPIs.getAccountAPIsContentsList("accountManager","message");
		
		test.log(LogStatus.INFO, "Prepare to verify the message content in Account Manager");
		Assert.assertTrue(coreAPIs.verifyAPIsArrayListContentSame(accountManagerMessageExpect, accountManagerMessageActual));
		test.log(LogStatus.PASS, "The message in Account Manager display correctly");
		
		
		ArrayList<String> accountManagerWarningActual = new ArrayList<String>();
		accountManagerWarningActual = coreAPIs.getAccountAPIsContentsList("accountManager","warning");
		
		test.log(LogStatus.INFO, "Prepare to verify the warning content in Account Manager");
		Assert.assertTrue(coreAPIs.verifyAPIsArrayListContentSame(accountManagerWarningExpect, accountManagerWarningActual));
		test.log(LogStatus.PASS, "The warning in Account Manager display correctly");
		
		
		ArrayList<String> QueryParametersTableActual = new ArrayList<String>();
		QueryParametersTableActual = coreAPIs.getAccountManagerQueryParametersTable();
		
		test.log(LogStatus.INFO, "Prepare to verify the values in Query Parameters table");
		Assert.assertTrue(coreAPIs.verifyAPIsArrayListContentSame(QueryParametersTableExpect, QueryParametersTableActual));
		test.log(LogStatus.PASS, "The values in Query Parameters table are correctly");
		
		
		ArrayList<String> URLParametersTableActual = new ArrayList<String>();
		URLParametersTableActual = coreAPIs.getAccountManagerURLParametersTable();
		
		test.log(LogStatus.INFO, "Prepare to verify the values in URL Parameters table");
		Assert.assertTrue(coreAPIs.verifyAPIsArrayListContentSame(URLParametersTableExpect, URLParametersTableActual));
		test.log(LogStatus.PASS, "The values in URL Parameters table are correctly");
		
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
