package tests.MDP.APIs;

import java.util.ArrayList;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import pages.login.MDPLoginPage;
import tests.BaseTest;

public class Authentication_002_CodeTypeVerify extends BaseTest {
	@Test(groups = { "sanity", "Authentication_002_CodeTypeVerify" })
	public void Authentication_002_CodeTypeVerify() {
		test = report.startTest("Authentication_002_CodeTypeVerify");

		MDPLoginPage loginPage = new MDPLoginPage(driver, test);

		loginPage.loginMDPPortal();
		test.log(LogStatus.INFO, "Logged in the MDP portal");

		coreNavigation.clickAPIsTab();
		test.log(LogStatus.INFO, "Clicked the APIs tab");
		coreAPIs.clickAuthenticationSection();
		test.log(LogStatus.INFO, "Clicked the Authentication subtab");

		test.log(LogStatus.INFO, "Prepare to click the Java code type");
		coreAPIs.clickAuthenticationJavaType();
		test.log(LogStatus.PASS, "Clicked the Java code type");

		test.log(LogStatus.INFO, "Prepare to click the NodeJS code type");
		coreAPIs.clickAuthenticationNodeJSType();
		test.log(LogStatus.PASS, "Clicked the NodeJS code type");

		test.log(LogStatus.INFO, "Prepare to click the Shell code type");
		coreAPIs.clickAuthenticationShellType();
		test.log(LogStatus.PASS, "Clicked the Shell code type");

		test.log(LogStatus.INFO, "Prepare to verify the Shell code type header");
		ArrayList<String> shellTypeHeaderExpect = new ArrayList<String>();
		shellTypeHeaderExpect.add("To authorize, use this code:");
		shellTypeHeaderExpect.add("Make sure to replace meowmeowmeow with your API key.");

		ArrayList<String> shellTypeHeaderActual = new ArrayList<String>();
		shellTypeHeaderActual = coreAPIs.getAuthenticationCodeTypeContentsList("header");

		Assert.assertTrue(coreAPIs.verifyAPIsArrayListContentSame(shellTypeHeaderExpect, shellTypeHeaderActual));
		test.log(LogStatus.PASS, "The Shell code type header is correctly");

		test.log(LogStatus.INFO, "Prepare to verify the Shell code type code");
		ArrayList<String> shellTypeCodeExpect = new ArrayList<String>();
		shellTypeCodeExpect.add("const kittn = require('kittn'); let api = kittn.authorize('meowmeowmeow');");

		ArrayList<String> shellTypeCodeActual = new ArrayList<String>();
		shellTypeCodeActual = coreAPIs.getAuthenticationCodeTypeContentsList("code");

		Assert.assertTrue(coreAPIs.verifyAPIsArrayListContentSame(shellTypeCodeExpect, shellTypeCodeActual));
		test.log(LogStatus.PASS, "The Shell code type code is correctly");

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
