package tests.MDP.login;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import pages.login.MDPLoginPage;
import tests.BaseTest;

public class Login_001_VerifyLoginScreenUI extends BaseTest {
	@Test(groups = { "sanity", "Login_001_VerifyLoginScreenUI" })
	public void Login_001_VerifyLoginScreenUI() {

		test = report.startTest("Login_001_VerifyLoginScreenUI");
				
		MDPLoginPage loginPage = new MDPLoginPage(driver, test);				

		test.log(LogStatus.INFO, "Prepare to verify the login page");
		Assert.assertTrue(loginPage.verifyMDPLoginPageLoaded(300));		
		test.log(LogStatus.PASS, "The login page loaded successfully");
		
/*		Assert.assertTrue(1==2);	
		test.log(LogStatus.FAIL,"shoudl fail");*/

	}

}
