package tests.MDP.login;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import pages.login.MDPLoginPage;
import tests.BaseTest;

public class Login_002_LoginWithCorrectCredential extends BaseTest {
	@Test(groups = { "sanity", "Login_002_LoginWithCorrectCredential" })
	public void Login_002_LoginWithCorrectCredential() {

		test = report.startTest("Login_002_LoginWithCorrectCredential");
		
		MDPLoginPage loginPage = new MDPLoginPage(driver, test);

		if (loginPage.verifyMDPLoginPageLoaded(300)) {
			loginPage.userLogin();
		}
		Assert.assertTrue(loginPage.verifyPortalSwitchPageLoaded(30));
		loginPage.selectAccount("0");

		Assert.assertTrue(loginPage.verifyMDPHomePageLoaded(60));
		
		test.log(LogStatus.PASS, "Login successfully");
		
		test.log(LogStatus.INFO, "Prepare to verify the User ID in MDP Home page");
		Assert.assertTrue(coreNavigation.verifyUserIDContains("@KFXGYIGM"));
		test.log(LogStatus.PASS, "Verified User ID in MDP Home page pass");
		
		test.log(LogStatus.INFO, "Prepare to verify the Subscribers in MDP Home page");
		Assert.assertTrue(coreNavigation.verifySubscribersContains("0 subscribers"));
		test.log(LogStatus.PASS, "Verified Subscribers in MDP Home page pass");
		
		test.log(LogStatus.INFO, "Prepare to verify the Plan in MDP Home page");
		Assert.assertTrue(coreNavigation.verifyMDPHomePlanLoaded(30));
		test.log(LogStatus.PASS, "Verified Plan in MDP Home page pass");
		
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
