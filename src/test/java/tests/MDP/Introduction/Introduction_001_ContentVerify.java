package tests.MDP.Introduction;

import java.util.ArrayList;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;
import pages.login.MDPLoginPage;
import tests.BaseTest;
import utils.Logging;

public class Introduction_001_ContentVerify extends BaseTest {
	@Test(groups = { "sanity", "Introduction_001_ContentVerify" })
	public void Introduction_001_ContentVerifyextends() {
		ArrayList<String> headerExpect = new ArrayList<String>();
		headerExpect.add("Overview of the MMP platform");
		headerExpect.add("Account types");
		headerExpect.add("There are three account types:");
		headerExpect.add("Plan types");
		headerExpect.add("Account management console");
		headerExpect.add("Brand Profile Page");
		headerExpect.add("Coupons");
		headerExpect.add("Surveys");
		headerExpect.add("Campaigns");
		headerExpect.add("Chatbots");
		headerExpect.add("Quick replies");
		headerExpect.add("1:1 messages");
		headerExpect.add("Analytics");

		for (int i = 0; i < 13; i++) {
			Logging.info("Array list for headerExpect is: " + headerExpect.get(i));
		}

		ArrayList<String> paragraphExpect = new ArrayList<String>();

		paragraphExpect.add(
				"Welcome to the MMP API! You can use our API to access endpoints, which can get information on various item in our database. We have language bindings in Java an Nodejs.");
		paragraphExpect.add(
				"The MMP Platform is about messaging as a platform. It allows brands to communicate with subscribers via RCS message applications. The brands can create and send campaign messages to end users. The MMP Platform allows brands to send out coupons and surveys, as well as integrating chatbots and 1:1 messaging between subscribers and individuals working for the brand");
		paragraphExpect.add(
				"Standard accounts, which can be opened by any small to medium business (SMB). Standard accounts have fewer than 100,000 subscribers, and fewer targeting options; Official accounts, which have a higher level of validation to confirm that the account holder is who they claim to be. Official accounts can have a larger number of subscribers than stand- ard accounts; and A2P accounts, which are used by aggregators");
		paragraphExpect.add(
				"Plans control what a user can do on the system. For example a plan may allow for a maximum number of subscribers or a maximum number of API calls per unit time. The number of plan types is configurable. The white-label version of the platform has three plan types: Free, Basic, and Pro.");
		paragraphExpect.add(
				"The Account Management Console is the GUI that is used to create and manage the content for an account. The different types of content are outlined in the following sections.");
		paragraphExpect.add(
				"The brand page is essentially the contact page for the brand. An end user can become a subscriber by using the brand page.");
		paragraphExpect.add(
				"MMP coupons act as electronic versions of normal coupons that offer money off or other special offers. They are based on RCS rich cards.");
		paragraphExpect.add(
				"MMP can send out surveys. Rather than being based on HTML pages, surveys use chatbots to provide a rich and interactive experience to the end user.");
		paragraphExpect.add(
				"A campaign is used to actually send content, such as coupons and surveys, to subscribers. Content is attached to a campaign and the campaign then sends the content out to the targeted subscribers.");
		paragraphExpect.add(
				"A chatbot is an automatic replier that receives text chat or media input from an end user and uses logic to generate a response. The response is in the form of an RCS message. Chatbots can be configured in an SDK or through the account management console.");
		paragraphExpect.add(
				"Quick replies are short strings of text that appear on the client to help the end user respond more quickly by tapping a button rather than entering the full text. Although they appear on the client side, they are configured through the account management console.");
		paragraphExpect.add(
				"The MMP system can send and receive messages between subscribers and users of the system, such as business owners. This is referred to as 1:1 messaging. It can be done through the account management console or via an API.");
		paragraphExpect.add("The analytics page of the account management console provides some simple metrics.");
		for (int i = 0; i < 13; i++) {
			Logging.info("Array list for paragraphExpect is: " + paragraphExpect.get(i));
		}

		test = report.startTest("Introduction_001_ContentVerifyextends");

		MDPLoginPage loginPage = new MDPLoginPage(driver, test);

		loginPage.loginMDPPortal();

		ArrayList<String> headerActual = new ArrayList<String>();

		test.log(LogStatus.INFO, "Prepare to verify the Introduction header content");
		for (int i = 1; i <= 13; i++) {
			headerActual.add(coreIntroduction.getIntroductionHeaderContent(String.valueOf(i)));
			Logging.info("ARY headerActual is: " + headerActual.get(i - 1));
			Logging.info("ARY headerExpect is: " + headerExpect.get(i - 1));
			Assert.assertTrue(coreIntroduction.verifyIntroductionContainsContent(headerExpect.get(i - 1),
					headerActual.get(i - 1)));
		}
		test.log(LogStatus.PASS, "The Introduction header content verified pass");

		ArrayList<String> paragraphActual = new ArrayList<String>();

		test.log(LogStatus.INFO, "Prepare to verify the Introduction paragraph content");
		for (int i = 1; i <= 13; i++) {
			paragraphActual.add(coreIntroduction.getIntroductionParagraphContent(String.valueOf(i)));
			Logging.info("ARY paragraphActual is: " + paragraphActual.get(i - 1));
			Logging.info("ARY paragraphExpect is: " + paragraphExpect.get(i - 1));
			Assert.assertTrue(coreIntroduction.verifyIntroductionContainsContent(paragraphExpect.get(i - 1),
					paragraphActual.get(i - 1)));
		}
		test.log(LogStatus.PASS, "The Introduction paragraph content verified pass");

	}

}
