package tests.onboardingflow.mmp87;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;
import utils.PropertyFileReader;

public class MMP87_VerifyPrefectureDropdown extends BaseTest {

    @Test
    public void verifyPrefectureDropdown_mmp359() {

        String email = PropertyFileReader.getProperty(onboardingDetailsFile,"emailid");
        String password = PropertyFileReader.getProperty(onboardingDetailsFile,"password");
        String prefecture = PropertyFileReader.getProperty(onboardingDetailsFile,"prefecture");
        int errors = 0;

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver, test);

        loginDetailsPage.submitLoginDetails(email, password);
        test.log(LogStatus.INFO, "Entered login details and navigated to Account Details page");

        accountDetailsPage.clickContinueBtn();
        test.log(LogStatus.INFO, "Clicked on Continue button without selecting Prefecture dropdown");

        if(accountDetailsPage.prefectureErrorDisplayed()) {
            test.log(LogStatus.PASS, "Error message displayed for Prefecture dropdown");
        } else {
            test.log(LogStatus.FAIL, "Error message not displayed for Prefecture dropdown");
            errors++;
        }

        accountDetailsPage.selectDropdown(driver, "Prefecture", prefecture);
        test.log(LogStatus.INFO,"Selected Prefecture dropdown value");

        accountDetailsPage.clickContinueBtn();

        if(accountDetailsPage.prefectureErrorDisplayed()) {
            test.log(LogStatus.FAIL, "Error message displayed even after selecting Prefecture dropdown");
            errors++;
        } else {
            test.log(LogStatus.PASS, "Error message not displayed after selecting Prefecture dropdown");
        }

        if(errors == 0) {
            test.log(LogStatus.PASS, "TestCase Passed: Error message validation is proper");

        } else {
            test.log(LogStatus.FAIL, "TestCase Failed: Error message validation is not proper");
            Assert.fail("TestCase Failed: Error message validation is not proper");
        }
    }
}

