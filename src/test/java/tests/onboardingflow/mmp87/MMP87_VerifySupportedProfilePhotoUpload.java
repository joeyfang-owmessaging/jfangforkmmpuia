package tests.onboardingflow.mmp87;

import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.LoginDetailsPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;
import utils.PropertyFileReader;

import java.awt.*;

public class MMP87_VerifySupportedProfilePhotoUpload extends BaseTest {

    @Test
    public void verifyUploadProfilePhoto_mmp355() throws AWTException {

        String email = PropertyFileReader.getProperty(onboardingDetailsFile,"emailid");
        String password = PropertyFileReader.getProperty(onboardingDetailsFile,"password");
        String jpgImagePath=PropertyFileReader.getProperty(onboardingDetailsFile,"image.jpg");
        String pngImagePath=PropertyFileReader.getProperty(onboardingDetailsFile,"image.png");
        int errors = 0;

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver, test);

        loginDetailsPage.submitLoginDetails(email, password);
        test.log(LogStatus.INFO, "Navigated to organization page");

        accountDetailsPage.uploadFile(System.getProperty("user.dir") + jpgImagePath);

        if(accountDetailsPage.cropButtonPresent()) {
            test.log(LogStatus.PASS, ".JPG photo selected for upload");
        } else {
            test.log(LogStatus.FAIL, ".JPG photo not selected for upload");
            errors++;
        }

        accountDetailsPage.cropPhoto();

        accountDetailsPage.uploadFile(System.getProperty("user.dir") + pngImagePath);

        if(accountDetailsPage.cropButtonPresent()) {
            test.log(LogStatus.PASS, ".PNG photo selected for upload");
        } else {
            test.log(LogStatus.FAIL, ".PNG photo not selected for upload");
            errors++;
        }

        if(errors > 0) {
            Assert.fail("uploading failed for supported formats");
        }

    }

}
