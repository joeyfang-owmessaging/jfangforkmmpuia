package tests.onboardingflow.mmp87;

import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.LoginDetailsPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;
import utils.PropertyFileReader;

import java.awt.*;
import java.util.Set;

public class MMP87_VerifyTermsAndConditionsLink extends BaseTest {

    @Test
    public void verifyTermsAndConditionsLinks_mmp365() throws AWTException, InterruptedException {

        String email = PropertyFileReader.getProperty(onboardingDetailsFile,"emailid");
        String password = PropertyFileReader.getProperty(onboardingDetailsFile,"password");

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver, test);

        loginDetailsPage.submitLoginDetails(email,password);
        test.log(LogStatus.INFO, "Submitted Login Details");

        accountDetailsPage.clickTermsAndConditions();
        test.log(LogStatus.INFO, "Clicked Terms and Conditions link on Business Details page");

        Set<String> allWh = driver.getWindowHandles();

        if(allWh.size()==2) {
            for(String wh : allWh) {
                driver.switchTo().window(wh);
            }
            if(driver.getTitle().equals("Google Terms of Service – Privacy & Terms – Google")) {
                test.log(LogStatus.PASS, "Terms and Conditions URL is opened in new tab");
            } else {
                test.log(LogStatus.FAIL, "New tab opened but Terms and Conditions URL is not opened");
                Assert.fail();
            }
        }else {
            test.log(LogStatus.FAIL,"New tab is not opened when clicked on T&C Link");
        }
    }

}