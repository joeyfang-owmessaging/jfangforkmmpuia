package tests.onboardingflow.mmp87;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;
import utils.PropertyFileReader;

public class MMP87_VerifyPostalCodeField extends BaseTest {

    @Test
    public void verifyYourNameTextField_mmp358() {

        String email = PropertyFileReader.getProperty(onboardingDetailsFile,"emailid");
        String password = PropertyFileReader.getProperty(onboardingDetailsFile,"password");
        String postalCode = PropertyFileReader.getProperty(onboardingDetailsFile,"postalcode");
        int errors = 0;

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver, test);

        loginDetailsPage.submitLoginDetails(email, password);
        test.log(LogStatus.INFO, "Entered login details and navigated to Account Details page");

        accountDetailsPage.clickContinueBtn();
        test.log(LogStatus.INFO, "Clicked on Continue button without entering Postal code field");

        if(accountDetailsPage.postalCodeErrorDisplayed()) {
            test.log(LogStatus.PASS, "Error message displayed for Postal code field");
        } else {
            test.log(LogStatus.FAIL, "Error message not displayed for Postal code field");
            errors++;
        }

        accountDetailsPage.enterPostalCode(postalCode);
        test.log(LogStatus.INFO,"Entered Postal code");

        accountDetailsPage.clickContinueBtn();

        if(accountDetailsPage.postalCodeErrorDisplayed()) {
            test.log(LogStatus.FAIL, "Error message displayed even after entering Postal code field");
            errors++;
        } else {
            test.log(LogStatus.PASS, "Error message not displayed after entering Postal code field");
        }

        if (errors == 0) {
            test.log(LogStatus.PASS, "TestCase Passed: Error message validation is proper");
        } else {
            test.log(LogStatus.FAIL, "TestCase Failed: Error message validation is not proper");
            Assert.fail("TestCase Failed: Error message validation is not proper");
        }
    }
}