package tests.onboardingflow.mmp87;

import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.LoginDetailsPage;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;
import utils.PropertyFileReader;

import java.awt.*;

public class MMP87_ContinueWithoutAgreeTC extends BaseTest {

    @Test()
    public void continueWithoutAgreeTC_mmp366() throws AWTException{

        String emailid = PropertyFileReader.getProperty(onboardingDetailsFile,"emailid");
        String password = PropertyFileReader.getProperty(onboardingDetailsFile,"password");

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver, test);

        loginDetailsPage.submitLoginDetails(emailid, password);
        test.log(LogStatus.INFO, "Submitted Login Details");

        accountDetailsPage.enterYourName(PropertyFileReader.getProperty(onboardingDetailsFile,"name"));
        test.log(LogStatus.INFO, "Entered Name");

        accountDetailsPage.enterBusinessName(PropertyFileReader.getProperty(onboardingDetailsFile,"business"));
        test.log(LogStatus.INFO, "Entered Business Name");

        accountDetailsPage.selectDropdown(driver,"Business Category", PropertyFileReader.getProperty(onboardingDetailsFile,"category"));
        test.log(LogStatus.INFO, "Selected Business Category");

        accountDetailsPage.uploadFile(System.getProperty("user.dir") + PropertyFileReader.getProperty(onboardingDetailsFile,"image.jpg"));
        test.log(LogStatus.INFO, "image selected for upload");

        accountDetailsPage.cropPhoto();
        test.log(LogStatus.INFO, "Cropped selected photo");

        accountDetailsPage.enterPostalCode(PropertyFileReader.getProperty(onboardingDetailsFile,"postalcode"));
        test.log(LogStatus.INFO, "Entered Postal code");

        accountDetailsPage.selectDropdown(driver,"Prefecture",PropertyFileReader.getProperty(onboardingDetailsFile,"prefecture"));
        test.log(LogStatus.INFO, "Selected Prefecture value");

        accountDetailsPage.enterCity(PropertyFileReader.getProperty(onboardingDetailsFile,"city"));
        test.log(LogStatus.INFO, "Entered City");

        accountDetailsPage.enterAddress(PropertyFileReader.getProperty(onboardingDetailsFile,"address"));
        test.log(LogStatus.INFO, "Entered Address");

        accountDetailsPage.enterDescription(PropertyFileReader.getProperty(onboardingDetailsFile,"description"));
        test.log(LogStatus.INFO, "Entered Description");

        accountDetailsPage.enterPhoneNumber(PropertyFileReader.getProperty(onboardingDetailsFile,"phonenumber"));
        test.log(LogStatus.INFO, "Entered Phone number");

        accountDetailsPage.enterUrl(PropertyFileReader.getProperty(onboardingDetailsFile,"website"));
        test.log(LogStatus.INFO, "Entered Business Website URL");

        accountDetailsPage.clickContinueBtn();
        test.log(LogStatus.INFO, "Clicked Continue button without agreeing to Terms and Conditions");

        if(accountDetailsPage.isTCErrorMessageDisplayed()){
            test.log(LogStatus.PASS,"Test passed: Error Message Displayed to accept Terms and Conditions");

        } else{
            test.log(LogStatus.FAIL,"Test failed: Error Message to accept Terms and Conditions not Displayed");
            Assert.fail();
        }
    }
}
