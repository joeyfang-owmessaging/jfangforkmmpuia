package tests.onboardingflow.mmp87;

import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.LoginDetailsPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;
import utils.PropertyFileReader;

import java.awt.*;

public class MMP87_VerifyUnsupportedProfilePhotoUpload extends BaseTest {

    @Test
    public void verifyUnsupportedProfilePhotoUpload_mmp356() throws AWTException {

        String email = PropertyFileReader.getProperty(onboardingDetailsFile,"emailid");
        String password = PropertyFileReader.getProperty(onboardingDetailsFile,"password");

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver, test);

        loginDetailsPage.submitLoginDetails(email, password);
        test.log(LogStatus.INFO, "Entered Login details and navigated to Organization Details page");

        accountDetailsPage.uploadFile(System.getProperty("user.dir") + PropertyFileReader.getProperty(onboardingDetailsFile,"image.tif"));
        test.log(LogStatus.INFO, "Selected .TIF format image for upload");

        if(accountDetailsPage.formatErrorPresent()) {

            test.log(LogStatus.PASS, "Error Message displayed when .TIF photo uploaded");
        } else {
            test.log(LogStatus.FAIL, "Error Message not displayed when .TIF photo uploaded");
            Assert.fail("Error Message not displayed when .TIF photo uploaded");
        }

    }
}
