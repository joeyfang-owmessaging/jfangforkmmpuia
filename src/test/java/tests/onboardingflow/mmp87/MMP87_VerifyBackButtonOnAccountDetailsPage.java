package tests.onboardingflow.mmp87;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;
import utils.PropertyFileReader;

public class MMP87_VerifyBackButtonOnAccountDetailsPage extends BaseTest {

    @Test
    public void verifyBackButtonOnAccountDetailsPage_mmp369() {

        String email = PropertyFileReader.getProperty(onboardingDetailsFile,"emailid");
        String password = PropertyFileReader.getProperty(onboardingDetailsFile,"password");
        int errors = 0;

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver, test);

        loginDetailsPage.submitLoginDetails(email, password);
        test.log(LogStatus.INFO, "Entered Login detail and navigated to Account details page");

        accountDetailsPage.uploadFile(System.getProperty("user.dir") + "/src/test/resources/pictures/Picture.jpg");
        accountDetailsPage.cropPhoto();

        accountDetailsPage.clickBackBtn();
        test.log(LogStatus.INFO, "Clicked Back button on Account details page");

        String actualEmail = loginDetailsPage.getEmailEntered();
        String actualrepeatEmail = loginDetailsPage.getRepeatEmailEntered();
        String actualPassword = loginDetailsPage.getPasswordEntered();
        String actualrepeatPassword = loginDetailsPage.getRepeatPasswordEntered();

        if(actualEmail.equals(email)) {
            test.log(LogStatus.PASS, "Email ID value entered is retained");
        } else {
            test.log(LogStatus.FAIL, "Email ID value entered is not retained");
            errors++;
        }

        if(actualrepeatEmail.equals(email)) {
            test.log(LogStatus.PASS, "Repeat Email ID value entered is retained");
        } else {
            test.log(LogStatus.FAIL, "RepeatEmail ID value entered is not retained");
            errors++;
        }

        if(actualPassword.equals(password)) {
            test.log(LogStatus.PASS, "Password value entered is retained");
        } else {
            test.log(LogStatus.FAIL, "Password value entered is not retained");
            errors++;
        }

        if(actualrepeatPassword.equals(password)) {
            test.log(LogStatus.PASS, "Repeat password value entered is retained");
        } else {
            test.log(LogStatus.FAIL, "Repeat password value entered is not retained");
            errors++;
        }

        if(errors == 0) {
            test.log(LogStatus.PASS, "All details entered in login details retained");
        } else {
            test.log(LogStatus.FAIL, "Some/All of the details entered in login details not retained");
            Assert.fail("Some/All of the details entered in login details not retained");
        }

    }
}