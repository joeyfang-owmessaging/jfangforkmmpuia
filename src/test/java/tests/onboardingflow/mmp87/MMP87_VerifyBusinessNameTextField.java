package tests.onboardingflow.mmp87;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;
import utils.PropertyFileReader;

public class MMP87_VerifyBusinessNameTextField extends BaseTest {

    @Test
    public void verifyBusinessNameTextField_mmp350() {

        String email = PropertyFileReader.getProperty(onboardingDetailsFile,"emailid");
        String password = PropertyFileReader.getProperty(onboardingDetailsFile,"password");
        String businessName = PropertyFileReader.getProperty(onboardingDetailsFile,"business");
        int errors = 0;

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver, test);

        loginDetailsPage.submitLoginDetails(email, password);
        test.log(LogStatus.INFO, "Entered login details and navigated to Account Details page");

        accountDetailsPage.clickContinueBtn();
        test.log(LogStatus.INFO, "Clicked on Continue button without entering Business Name field");

        if(accountDetailsPage.businessNameErrorDisplayed()) {
            test.log(LogStatus.PASS, "Error message displayed for Business Name field");
        } else {
            test.log(LogStatus.FAIL, "Error message not displayed for Business Name field");
            errors++;
        }

        accountDetailsPage.enterBusinessName(businessName);
        test.log(LogStatus.INFO,"Entered Business Name field");

        accountDetailsPage.clickContinueBtn();

        if(accountDetailsPage.businessNameErrorDisplayed()) {
            test.log(LogStatus.FAIL, "Error message displayed even after entering Business Name field");
            errors++;
        } else {
            test.log(LogStatus.PASS, "Error message not displayed after entering Business Name field");
        }

        if(errors == 0) {
            test.log(LogStatus.PASS, "TestCase Passed: Error message validation is proper");
        } else {
            test.log(LogStatus.FAIL, "TestCase Failed: Error message validation is not proper");
            Assert.fail("TestCase Failed: Error message validation is not proper");
        }
    }
}

