package tests.onboardingflow.mmp87;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;
import utils.PropertyFileReader;

public class MMP87_VerifyYourNameTextField extends BaseTest {

    @Test
    public void verifyYourNameTextField_mmp349() {

        String email = PropertyFileReader.getProperty(onboardingDetailsFile,"emailid");
        String password = PropertyFileReader.getProperty(onboardingDetailsFile,"password");
        String yourName = PropertyFileReader.getProperty(onboardingDetailsFile,"name");
        int errors = 0;

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver, test);

        loginDetailsPage.submitLoginDetails(email, password);
        test.log(LogStatus.INFO, "Entered login details and navigated to Account Details page");

        accountDetailsPage.clickContinueBtn();
        test.log(LogStatus.INFO, "Clicked on Continue button without entering Your Name field");

        if(accountDetailsPage.nameErrorDisplayed()) {
            test.log(LogStatus.PASS, "Error message displayed for Your Name field");
        } else {
            test.log(LogStatus.FAIL, "Error message not displayed for Your Name field");
            errors++;
        }

        accountDetailsPage.enterYourName(yourName);
        test.log(LogStatus.INFO,"Entered Your Name field");

        accountDetailsPage.clickContinueBtn();

        if(accountDetailsPage.nameErrorDisplayed()) {
            test.log(LogStatus.FAIL, "Error message displayed even after entering Your Name field");
            errors++;
        } else {
            test.log(LogStatus.PASS, "Error message not displayed after entering Your Name field");
        }

        if(errors == 0) {
            test.log(LogStatus.PASS, "TestCase Passed: Error message validation is proper");
        } else {
            test.log(LogStatus.FAIL, "TestCase Failed: Error message validation is not proper");
            Assert.fail("TestCase Failed: Error message validation is not proper");
        }
    }
}
