package tests.onboardingflow.mmp87;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;
import utils.PropertyFileReader;

public class MMP87_VerifyCityTextField extends BaseTest {

    @Test
    public void verifyCityTextField_mmp360() {

        String email = PropertyFileReader.getProperty(onboardingDetailsFile,"emailid");
        String password = PropertyFileReader.getProperty(onboardingDetailsFile,"password");
        String city = PropertyFileReader.getProperty(onboardingDetailsFile,"city");
        int errors = 0;

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver, test);

        loginDetailsPage.submitLoginDetails(email, password);
        test.log(LogStatus.INFO, "Entered login details and navigated to Account Details page");

        accountDetailsPage.clickContinueBtn();
        test.log(LogStatus.INFO, "Clicked on Continue button without entering city field");

        if(accountDetailsPage.cityErrorDisplayed()) {
            test.log(LogStatus.PASS, "Error message displayed for city field");
        } else {
            test.log(LogStatus.FAIL, "Error message not displayed for city field");
            errors++;
        }

        accountDetailsPage.enterCity(city);
        test.log(LogStatus.INFO,"Entered city field");

        accountDetailsPage.clickContinueBtn();

        if(accountDetailsPage.cityErrorDisplayed()) {
            test.log(LogStatus.FAIL, "Error message displayed even after entering city field");
            errors++;
        } else {
            test.log(LogStatus.PASS, "Error message not displayed after entering city field");
        }

        if(errors == 0) {
            test.log(LogStatus.PASS, "TestCase Passed: Error message validation is proper");
        } else {
            test.log(LogStatus.FAIL, "TestCase Failed: Error message validation is not proper");
            Assert.fail("TestCase Failed: Error message validation is not proper");
        }
    }
}
