package tests.onboardingflow.mmp87;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;
import utils.PropertyFileReader;

public class MMP87_VerifyUploadPhotoSection extends BaseTest {

    @Test
    public void verifyUploadPhotoSection_mmp352() {

        String email = PropertyFileReader.getProperty(onboardingDetailsFile,"emailid");
        String password = PropertyFileReader.getProperty(onboardingDetailsFile,"password");
        String path = System.getProperty("user.dir") + PropertyFileReader.getProperty(onboardingDetailsFile,"image.png");
        int errors = 0;

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver, test);

        loginDetailsPage.submitLoginDetails(email, password);
        test.log(LogStatus.INFO, "Entered login details and navigated to Account Details page");

        accountDetailsPage.clickContinueBtn();
        test.log(LogStatus.INFO, "Clicked on Continue button without Selecting Photo for Upload");

        if(accountDetailsPage.photoErrorDisplayed()) {
            test.log(LogStatus.PASS, "Error message displayed to select Profile Image");
        } else {
            test.log(LogStatus.FAIL, "Error message not displayed for Profile Image");
            errors++;
        }

        accountDetailsPage.uploadFile(path);
        test.log(LogStatus.INFO,"Selected Photo for Upload");

        accountDetailsPage.clickContinueBtn();

        if(accountDetailsPage.photoErrorDisplayed()) {
            test.log(LogStatus.FAIL, "Error message displayed even after Selecting Photo for Upload");
            errors++;
        } else {

            test.log(LogStatus.PASS, "Error message not displayed after Selecting Photo for Upload");

        }

        if(errors == 0) {
            test.log(LogStatus.PASS, "TestCase Passed: Error message validation is proper");

        } else {
            test.log(LogStatus.FAIL, "TestCase Failed: Error message validation is not proper");
            Assert.fail("TestCase Failed: Error message validation is not proper");
        }
    }
}
