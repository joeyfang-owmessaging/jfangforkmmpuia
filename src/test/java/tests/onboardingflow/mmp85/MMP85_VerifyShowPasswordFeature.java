package tests.onboardingflow.mmp85;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;

public class MMP85_VerifyShowPasswordFeature extends BaseTest {
    @Test
    public void verifyShoePasswordFeature_MMP383() {

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);

        ld.enterEmail1("test1@synchronoss.com");

        test.log(LogStatus.INFO, "Entered Email");

        ld.enterEmail2("test1@synchronoss.com");

        test.log(LogStatus.INFO, "Entered repeat Email");

        ld.enterPwd1("Test@1234");

        test.log(LogStatus.INFO, "Entered Password");

        ld.clickOnShowPasswordOption();

        String passwordType = ld.checkPasswordMask();

        if (passwordType.equals("text")) {

            test.log(LogStatus.PASS, "Password is displayed as plain text");
        } else {
            test.log(LogStatus.FAIL, "Password is displaying as masked");
            Assert.fail("Password is displaying as masked");
        }

    }
}
