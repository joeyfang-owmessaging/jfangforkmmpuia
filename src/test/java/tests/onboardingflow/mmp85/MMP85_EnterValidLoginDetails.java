package tests.onboardingflow.mmp85;


import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;


public class MMP85_EnterValidLoginDetails extends BaseTest {

    @Test
    public void verifyEnterLoginDetails_MMP386() {

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);

        ld.enterEmail1("test1@synchronoss.com");

        test.log(LogStatus.INFO,"Entered Email");

        ld.enterEmail2("test1@synchronoss.com");

        test.log(LogStatus.INFO,"Entered repeat Email");

        ld.enterPwd1("Test@1234");

        test.log(LogStatus.INFO,"Entered Password");

        ld.enterPwd2("Test@1234");

        test.log(LogStatus.INFO,"Entered repeat Password");

        ld.selectBusinessAccountType();

        test.log(LogStatus.INFO,"Selected Account Type");

        ld.submitDetails();

        test.log(LogStatus.INFO,"Clicked on Submit");


            String heading = ld.checkAccountDetailScreen();

            if(heading.contains("visited"))
            {

                test.log(LogStatus.PASS, "Login successful and Account Detail screen is displayed");
            }

            else{
                test.log(LogStatus.FAIL, "login unsuccessful ");
                Assert.fail("login unsuccessful ");
            }

    }

}
