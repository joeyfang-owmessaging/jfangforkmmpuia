package tests.onboardingflow.mmp85;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;

public class MMP85_SelectAccountType extends BaseTest {

    @Test
    public void verifySelectAccountType_384() {

        int error = 0;

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);

        loginDetailsPage.enterEmail1("test1@synchronoss.com");

        test.log(LogStatus.INFO, "Entered Email");

        loginDetailsPage.enterEmail2("test1@synchronoss.com");

        test.log(LogStatus.INFO, "Entered repeat Email");

        loginDetailsPage.enterPwd1("Test@1234");

        test.log(LogStatus.INFO, "Entered Password");

        loginDetailsPage.enterPwd2("Test@1234");

        test.log(LogStatus.INFO, "Entered repeat Password");

        loginDetailsPage.selectBusinessAccountType();


        if (loginDetailsPage.checkAccountTypeSelectedOrNot().contains("checked")) {
            test.log(LogStatus.PASS, "Business Account type Selected");

        } else {
            test.log(LogStatus.FAIL, "Business Account Type not selected");

            error++;
        }

        loginDetailsPage.selectBusinessDeveloperAccountType();

        if (loginDetailsPage.checkDevelopervsBusinessAccountTypeSelected().contains("checked")) {

            test.log(LogStatus.PASS, "Business vs Developer Account type Selected");

        } else {
            test.log(LogStatus.FAIL, "Businessvs Developer Account Type not selected");

            error++;
        }

        loginDetailsPage.selectDeveloperOnlyAccountType();

        if (loginDetailsPage.checkDeveloperOnlyAccountTypeSelected().contains("checked")) {
            test.log(LogStatus.PASS, "Developer only Account type Selected");

        } else {
            test.log(LogStatus.FAIL, "Developer only Account Type not selected");

            error++;

        }

        if (error > 0) {
            Assert.fail("Account Type not selected");
        }


    }

}
