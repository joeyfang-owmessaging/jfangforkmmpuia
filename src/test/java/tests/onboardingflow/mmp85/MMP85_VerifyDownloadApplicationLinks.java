package tests.onboardingflow.mmp85;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;

public class MMP85_VerifyDownloadApplicationLinks extends BaseTest {
    @Test
    public void verifyDownloadApplicationLinks_MMP387() {

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);

        ld.enterEmail1("test1@synchronoss.com");

        test.log(LogStatus.INFO, "Entered Email");

        ld.enterEmail2("test1@synchronoss.com");

        test.log(LogStatus.INFO, "Entered repeat Email");

        ld.enterPwd1("Test@1234");

        test.log(LogStatus.INFO, "Entered Password");

        ld.enterPwd2("Test@1234");

        int error = 0;

        ld.selectBusinessAccountType();
        String businesslink = ld.getBusinessOnlyApplicationLink();

        if (businesslink.equals("Download Bussines Only Application Form")) {
            test.log(LogStatus.PASS, "Download link to download Business only application form is displayed");
        } else {
            test.log(LogStatus.FAIL, "Download link to download Business only application form is not displayed");
            error++;
        }

        ld.selectBusinessDeveloperAccountType();
        String businessDeveloperLink = ld.getBusinessDeveloperApplicationLink();
        if (businessDeveloperLink.equals("Download Bussines W/Developer Application Form")) {
            test.log(LogStatus.PASS, "Download link to download Business/Developer application form is displayed");
        } else {
            test.log(LogStatus.FAIL, "Download link to download Business/Developer application form is not displayed");
            error++;
        }

        ld.selectDeveloperOnlyAccountType();
        String developerLink = ld.getDeveloperApplicationLink();
        if (developerLink.equals("Download Developer Only Application Form")) {
            test.log(LogStatus.PASS, "Download link to download Developer application form is displayed");
        } else {
            test.log(LogStatus.FAIL, "Download link to download Developer application form is not displayed");
            error++;
        }

        if (error > 0) {
            Assert.fail("Links not displayed");
        }

    }
}
