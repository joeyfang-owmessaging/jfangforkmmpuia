package tests.onboardingflow.mmp85;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;

public class MMP85_VerifyPasswordMasked extends BaseTest {
    @Test
    public void verifyPasswordMasked_MMP382() {

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);

        ld.enterEmail1("test1@synchronoss.com");

        test.log(LogStatus.INFO, "Entered Email");

        ld.enterEmail2("test1@synchronoss.com");

        test.log(LogStatus.INFO, "Entered repeat Email");

        ld.enterPwd1("Test@1234");

        test.log(LogStatus.INFO, "Entered Password");

        String passwordType = ld.checkPasswordMask();


        if (passwordType.equals("password")) {

            test.log(LogStatus.PASS, "Password is masked");
        } else {
            test.log(LogStatus.FAIL, "Password is displaying as plain text");
            Assert.fail("Password is displaying as plain text");
        }

    }
}
