package tests.onboardingflow.mmp85;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;

public class MMP85_VerifyBreadCrumbMenuOptionsNotClickable extends BaseTest {
    @Test
    public void verifyBreadCrumbmenuOptionNotClickable_MMP388() {

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);

        loginDetailsPage.clickOnAccountDetailTab();

        String headingCheck = loginDetailsPage.checkAccountDetailScreen();

        if (headingCheck.equals("")) {
            test.log(LogStatus.PASS, "BreadCrumb options are not clickable");
        } else {
            test.log(LogStatus.FAIL, "BreadCrumb options are clickable");

            Assert.fail("BreadCrumb options are clickable");
        }

    }
}
