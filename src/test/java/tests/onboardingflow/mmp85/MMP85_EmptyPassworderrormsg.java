package tests.onboardingflow.mmp85;

import pages.onboarding.business.LoginDetailsPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP85_EmptyPassworderrormsg extends BaseTest {

    @Test
    public void EmptyPassworderrormsg_MMP380(){

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);
        ld.enterEmail1("test@gmail.com");
        ld.enterEmail2("test@gmail.com");
        ld.submitDetails();

        String EmptyPassMsg = ld.checkEmptyPassworderrormsg();

        if (EmptyPassMsg.equals("Password is required.")) {

            test.log(LogStatus.PASS,"'Password is required' error message displayed");
        }else {

            test.log(LogStatus.FAIL,"'Password is required' error message not displayed");

            Assert.fail("'Password is required' error message not displayed");
        }

    }
}
