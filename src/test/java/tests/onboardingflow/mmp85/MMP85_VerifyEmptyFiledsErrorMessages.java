package tests.onboardingflow.mmp85;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;

public class MMP85_VerifyEmptyFiledsErrorMessages extends BaseTest {
    @Test
    public void verifyEmptyFieldsErrorMessages_MMP633() {

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);

        int error = 0;

        ld.submitDetails();
        String emptyMailErrorMsg = ld.checkEmptyemailerrormsg();

        if (emptyMailErrorMsg.equals("Email is required.")) {
            test.log(LogStatus.PASS, "'Email is required' error message displayed");
        } else {
            test.log(LogStatus.FAIL, "'Email is required' error message not displayed");
            error++;
        }

        String repeatEmptyEmailError = ld.checkRepeatEmptyemailerrormsg();

        if (repeatEmptyEmailError.equals("It is required you type again your email.")) {
            test.log(LogStatus.PASS, "'It is required you type again your email' error message displayed");
        } else {

            test.log(LogStatus.FAIL, "'It is required you type again your email' error message not displayed");
            error++;
        }
        String EmptyPassMsg = ld.checkEmptyPassworderrormsg();

        if (EmptyPassMsg.equals("Password is required.")) {

            test.log(LogStatus.PASS, "'Password is required' error message displayed");
        } else {

            test.log(LogStatus.FAIL, "'Password is required' error message not displayed");
            error++;
        }

        String emptyRepeatPassMsg = ld.checkRepeatPassworderrormsg();

        if (emptyRepeatPassMsg.equals("It is required you type again your password.")) {

            test.log(LogStatus.PASS, "'It is required you type again your password.' error message displayed");
        } else {

            test.log(LogStatus.FAIL, "'It is required you type again your password.' error message not displayed");
            error++;
        }

        String accountTypeError = ld.getAccountTypeErrorMsg();

        if (accountTypeError.equals("Account type is required.")) {

            test.log(LogStatus.PASS, "'Account Type is Required' error message displayed");
        } else {

            test.log(LogStatus.FAIL, "'Account Type is Required' error message not displayed");
            error++;
        }

        if (error > 0) {
            Assert.fail("Error message not displayed");
        }


    }
}
