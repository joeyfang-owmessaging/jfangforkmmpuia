package tests.onboardingflow.mmp85;

import pages.onboarding.business.LoginDetailsPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP85_Emptyemailerrormsgverification extends BaseTest {

    @Test
    public void VerifyemptyErrormsg_MMP376(){

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);
        ld.enterEmail1("");
        ld.submitDetails();
        String emptyMailErrorMsg = ld.checkEmptyemailerrormsg();

        if (emptyMailErrorMsg.equals("Email is required."))
        {
            test.log(LogStatus.PASS,"'Email is required' error message displayed");

        }
        else
        {

            test.log(LogStatus.FAIL,"'Email is required' error message not displayed");
            Assert.fail("'Email is required' error message not displayed");
        }

    }
}
