package tests.onboardingflow.mmp85;

import pages.onboarding.business.LoginDetailsPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP85_Repeatpassworderrormsg extends BaseTest {

    @Test
    public void RepeatPasswordErrorMsg_MMP379(){

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);
        ld.enterEmail1("test@gmail.com");
        ld.enterEmail2("test@gmail.com");
        ld.enterPwd1("Asdf@1234");
        ld.enterPwd2("asdf123");
        ld.submitDetails();

        String repeatpassderrormsg = ld.checkRepeatPassworderrormsg();

        if(repeatpassderrormsg.equals("Passwords not match.")){

            test.log(LogStatus.PASS,"'Password not match' error message displayed");
        }else {

            test.log(LogStatus.FAIL,"'Password not match' error message not displayed");

            Assert.fail("'Password not match' error message not displayed");
        }

    }
}
