package tests.onboardingflow.mmp85;

import pages.onboarding.business.LoginDetailsPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP85_RepeatEmailErrormsgVerification extends BaseTest{

    @Test

    public void VerifyRepeatemailerrormsg_MMP375(){

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);
        ld.enterEmail1("test@gmail.com");
        ld.enterEmail2("test");
        ld.submitDetails();
        //ld.enterPwd1("");
        String Repeaterrormsg = ld.checkRepeatemailerrromsg();

        if(Repeaterrormsg.equals("Emails not match.")){

            test.log(LogStatus.PASS,"'Email not match' error message displayed");
        }

        else
        {

            test.log(LogStatus.FAIL,"Email error message not displayed");
            Assert.fail("Email error message not displayed");
        }

    }
}
