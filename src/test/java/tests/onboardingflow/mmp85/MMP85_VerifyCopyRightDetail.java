package tests.onboardingflow.mmp85;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;

public class MMP85_VerifyCopyRightDetail extends BaseTest {
    @Test
    public void verifyCopyRightDetail_MMP390() {

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);
        String copyRight = ld.getCopyRightDetail();

        if (copyRight.equals("© 2018 Synchronoss. All Rights Reserved.")) {

            test.log(LogStatus.PASS, "Copy Right Details displayed");
        } else {
            test.log(LogStatus.FAIL, "Copy right Details not displayed");
            Assert.fail("Copy right Details not displayed");

        }

    }
}
