package tests.onboardingflow.mmp85;

/**
 * Created by pkum0020 on 28/12/17.
 */

import org.testng.Assert;
import pages.onboarding.business.LoginDetailsPage;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import tests.BaseTest;

import java.awt.*;

public class MMP85_PasswordValidation extends BaseTest {

    @Test
    public void verifypassword_MMP378() throws InterruptedException, AWTException {


        LoginDetailsPage ld = new LoginDetailsPage(driver, test);

        driver.findElement(By.xpath("//input[@id='password']")).click();
        driver.findElement(By.xpath("//input[@id='password']")).sendKeys("t");
        driver.findElement(By.xpath("//input[@id='repeatPassword']")).click();
        driver.findElement(By.xpath("//input[@id='password']")).click();
        driver.findElement(By.xpath("//input[@id='password']")).sendKeys("");
        driver.findElement(By.xpath("//input[@id='repeatPassword']")).click();
        driver.findElement(By.xpath("//input[@id='password']")).click();


        ld.selectBusinessAccountType();

        ld.submitDetails();

        String minimum = ld.checkminimumchar();

        int errormsg = 0;

        if(minimum.equals("")){

            test.log(LogStatus.PASS, "Error message displayed for minimum characters");


        }
        else {
            test.log(LogStatus.FAIL,"Error message not displayed for minimum characters");
            errormsg++;
        }

        String upper = ld.checkuppercase();

        if(upper.equals("")){

            test.log(LogStatus.PASS, "Error message displayed for Uppercase letter ");
        }

        else{
            test.log(LogStatus.FAIL, "Error message not displayed for Uppercase letter ");
            errormsg++;
        }


        String numeric = ld.checknumericvalue();

        if(numeric.equals("")){

            test.log(LogStatus.PASS, "Error message displayed for Numeric value ");
        }
        else{
            test.log(LogStatus.FAIL, "Error message not displayed for Numeric value ");
            errormsg++;
        }

        String special = ld.checkspecialchar();

        if(special.equals("")){

            test.log(LogStatus.PASS, "Error message displayed for Special character ");
        }
        else{
            test.log(LogStatus.FAIL, "Error message not displayed for Special character ");
            errormsg++;
        }

        //To check validation for Lower case
        driver.findElement(By.xpath("//input[@id='password']")).clear();
        driver.findElement(By.xpath("//input[@id='password']")).sendKeys("TTTT");

        String lower = ld.checklowercase();

        if(lower.equals("")){

            test.log(LogStatus.PASS, "Error message displayed for Lowercase letter ");
        }
        else{
            test.log(LogStatus.FAIL, "Error message not displayed for Lowercase letter ");
            errormsg++;
        }

        //To check validation for maximum characters
        driver.findElement(By.xpath("//input[@id='password']")).clear();
        driver.findElement(By.xpath("//input[@id='password']")).sendKeys("asdfAsdf@1234Asdf@1234Asdf@1234");

        String  maxim = ld.checkmaximumchar();

        if(maxim.equals("")){

            test.log(LogStatus.PASS, " Error message displayed for Max Password length ");
        }

        else{
            test.log(LogStatus.FAIL, "Error message not displayed for max Password length");
            errormsg++;
        }
        if (errormsg > 0) {
            Assert.fail("Error message not displayed");
        }

    }
}
