package tests.onboardingflow.mmp85;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;

import java.util.Set;

public class MMP85_VerifyTermsAndConditionsLink extends BaseTest {
    @Test
    public void verifyTermsAndConditionLink_MMP391() {

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);

        loginDetailsPage.clickOnTermsAndConditionLink();

        Set<String> allWindow = driver.getWindowHandles();

        String window1 = (String) allWindow.toArray()[0];

        String window2 = (String) allWindow.toArray()[1];

        int error = 0;

        //Switch to window 2 (Child window)and verify Title

        if (allWindow.size() == 2) {

            test.log(LogStatus.PASS, "Clicked on link and New Tab opened ");
        } else {

            test.log(LogStatus.FAIL, "Not clicked on Link");
            error++;
        }

        driver.switchTo().window(window2);
        if (driver.getTitle().equals("Google Terms of Service – Privacy & Terms – Google")) {

            test.log(LogStatus.PASS, "Terms and Condition URL is opened  in new tab");

        } else {

            test.log(LogStatus.FAIL, " New tab opened but Terms and Condition URL is not opened");
            error++;

        }

        if (error > 0) {
            Assert.fail("Link not opened");
        }

    }
}
