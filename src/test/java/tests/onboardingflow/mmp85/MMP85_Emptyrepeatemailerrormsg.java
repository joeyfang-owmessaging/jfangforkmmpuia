package tests.onboardingflow.mmp85;

import pages.onboarding.business.LoginDetailsPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP85_Emptyrepeatemailerrormsg extends BaseTest {
    @Test
    public void VerifyEmptyRepeatemailerrormsg_MMP377(){

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);
        ld.enterEmail1("Test@gmail.com");
        ld.submitDetails();
        String repeatEmptyEmailError = ld.checkRepeatEmptyemailerrormsg();

        if (repeatEmptyEmailError.equals("It is required you type again your email."))
        {
            test.log(LogStatus.PASS,"'It is required you type again your email' error message displayed");

        }
        else {

            test.log(LogStatus.FAIL, "'It is required you type again your email' error message not displayed");
            Assert.fail("'It is required you type again your email' error message not displayed");

        }
    }
}
