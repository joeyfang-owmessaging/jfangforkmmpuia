package tests.onboardingflow.mmp85;

import pages.onboarding.business.LoginDetailsPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP85_EmptyRepeatPassworderrormsg extends BaseTest {

    @Test
    public void Emptyrepeatpasserrormsg_MMP381(){

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);
        ld.enterEmail1("test@gmail.com");
        ld.enterEmail2("test@gmail.com");
        ld.enterPwd1("Asdf@123");
        ld.submitDetails();
        String emptyRepeatPassMsg = ld.checkRepeatPassworderrormsg();

        if (emptyRepeatPassMsg.equals("It is required you type again your password.")) {

            test.log(LogStatus.PASS,"'It is required you type again your password.' error message displayed");
        }else {

            test.log(LogStatus.FAIL,"'It is required you type again your password.' error message not displayed");

            Assert.fail("'It is required you type again your password.' error message not displayed");
        }

    }
}
