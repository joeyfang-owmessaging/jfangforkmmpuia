package tests.onboardingflow.mmp85;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;
import pages.onboarding.business.LoginDetailsPage;
import tests.BaseTest;

public class MMP85_LoginDetailUIVerification extends BaseTest {
    @Test
    public void verifyLoginDetailUI_MMP372() {

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);
        int error = 0;


        if (loginDetailsPage.checkCompanyLogoandText()) {
            test.log(LogStatus.PASS, "Company Logo and Text is displayed");
        } else {
            test.log(LogStatus.FAIL, "Company Logo and Text not displayed");
            error++;
        }

        if (loginDetailsPage.checkHeadingText()) {
            test.log(LogStatus.PASS, "Heading text displayed");
        } else {
            test.log(LogStatus.FAIL, "Heading text not displayed");
            error++;
        }

        if (loginDetailsPage.checkBreadCrumbMenu()) {
            test.log(LogStatus.PASS, "Bread Crumb Menu displayed");
        } else {
            test.log(LogStatus.FAIL, "Bread Crumb Menu not displayed");
            error++;
        }
    }
}
