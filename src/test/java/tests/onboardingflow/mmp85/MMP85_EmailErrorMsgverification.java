package tests.onboardingflow.mmp85;

import pages.onboarding.business.LoginDetailsPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP85_EmailErrorMsgverification extends BaseTest {

    @Test

    public void verifyEmailErrormsg_MMP374(){

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);
        ld.enterEmail1("test@com");
        ld.submitDetails();
        //ld.enterEmail2("");
        String errormsg = ld.checkemailerrormsg();
        if(errormsg.equals("Email is invalid")){

            test.log(LogStatus.PASS,"'Email is invalid' error message displayed");
        }

        else
            {

               // test.log(LogStatus.FAIL,"Email error message not displayed");
                Assert.fail("Email error message not displayed");
            }

    }

}
