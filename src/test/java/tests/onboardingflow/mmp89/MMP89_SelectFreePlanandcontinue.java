package tests.onboardingflow.mmp89;

import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.AccountTypePage;
import pages.onboarding.business.LoginDetailsPage;
import pages.onboarding.business.SubscriptionPlanPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP89_SelectFreePlanandcontinue extends BaseTest {

    @Test
    public void SelectFreePlanContinue_MMP445() {

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);

        ld.submitLoginDetails("test3@synchronoss.com", "Test@12345");

        test.log(LogStatus.INFO, "Entered Login Details and clicked on continue Button");

        AccountDetailsPage aD = new AccountDetailsPage(driver,test);

        aD.submitAccountDetails(driver);

        test.log(LogStatus.INFO, "Entered Business Details and clicked on continue Button");

        AccountTypePage atp = new AccountTypePage(driver, test);

        atp.selectStdAcc();
        atp.clickContinueBtn();

        test.log(LogStatus.INFO, "Selected standard account type and clicked on continue Button");

        SubscriptionPlanPage sp = new SubscriptionPlanPage(driver);

        sp.SelectFreePlan();
        sp.Continue();

        String heading = sp.checkSummaryScreen();
        if(heading.contains("visited"))
        {

            test.log(LogStatus.PASS, "Summary screen is displayed");
        }

        else{
            test.log(LogStatus.FAIL, "Summary screen is not displayed");
            Assert.fail("Summary Screen not displayed after clicked on continue in Subscription Plan screen");
        }

    }


}
