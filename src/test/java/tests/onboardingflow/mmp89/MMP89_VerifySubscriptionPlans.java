package tests.onboardingflow.mmp89;

import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.AccountTypePage;
import pages.onboarding.business.LoginDetailsPage;
import pages.onboarding.business.SubscriptionPlanPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP89_VerifySubscriptionPlans extends BaseTest {

    @Test
    public void Verifyplans_MM437(){

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);

        ld.submitLoginDetails("test3@synchronoss.com", "Test@12345");

        test.log(LogStatus.INFO, "Entered Login Details and clicked on continue Button");

        AccountDetailsPage aD = new AccountDetailsPage(driver, test);

        aD.submitAccountDetails(driver);

        test.log(LogStatus.INFO, "Entered Business Details and clicked on continue Button");

        AccountTypePage atp = new AccountTypePage(driver, test);

        atp.selectStdAcc();
        atp.clickContinueBtn();

        test.log(LogStatus.INFO, "Selected standard account type and clicked on continue Button");

        SubscriptionPlanPage sp = new SubscriptionPlanPage(driver);

        int error = 0;


        if(sp.CheckFreePlan()){

            test.log(LogStatus.PASS,"Free Plan Exists");
        }

        else{

            test.log(LogStatus.FAIL,"Free Plan Not available");
            error++;

        }


        if(sp.CheckBasicPlan()){

            test.log(LogStatus.PASS,"Basic Plan Exists");
        }

        else{

            test.log(LogStatus.FAIL,"Basic Plan Not available");
            error++;

        }

        if(sp.CheckProPlan()){

            test.log(LogStatus.PASS,"Pro Plan Exists");

        }

        else{

            test.log(LogStatus.FAIL,"Pro Plan Not available");
            error++;
        }


        if (error > 0) {
            Assert.fail("All Plans not displayed");
        }
    }


}
