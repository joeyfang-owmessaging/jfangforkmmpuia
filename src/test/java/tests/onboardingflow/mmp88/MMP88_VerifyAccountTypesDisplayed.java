package tests.onboardingflow.mmp88;

import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.AccountTypePage;
import pages.onboarding.business.LoginDetailsPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;
import utils.PropertyFileReader;

public class MMP88_VerifyAccountTypesDisplayed extends BaseTest {

    @Test
    public void verifyAccountTypesDisplayed_mmp421() {

        String email = PropertyFileReader.getProperty(onboardingDetailsFile,"emailid");
        String password = PropertyFileReader.getProperty(onboardingDetailsFile,"password");
        int errors = 0;

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver,test);
        AccountTypePage accountTypePage = new AccountTypePage(driver, test);

        loginDetailsPage.submitLoginDetails(email, password);
        test.log(LogStatus.INFO, "Submitted Login Details");

        accountDetailsPage.submitAccountDetails(driver);
        test.log(LogStatus.INFO, "Submitted Business Details");

        if(accountTypePage.standardAccountTypeDisplayed()) {
            test.log(LogStatus.PASS, "Standard Account Displayed");
        } else {
            test.log(LogStatus.FAIL, "Standard Account Not Displayed");
            errors++;
        }

        if(accountTypePage.officialAccountTypeDisplayed()) {
            test.log(LogStatus.PASS, "Official Account Displayed");
        } else {
            test.log(LogStatus.FAIL, "Offical Account Not Displayed");
            errors++;
        }
        if(accountTypePage.a2PAccountTypeDisplayed()) {
            test.log(LogStatus.PASS, "A2P Account Displayed");
        } else {
            test.log(LogStatus.FAIL, "A2P Account Not Displayed");
            errors++;
        }

        if(errors > 0) {
            test.log(LogStatus.FAIL, "Test Failed: One or more Account Types not Displayed");
            Assert.fail();
        }


    }
}
