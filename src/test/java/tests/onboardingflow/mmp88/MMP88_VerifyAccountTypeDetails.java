package tests.onboardingflow.mmp88;

import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.AccountTypePage;
import pages.onboarding.business.LoginDetailsPage;
import utils.ExcelFileReader;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;
import utils.PropertyFileReader;

public class MMP88_VerifyAccountTypeDetails extends BaseTest {

    @Test
    public void verifyAccountTypeDetails_mmp423(){

        String email = PropertyFileReader.getProperty(globalPropertyFile,"email");
        String password = PropertyFileReader.getProperty(globalPropertyFile,"password");
        String path = System.getProperty("user.dir") + PropertyFileReader.getProperty(globalPropertyFile,"excelfile");
        int errors=0;

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver, test);
        AccountTypePage accountTypePage = new AccountTypePage(driver, test);

        loginDetailsPage.submitLoginDetails(email,password);
        test.log(LogStatus.INFO,"Entered Login Details");

        accountDetailsPage.submitAccountDetails(driver);
        test.log(LogStatus.INFO,"Entered Business Details");

        if(accountTypePage.businessTypeRowLabelDisplayed()){
            test.log(LogStatus.PASS, "Business Type row label displayed");
        }else {
            test.log(LogStatus.FAIL, "Business Type row label not displayed");
            errors++;
        }

        if(accountTypePage.numberOfSubscribersRowLabelDisplayed()){
            test.log(LogStatus.PASS, "Number Of Subscribers row label displayed");
        }else {
            test.log(LogStatus.FAIL, "Number Of Subscribers row label not displayed");
            errors++;
        }

        if(accountTypePage.sendMessagesRowLabelDisplayed()){
            test.log(LogStatus.PASS, "Send Messages row label displayed");
        }else {
            test.log(LogStatus.FAIL, "Send Messages row label not displayed");
            errors++;
        }

        if(accountTypePage.pricingStructureRowLabelDisplayed()){
            test.log(LogStatus.PASS, "pricing Structure row label displayed");
        }else {
            test.log(LogStatus.FAIL, "pricing Structure row label not displayed");
            errors++;
        }


        for(int i=0;i<3;i++){

            for(int j=0;j<3;j++){

                String xy = String.valueOf(i)+String.valueOf(j);

                String actual=accountTypePage.getGridText(driver,xy);

                String expected = ExcelFileReader.getData(path,"AccountTypeData",i,j);

                String result = "Expected: "+expected+"|| Actual: "+actual;

                if(actual.equals(expected)){
                    test.log(LogStatus.PASS,result);
                }else{
                    test.log(LogStatus.FAIL,result);
                    errors++;
                }
            }
        }
        if(errors==0){
            test.log(LogStatus.PASS,"Account Type details are verified");
        }else{
            test.log(LogStatus.FAIL,"Account Type details are not as expected");
            Assert.fail("Account Type details are not as expected");
        }

    }
}
