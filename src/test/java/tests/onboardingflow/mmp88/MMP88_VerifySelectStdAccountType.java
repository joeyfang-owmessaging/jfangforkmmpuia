package tests.onboardingflow.mmp88;

import org.testng.annotations.Test;
import pages.onboarding.business.AccountDetailsPage;
import pages.onboarding.business.AccountTypePage;
import pages.onboarding.business.LoginDetailsPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import tests.BaseTest;
import utils.PropertyFileReader;

public class MMP88_VerifySelectStdAccountType extends BaseTest {

    @Test
    public void verifySelectStdAccountType_mmp424() {

        String email = PropertyFileReader.getProperty(onboardingDetailsFile,"emailid");
        String password = PropertyFileReader.getProperty(onboardingDetailsFile,"password");
        String expectedTitle = PropertyFileReader.getProperty(onboardingDetailsFile,"plans.title");

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver, test);
        AccountTypePage accountTypePage = new AccountTypePage(driver, test);

        ld.submitLoginDetails(email, password);
        test.log(LogStatus.INFO, "Entered Login Details and clicked on continue Button");

        accountDetailsPage.submitAccountDetails(driver);
        test.log(LogStatus.INFO, "Entered Business Details and clicked on continue Button");


        accountTypePage.selectStdAcc();

        if(accountTypePage.standardAccIsSelected()) {
            test.log(LogStatus.PASS, "Standard Account Selected");
        } else {
            test.log(LogStatus.FAIL, "Standard account not selected");
        }

        accountTypePage.clickContinueBtn();
        test.log(LogStatus.INFO, "Clicked on Continue Button");


        String actualTitle = driver.getTitle();

        if(actualTitle.equalsIgnoreCase(expectedTitle)) {
            test.log(LogStatus.PASS, "Plans screen displayed");
        } else {
            test.log(LogStatus.FAIL, "Plans screen not displayed. Expected Title: " + expectedTitle + " | Actual Title: " + actualTitle);
            Assert.fail();
        }

    }
}


