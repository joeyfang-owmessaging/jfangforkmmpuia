package tests.onboardingflow.mmp90;

import pages.onboarding.business.*;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP90_VerifyBreadCrumbmenuinsummaryscreen extends BaseTest {

    @Test
    public void VerifyBreadCrumbMenuHighlighted_MMP577(){

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);

        AccountDetailsPage aD = new AccountDetailsPage(driver, test);

        ld.submitLoginDetails("test1@synchronoss.com","Test@1234");
        test.log(LogStatus.INFO,"Submitted Login Details");

        aD.submitAccountDetails(driver);
        test.log(LogStatus.INFO, "Account Selected and clicked on continue ");

        AccountTypePage at = new AccountTypePage(driver, test);
        at.selectStdAcc();
        at.clickContinueBtn();
        test.log(LogStatus.INFO, "Account Selected and clicked on continue ");

        SubscriptionPlanPage sp = new SubscriptionPlanPage(driver);
        sp.SelectFreePlan();
        sp.Continue();
        test.log(LogStatus.INFO, "Plan Selected and clicked on continue ");

        SummaryPage ss = new SummaryPage(driver);
        String heading = ss.checkBreadCrumbMenu();

        if(heading.contains("visited"))
        {

            test.log(LogStatus.PASS, "Summary Tab is highlighted");
        }

        else{
            test.log(LogStatus.FAIL, "Summary Tab is not highlighted");
            Assert.fail("Summary Tab is not highlighted");
        }

    }
}
