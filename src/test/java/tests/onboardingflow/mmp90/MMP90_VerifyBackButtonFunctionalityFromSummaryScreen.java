package tests.onboardingflow.mmp90;

import pages.onboarding.business.*;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP90_VerifyBackButtonFunctionalityFromSummaryScreen extends BaseTest{
    @Test
    public void veryBackBUttonFunctionality_MMP581() throws InterruptedException {

        SummaryPage sd = new SummaryPage(driver);

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);

        AccountDetailsPage aD = new AccountDetailsPage(driver, test);

        String emailid = "test1@synchronoss.com";
        String password = "Test@1234";

        ld.submitLoginDetails(emailid,password);

        test.log(LogStatus.INFO,"Submitted Login Details");

        String name = "Test123";
        String business = "Synchronoss Technologies";
        String businessCategory="Finance";
        String pincode = "560029";
        String prefecture="Akita-ken";
        String city = "Bangalore";
        String address =  "Tower B Subramanya Arcade Bannerghatta Road";
        String description = "Its a product based company";
        String phone = "08026280012";
        String link = "https://www.synchronoss.com";
        String accType;
        String freePlan;
        String imagename = "Picture.jpg";

        aD.AccountDetailFill(driver, name, business,businessCategory, pincode,prefecture, city, address, description, phone, link);

        test.log(LogStatus.INFO, "Account details submitted");

        AccountTypePage at = new AccountTypePage(driver, test);

        at.selectStdAcc();
        accType=at.getStandardAccText();
        at.clickContinueBtn();

        test.log(LogStatus.INFO, "Account Selected and clicked on continue ");

        SubscriptionPlanPage sp = new SubscriptionPlanPage(driver);
        sp.SelectFreePlan();
        freePlan=sp.GetFreePlanText();
        sp.Continue();

        test.log(LogStatus.INFO, "Plan Selected and clicked on continue ");

        Thread.sleep(5000);

        sd.clickBackButton();

        String heading = sd.checkBreadCrumbMenu();

        if(heading.contains("visited"))
        {

            test.log(LogStatus.FAIL, "Still IN summary Tab");
        }


        Boolean planScreenText = sd.checkSelectPlanScreen();

        if(planScreenText==true) {
            test.log(LogStatus.PASS, "Back button clicked and Plan screen Displayed");
        }else {
            test.log(LogStatus.FAIL,"Plan Screen not displayed");
            Assert.fail("Plan Screen not displayed");
        }

    }

//        env+".login.url"

}

