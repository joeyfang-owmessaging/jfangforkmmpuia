package tests.onboardingflow.mmp90;

import org.testng.Assert;
import pages.onboarding.business.*;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;
import tests.BaseTest;
import utils.PropertyFileReader;

public class MMP90_VerifyBusinessDetailsinSummaryScreen extends BaseTest {

    @Test
    public void verifyBusinessDetail_MMP579(){

        LoginDetailsPage loginDetailsPage = new LoginDetailsPage(driver, test);
        AccountDetailsPage accountDetailsPage = new AccountDetailsPage(driver,test);
        AccountTypePage accountTypePage = new AccountTypePage(driver, test);
        SubscriptionPlanPage subscriptionPlanPage = new SubscriptionPlanPage(driver);
        SummaryPage summaryPage = new SummaryPage(driver);

        int error = 0;
        String emailid = PropertyFileReader.getProperty(onboardingDetailsFile,"emailid");
        String password = PropertyFileReader.getProperty(onboardingDetailsFile,"password");
        String name = PropertyFileReader.getProperty(onboardingDetailsFile,"name");
        String business = PropertyFileReader.getProperty(onboardingDetailsFile,"business");
        String businessCategory=PropertyFileReader.getProperty(onboardingDetailsFile,"category");
        String pincode = PropertyFileReader.getProperty(onboardingDetailsFile,"postalcode");
        String prefecture=PropertyFileReader.getProperty(onboardingDetailsFile,"prefecture");
        String city = PropertyFileReader.getProperty(onboardingDetailsFile,"city");
        String address =  PropertyFileReader.getProperty(onboardingDetailsFile,"address");
        String description = PropertyFileReader.getProperty(onboardingDetailsFile,"description");
        String phone = PropertyFileReader.getProperty(onboardingDetailsFile,"phonenumber");
        String link = PropertyFileReader.getProperty(onboardingDetailsFile,"website");
        String imagename = PropertyFileReader.getProperty(onboardingDetailsFile,"jpg.image.name");

        loginDetailsPage.submitLoginDetails(emailid,password);

        test.log(LogStatus.INFO,"Submitted Login Details");

        accountDetailsPage.AccountDetailFill(driver, name, business,businessCategory, pincode,prefecture, city, address, description, phone, link);

        test.log(LogStatus.INFO, "Account details submitted");

        accountTypePage.selectStdAcc();
        accountTypePage.clickContinueBtn();
        test.log(LogStatus.INFO, "Standard Account Selected and clicked on continue ");

        subscriptionPlanPage.SelectFreePlan();
        subscriptionPlanPage.Continue();
        test.log(LogStatus.INFO, "Free Plan Selected and clicked on continue ");

        String actualName = summaryPage.getName();
        String actualBusinessName = summaryPage.getBusinessName();
        String actualPhotoName = summaryPage.getProfilePhotoName();
        String actualPostalCode = summaryPage.getPostalCode();
        String actualPrefeture = summaryPage.getPrefecture();
        String actualCity = summaryPage.getCity();
        String actualAddress = summaryPage.getAddress();
        String actualBusinessDescription = summaryPage.getBusinessDescription();
        String actualPhnoneNo = summaryPage.getPhoneNumber();
        String actualWeburl = summaryPage.getWebUrl();

        if(actualName.equals(name)){
            test.log(LogStatus.PASS,"User Name verified");
        }else {
            test.log(LogStatus.FAIL,"User name not match. Expected:"+name+" | Actual: "+actualName);
            error++;
        }

        if(actualBusinessName.equals(business))
        {
            test.log(LogStatus.PASS,"Business name  verified");
        } else {

            test.log(LogStatus.FAIL,"Business name not match. Expected: "+business+" | Actual: "+actualBusinessName);
            error++;
        }

        if(actualPhotoName.equals(imagename)){
            test.log(LogStatus.PASS,"image name verified");
        }else {
            test.log(LogStatus.FAIL,"image name not match. Expected:"+imagename+" | Actual: "+actualPhotoName);
            error++;
        }

        if(actualPostalCode.equals(pincode)){
            test.log(LogStatus.PASS,"Postal code verified");
        }else {
            test.log(LogStatus.FAIL,"postal code not match. Expected: "+pincode+" | Actual: "+actualPostalCode);
            error++;
        }

        if(actualPrefeture.equals(prefecture)){
            test.log(LogStatus.PASS,"Prefecture verified");
        }else {
            test.log(LogStatus.FAIL,"Prefecture not match. Expected:"+prefecture+" | Actual: "+actualPrefeture);
            error++;
        }

        if(actualCity.equals(city)){
            test.log(LogStatus.PASS,"City name verified");
        }else {
            test.log(LogStatus.FAIL,"City name not match. Expected:"+city+" | Actual: "+actualCity);
            error++;
        }

        if(actualAddress.equals(address)){
            test.log(LogStatus.PASS,"Address verified");
        }else {
            test.log(LogStatus.FAIL,"Address not match. Expected: "+address+" | Actual: "+actualAddress);
            error++;
        }

        if(actualBusinessDescription.equals(description)){
            test.log(LogStatus.PASS,"Business description verified");
        }else {
            test.log(LogStatus.FAIL,"Business description not match. Expected: "+description+" | Actual: "+actualBusinessDescription);
            error++;
        }

        if(actualPhnoneNo.equals(phone)){
            test.log(LogStatus.PASS,"Phone number verified");
        }else {
            test.log(LogStatus.FAIL,"Phone number not match. Expected: "+phone+" | Actual: "+actualPhnoneNo);
            error++;
        }

        if(actualWeburl.equals(link)){
            test.log(LogStatus.PASS,"Web URL verified");
        }else {
            test.log(LogStatus.FAIL,"Web URL not match. Expected: "+link+" | Actual: "+actualWeburl);
            error++;
        }

        if (error > 0) {

            Assert.fail("Business Details not displayed");
        }

    }
}
