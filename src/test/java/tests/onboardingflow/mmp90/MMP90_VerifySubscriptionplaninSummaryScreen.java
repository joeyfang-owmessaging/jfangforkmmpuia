package tests.onboardingflow.mmp90;

import org.testng.Assert;
import pages.onboarding.business.*;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP90_VerifySubscriptionplaninSummaryScreen extends BaseTest{
    @Test
    public void verifySubscriptionPlan_MMP580(){

        SummaryPage sd = new SummaryPage(driver);

        LoginDetailsPage ld = new LoginDetailsPage(driver, test);

        AccountDetailsPage aD = new AccountDetailsPage(driver, test);

        String emailid = "test1@synchronoss.com";
        String password = "Test@1234";

        ld.submitLoginDetails(emailid,password);

        test.log(LogStatus.INFO,"Submitted Login Details");

        String name = "Test123";
        String business = "Synchronoss Technologies";
        String businessCategory="Finance";
        String pincode = "560029";
        String prefecture="Akita-ken";
        String city = "Bangalore";
        String address =  "Tower B Subramanya Arcade Bannerghatta Road";
        String description = "Its a product based company";
        String phone = "08026280012";
        String link = "https://www.synchronoss.com";
        String accType;
        String freePlan;
        String imagename = "Picture.jpg";

        aD.AccountDetailFill(driver, name, business,businessCategory, pincode,prefecture, city, address, description, phone, link);

        test.log(LogStatus.INFO, "Account details submitted");

        AccountTypePage at = new AccountTypePage(driver, test);

        at.selectStdAcc();
        accType=at.getStandardAccText();
        at.clickContinueBtn();

        test.log(LogStatus.INFO, "Account Selected and clicked on continue ");

        SubscriptionPlanPage sp = new SubscriptionPlanPage(driver);
        sp.SelectFreePlan();
        freePlan=sp.GetFreePlanText();
        sp.Continue();

        test.log(LogStatus.INFO, "Plan Selected and clicked on continue ");

        SummaryPage ss = new SummaryPage(driver);

        String emailaddress = ss.getEmailAddress();
        String businesscat = ss.getBusinessCaterory();
        String premiumId = ss.getPremiumId();
        String yourname = ss.getName();
        String businessname = ss.getBusinessName();
        String photoname = ss.getProfilePhotoName();
        String postalCode = ss.getPostalCode();
        String prefetur = ss.getPrefecture();
        String city1 = ss.getCity();
        String address1 = ss.getAddress();
        String businessDescription = ss.getBusinessDescription();
        String phnno = ss.getPhoneNumber();
        String weburl = ss.getWebUrl();
        String acctype =ss.getAccType();
        String plantype = ss.getPlanplantype();
        String premiumid = ss.getPlanPremiumId();

        int error = 0;

        if(acctype.equals(accType)){
            test.log(LogStatus.PASS,"Account verified");
        }else {
            test.log(LogStatus.FAIL,"Account not match");
            error++;
        }

        if(plantype.equals(freePlan)){
            test.log(LogStatus.PASS,"Plan verified");
        }else {
            test.log(LogStatus.FAIL,"plan not match");
            error++;
        }

        if(premiumid.equals("@SampleId2018")){
            test.log(LogStatus.PASS,"premiumID verified");
        }else {
            test.log(LogStatus.FAIL,"premiumID not match");
            error++;
        }

        if (error > 0) {
            Assert.fail("Subscription Plan not displayed");
        }

    }
}
