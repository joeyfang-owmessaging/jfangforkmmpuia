package tests.loginflow.mmp94;

import pages.login.LoginPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP94_ErrorMsgForunregisteredEmail extends BaseTest {

    @Test
    public void verifyErrorMsgforUnregisteredEmail_MMP789(){

        LoginPage lp = new LoginPage(driver, test);
        lp.enterEmailId("Unregistered@gmail.com");
        lp.enterPassword("testing");
        lp.SubmitLoginDetails();
        String error_msg = lp.checkerrormsgforinvalidEmailPass();


        if (error_msg.equals("Invalid email or password")) {

            test.log(LogStatus.PASS,"Error message for unregistered Email ID is verified");
        }else {
            test.log(LogStatus.FAIL,"Error message for unregistered Email ID not displayed");
            Assert.fail("Error message for unregistered Email ID not displayed");
        }

    }
}
