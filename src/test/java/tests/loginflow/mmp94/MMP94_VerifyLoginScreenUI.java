package tests.loginflow.mmp94;

import org.testng.Assert;
import pages.login.LoginPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP94_VerifyLoginScreenUI extends BaseTest {

    @Test
    public void verifyLoginScreenUI_MMP788(){

        LoginPage lp = new LoginPage(driver, test);

        int error = 0;

        Boolean companyLogoText = lp.checkCompanyLogoandText();

        if (companyLogoText) {

         test.log(LogStatus.PASS,"Company Logo and Text is displayed");
        }else {

         test.log(LogStatus.FAIL,"Company Logo and Text not displayed");
         error++;
        }

        Boolean heading = lp.checkHeadingText();

        if (heading) {
         test.log(LogStatus.PASS,"Heading displayed");
        }else {
         test.log(LogStatus.FAIL,"Heading not displayed");
         error++;
        }

        Boolean emailAddLabel = lp.checkEmailAddressLabel();

        if (emailAddLabel) {
             test.log(LogStatus.PASS,"Email Address Label displayed");
        }else {
             test.log(LogStatus.FAIL,"Email Address Label not displayed");
             error++;
        }

         Boolean emailTextBox = lp.checkEmailAddressTxtBox();
            if (emailTextBox) {
             test.log(LogStatus.PASS,"Email Address Text Box Displayed");
         }else {
             test.log(LogStatus.FAIL,"Email Address Text Box Displayed");
             error++;
         }

         Boolean passwordLab = lp.checkPasswordLabel();

            if (passwordLab) {
             test.log(LogStatus.PASS,"Password label displayed");
         }else {
             test.log(LogStatus.FAIL,"Password label not displayed");
             error++;
         }

         Boolean passwordTxtBox = lp.checkPasswordTextBox();
            if (passwordTxtBox) {
             test.log(LogStatus.PASS,"Password Textbox displayed");
         }else {
             test.log(LogStatus.FAIL,"Password TextBox not displayed");
             error++;
         }

         Boolean checkBox = lp.checkKeepMeLoginCheckBox();
            if (checkBox) {
             test.log(LogStatus.PASS,"Keep me logged in checkBox displayed");
         }else {
             test.log(LogStatus.FAIL,"Keep me logged in CheckBox not displayed");
             error++;
         }

         Boolean keepMeLoggedinText = lp.checkKeepMeLoggedinText();
            if (keepMeLoggedinText) {
             test.log(LogStatus.PASS,"Keep Me Loggedin text displayed");
         }else {
             test.log(LogStatus.FAIL,"Keep Me Loggedin text not displayed");
             error++;
         }

         Boolean forgotPassLink = lp.checkForgotPassLink();
            if (forgotPassLink) {
             test.log(LogStatus.PASS,"Forgot Password Link Displayed");
         }else {
             test.log(LogStatus.FAIL,"Forgot Password Link not displayed");
             error++;
         }

         Boolean loginButton = lp.checkLoginButton();
            if (loginButton) {
             test.log(LogStatus.PASS,"Login button displayed");
         }else {
             test.log(LogStatus.FAIL,"Login button not displayed");
             error++;
         }

         Boolean createYourAccountLink = lp.checkCreateYourAccLink();
            if (createYourAccountLink) {
             test.log(LogStatus.PASS,"Create your account Link displayed");
         }else {
             test.log(LogStatus.FAIL,"Create your account Link not displayed");
             error++;
         }

         Boolean copyRight = lp.checkCopyRightDetail();
            if (copyRight) {
             test.log(LogStatus.PASS,"Copy Right Details displayed");
         }else {
             test.log(LogStatus.FAIL,"Copy Right Details not displayed");
             error++;
         }

            Boolean privacyPolicy = lp.checkPrivacyPolicy();
            if (privacyPolicy) {
             test.log(LogStatus.PASS,"Privacy Policy link displayed");
         }else {
             test.log(LogStatus.FAIL,"Privacy Policy Link not displayed");
             error++;
         }

         Boolean termsCondition = lp.checkTermsAndCondition();
            if (termsCondition) {
             test.log(LogStatus.PASS,"Terms and Conditions link displayed");
         }else {
             test.log(LogStatus.FAIL,"Terms and Conditions link not displayed");
             error++;
         }

            if (error > 0) {
                Assert.fail("Login Screen UI not displayed properly");
            }

        }


}
