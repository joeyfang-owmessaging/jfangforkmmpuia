package tests.loginflow.mmp94;

import pages.login.LoginPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP94_ErrorMsgForInvalidEmailFormat extends BaseTest {

    @Test
    public void verifyErrorMsgForInvalidEmailFormat_MMP796(){

    LoginPage lp = new LoginPage(driver, test);
    lp.enterEmailId("test@gmail");
    lp.SubmitLoginDetails();
    String invalidEmailFormat = lp.checkerrormsgforEmptyEmail();
    if (invalidEmailFormat.equals("Invalid email or password")) {

        test.log(LogStatus.PASS,"Error message for invalid EmailId is verified");
    }else {
        test.log(LogStatus.FAIL,"Error message for invalid EmailId is not displayed");
        Assert.fail("Error message for invalid EmailId not displayed");
    }

    }
}
