package tests.loginflow.mmp94;

import pages.login.LoginPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP94_ErrorMsgForEmptyEmailandPassword extends BaseTest{

    @Test
    public void verifyerrormsgforemptyemailandpassword_MMP794(){

        LoginPage lp = new LoginPage(driver, test);
        lp.SubmitLoginDetails();
        String emptyEmailerrormsg = lp.checkerrormsgforEmptyEmail();
        String emptyPassworderrormsg = lp.checkerrormsgforEmptyPass();

        if (emptyEmailerrormsg.equals("Invalid email or password")) {

            test.log(LogStatus.PASS,"Error message for empty email id is verified");
        }else {
            test.log(LogStatus.FAIL,"Error message for empty email id is not displayed");
            Assert.fail("Error message for empty email id is not displayed");
        }

        if (emptyPassworderrormsg.equals("Invalid email or password")) {

            test.log(LogStatus.PASS,"Error message for empty password field is verified");
        }else {
            test.log(LogStatus.FAIL,"Error message for empty password is not displayed");
            Assert.fail("Error message for empty password is not displayed");
        }


    }
}
