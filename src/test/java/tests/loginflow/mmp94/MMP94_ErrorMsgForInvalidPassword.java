package tests.loginflow.mmp94;

import pages.login.LoginPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP94_ErrorMsgForInvalidPassword extends BaseTest {

    @Test
    public void verifyerrormsgforinvalidpassword_MMP792(){

        LoginPage lp = new LoginPage(driver, test);
        lp.enterEmailId("kris@gmail.com");
        lp.enterPassword("invalidpassword");
        lp.SubmitLoginDetails();

        String error_msg_Pass = lp.checkerrormsgforinvalidEmailPass();


        if (error_msg_Pass.equals("Invalid email or password")) {

            test.log(LogStatus.PASS,"Error message for invalid password  is verified");
        }else {
            test.log(LogStatus.FAIL,"Error message for invalid password not displayed");
            Assert.fail("Error message for invalid password not displayed");
        }

    }
}
