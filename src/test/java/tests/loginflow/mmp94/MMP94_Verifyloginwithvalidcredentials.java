package tests.loginflow.mmp94;

import pages.login.LoginPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP94_Verifyloginwithvalidcredentials extends BaseTest {

   @Test
    public void verifyLogin_MMP793(){

       LoginPage lp = new LoginPage(driver, test);
       lp.enterEmailId("kris@gmail.com");
       lp.enterPassword("testing");
       lp.SubmitLoginDetails();
       String mineHeader = lp.verifyHeader();

       if(mineHeader.equalsIgnoreCase("MINE Manager")){
          test.log(LogStatus.PASS,"Login successful and Top Page displayed");
       }else {
          test.log(LogStatus.FAIL,"Login unsuccessful");
          Assert.fail("Login unsuccessfu");
       }

   }
}
