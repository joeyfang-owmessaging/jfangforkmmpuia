package tests.loginflow.mmp95;

import pages.login.LoginPage;
import pages.login.TopPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP95_VerifyTopPageHeader extends BaseTest {

    @Test
    public void verifyTopPageHeader_mmp867(){

        LoginPage loginPage = new LoginPage(driver, test);

        loginPage.userLogin("string28@gmail.com", "testing");

        test.log(LogStatus.INFO, "Submitted Login Details");

        TopPage topPage= new TopPage(driver, test);

        int errors = 0;

        if (topPage.synchronossLogoPresent()) {

            test.log(LogStatus.PASS, "Synchronoss Logo Present");

        } else {

            test.log(LogStatus.FAIL, "Synchronoss Logo not Present");
            errors++;

        }

        if (topPage.headerPresent()) {

            test.log(LogStatus.PASS, "MINE Manager Header Present");

        } else {

            test.log(LogStatus.FAIL, "MINE Manager not Present");
            errors++;

        }

        if (topPage.logoutIconPresent()) {

           test.log(LogStatus.PASS,"Logout Icon Present");

       }else{

           test.log(LogStatus.FAIL,"Logout Icon not Present");
           errors++;

        }

        if (errors > 0) {
            test.log(LogStatus.FAIL, "All the elements not present in Header section");
            Assert.fail("All the elements not present in Header section");
        }

    }
}
