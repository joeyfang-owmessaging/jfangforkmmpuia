package tests.loginflow.mmp95;

import pages.login.LoginPage;
import pages.login.TopPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP95_VerifyTopPageAccountSectionUI extends BaseTest {


    @Test
    public void verifyTopPageHeader_mmp868() {

        LoginPage loginPage = new LoginPage(driver, test);

        loginPage.userLogin("string28@gmail.com", "testing");

        test.log(LogStatus.INFO, "Submitted Login Details");

        TopPage topPage = new TopPage(driver, test);

        int errors = 0;

        if (topPage.chooseAccountTextPresent()) {

            test.log(LogStatus.PASS, "Choose an account below Text Present");

        } else {

            test.log(LogStatus.FAIL, "Choose an account below Text not Present");
            errors++;

        }

        if (topPage.searchIconPresent()) {

            test.log(LogStatus.PASS, "Search Icon Present");

        } else {

            test.log(LogStatus.FAIL, "Search Icon not Present");
            errors++;

        }

        if (topPage.accountStatusIconDisplayed()) {

            test.log(LogStatus.PASS, "Account Status Icon Present");

        } else {

            test.log(LogStatus.FAIL, "Account Status Icon not Present");
            errors++;

        }
        if (topPage.subscriberIconDisplayed()) {

            test.log(LogStatus.PASS, "Subscriber Icon Present");

        } else {

            test.log(LogStatus.FAIL, "Subscriber Icon not Present");
            errors++;

        }
        if (topPage.selectButtonPresent()) {

            test.log(LogStatus.PASS, "Select Button Present");

        } else {

            test.log(LogStatus.FAIL, "Select Button not Present");
            errors++;

        }
        if (topPage.createNewEntityButtonPresent()) {

            test.log(LogStatus.PASS, "create New Entity Button Present");

        } else {

            test.log(LogStatus.FAIL, "create New Entity Button not Present");
            errors++;

        }

        if (errors > 0) {
            test.log(LogStatus.FAIL, "All the Expected elements not present in Accounts Details section");
            Assert.fail("All the Expected elements not present in Accounts Details section");
        }


    }


}
