package tests.loginflow.mmp95;

import pages.login.LoginPage;
import pages.login.TopPage;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;
import tests.BaseTest;

public class MMP95_VerifyTopPageAccountDetails extends BaseTest {

    @Test
    public void verifyTopPageAccountDetails_mmp871() {

        String email = "string28@gmail.com";
        String password = "testing";
        String expectedBusinessName = "string28";
        String expectedUniqueID = "@string28";
        String expectedAccountTypeAndPlan = "Standard Account (Free)";
        String expectedAccountStatus = "Pending";
        String expectedNumberOfSubscribers = "users0";
        int errors = 0;

        LoginPage loginPage = new LoginPage(driver, test);
        loginPage.userLogin(email, password);
        test.log(LogStatus.INFO, "Submitted Login Details");

        TopPage topPage = new TopPage(driver, test);

        String actualBusinessName = topPage.getBusnessName();
        String actualUniqueID = topPage.getUniqueID();
        String actualAccountTypeAndPlan = topPage.getAccountTypeAndPlan();
        String actualAccountStatus = topPage.getAccountStatus();
        String actualNumberOfSubscribers = topPage.getNumberOfSubscribers();

        System.out.println("actualBusinessName: " + actualBusinessName);
        System.out.println("actualUniqueID: " + actualUniqueID);
        System.out.println("actualAccountTypeAndPlan: " + actualAccountTypeAndPlan);
        System.out.println("actualAccountStatus: " + actualAccountStatus);
        System.out.println("actualNumberOfSubscribers: " + actualNumberOfSubscribers);

        if (expectedBusinessName.equals(actualBusinessName)) {

            test.log(LogStatus.PASS, "Business Name is displayed correctly");

        } else {

            test.log(LogStatus.FAIL, "Business Name is not displayed correctly");
            errors++;

        }


    }
}
