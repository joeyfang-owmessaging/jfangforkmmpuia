package tests.accountmanagementconsole.mmp910;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.accountmanagementconsole.AMCHomePage;
import pages.accountmanagementconsole.CreateMessagePage;
import pages.accountmanagementconsole.ManageContentPage;
import pages.login.LoginPage;
import pages.login.TopPage;
import tests.BaseTest;
import utils.PropertyFileReader;

public class MMP1012_VerifyCreateMessagePageUI extends BaseTest{

    @Test
    public void verifyCreateMessagePageUI_mmp1012(){
    	System.out.println("5.1");
    	System.out.println("test is: "+test);
        test.log(LogStatus.INFO,"Login Page opened in browser");
        System.out.println("5.2");
        int errors = 0;

        CreateMessagePage createMessagePage = new CreateMessagePage(driver, test);
        createMessagePage.navigateToCreateMessagePage(driver);

        if(createMessagePage.cancelOptionDisplayed()){
            test.log(LogStatus.PASS,"Cancel Option displayed");
        } else{
            test.log(LogStatus.FAIL,"Cancel Option not displayed");
            errors++;
        }

        if(createMessagePage.createMessageHeaderDisplayed()){
            test.log(LogStatus.PASS,"Create Message Header displayed");
        } else{
            test.log(LogStatus.FAIL,"Create Message Header not displayed");
            errors++;
        }

        if(createMessagePage.previewButtonDisplayed()){
            test.log(LogStatus.PASS,"Preview button displayed");
        } else{
            test.log(LogStatus.FAIL,"Preview button not displayed");
            errors++;
        }

        if(createMessagePage.audienceLabelDisplayed()){
            test.log(LogStatus.PASS,"Audience Label displayed");
        } else{
            test.log(LogStatus.FAIL,"Audience Label not displayed");
            errors++;
        }

        if(createMessagePage.selectAudienceDropdownDisplayed()){
            test.log(LogStatus.PASS,"Select Audience dropdown displayed");
        } else{
            test.log(LogStatus.FAIL,"Select Audience dropdown not displayed");
            errors++;
        }

        if(createMessagePage.messageTitleLabelDisplayed()){
            test.log(LogStatus.PASS,"Message Title displayed");
        } else{
            test.log(LogStatus.FAIL,"Message Title not displayed");
            errors++;
        }

        if(createMessagePage.titleTextFieldDisplayed()){
            test.log(LogStatus.PASS,"Title dropdown displayed");
        } else{
            test.log(LogStatus.FAIL,"Title dropdown not displayed");
            errors++;
        }

        if(createMessagePage.sendNowRadioButtonDisplayed()){
            test.log(LogStatus.PASS,"Send Now radio button displayed");
        } else{
            test.log(LogStatus.FAIL,"Send Now radio button not displayed");
            errors++;
        }

        if(createMessagePage.scheduleRadioButtonDisplayed()){
            test.log(LogStatus.PASS,"Schedule radio button displayed");
        } else{
            test.log(LogStatus.FAIL,"Schedule radio button not displayed");
            errors++;
        }

        if(createMessagePage.imageRadioButtonDisplayed()){
            test.log(LogStatus.PASS,"Image radio button displayed");
        } else{
            test.log(LogStatus.FAIL,"Image radio button not displayed");
            errors++;
        }

        if(createMessagePage.textRadioButtonDisplayed()){
            test.log(LogStatus.PASS,"Text radio button displayed");
        } else{
            test.log(LogStatus.FAIL,"Text radio button not displayed");
            errors++;
        }

        if(createMessagePage.couponsAndPromotionsRadioButtonDisplayed()){
            test.log(LogStatus.PASS,"Coupons & Promotions radio button displayed");
        } else{
            test.log(LogStatus.FAIL,"Coupons & Promotions radio button not displayed");
            errors++;
        }

        if(createMessagePage.surveyRadioButtonDisplayed()){
            test.log(LogStatus.PASS,"Surevey radio button displayed");
        } else{
            test.log(LogStatus.FAIL,"Surevey radio button not displayed");
            errors++;
        }

        if(createMessagePage.richCardRadioButtonDisplayed()){
            test.log(LogStatus.PASS,"Rich Card radio button displayed");
        } else{
            test.log(LogStatus.FAIL,"Rich Card radio button not displayed");
            errors++;
        }

        if(createMessagePage.carouselRadioButtonDisplayed()){
            test.log(LogStatus.PASS,"Carousel radio button displayed");
        } else{
            test.log(LogStatus.FAIL,"Carousel radio button not displayed");
            errors++;
        }

        if(createMessagePage.saveDraftButtonDisplayed()){
            test.log(LogStatus.PASS,"Save Draft button displayed");
        } else{
            test.log(LogStatus.FAIL,"Save Draft button not displayed");
            errors++;
        }

        if(createMessagePage.sendButtonDisplayed()){
            test.log(LogStatus.PASS,"Send Button displayed");
        } else{
            test.log(LogStatus.FAIL,"Send Button not displayed");
            errors++;
        }

        if(errors==0){
            test.log(LogStatus.PASS,"All the expected UI elements displayed on Create Message page");

        }else{
            test.log(LogStatus.FAIL,"Some/All of the expected UI elements displayed on Create Message page");
            Assert.fail("Some/All of the expected UI elements displayed on Create Message page");
        }

    }
}
