package tests.accountmanagementconsole.mmp910;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.Test;
import pages.accountmanagementconsole.AMCHomePage;
import pages.accountmanagementconsole.CreateMessagePage;
import pages.accountmanagementconsole.ManageContentPage;
import pages.login.LoginPage;
import pages.login.TopPage;
import tests.BaseTest;
import utils.PropertyFileReader;

public class MMP1014_VerifyTextRadioButton extends BaseTest{

    @Test
    public void verifyTextRadioButton_mmp1014(){

        test.log(LogStatus.INFO,"Login Page opened in browser");

        int errors = 0;
        CreateMessagePage createMessagePage = new CreateMessagePage(driver, test);
        createMessagePage.navigateToCreateMessagePage(driver);


        createMessagePage.clickTextRadioButton();
        test.log(LogStatus.INFO,"Clicked on Text radio button on Create Message page");

        String message = PropertyFileReader.getProperty(globalPropertyFile, "text.message.305characters");

        createMessagePage.enterTextMessage(message,test);
        test.log(LogStatus.INFO,"Tried to enter 305 characters in Text field");

        int enteredMessagelength = createMessagePage.getMessageText().length();

        if(enteredMessagelength==300){
            test.log(LogStatus.PASS,"Only 300 characters entered in text message field");
        } else{
            test.log(LogStatus.FAIL,"enteredMessagelength "+"characters entered in text message field");
        }

    }

}
