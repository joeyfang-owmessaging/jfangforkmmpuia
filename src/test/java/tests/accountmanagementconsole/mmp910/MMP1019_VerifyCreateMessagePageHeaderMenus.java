package tests.accountmanagementconsole.mmp910;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.accountmanagementconsole.AMCPageHeader;
import pages.accountmanagementconsole.CreateMessagePage;
import tests.BaseTest;

public class MMP1019_VerifyCreateMessagePageHeaderMenus extends BaseTest{

    @Test
    public void verifyCreateMessagePageHeaderMenus_mmp1019(){

        test.log(LogStatus.INFO,"Login Page opened in browser");
        int errors=0;
        String expectedPlan = "";

        CreateMessagePage createMessagePage = new CreateMessagePage(driver, test);
        createMessagePage.navigateToCreateMessagePage(driver);

        AMCPageHeader amcPageHeader = new AMCPageHeader(driver, test);

        if(amcPageHeader.synchronossLogoDisplayed()){
            test.log(LogStatus.PASS," Displayed");

        } else {
            test.log(LogStatus.FAIL," not Displayed");
            errors++;
        }

        if(amcPageHeader.headerDisplayed()){
            test.log(LogStatus.PASS,"Account Management Console Header Displayed");

        } else {
            test.log(LogStatus.FAIL,"Account Management Console Header not Displayed");
            errors++;
        }

        if(amcPageHeader.uniqueIDDisplayed()){
            test.log(LogStatus.PASS,"UniqueID Displayed");

        } else {
            test.log(LogStatus.FAIL,"UniqueID not Displayed");
            errors++;
        }

        if(amcPageHeader.subscriberIconDisplayed()){
            test.log(LogStatus.PASS,"Subscriber Icon Displayed");

        } else {
            test.log(LogStatus.FAIL,"Subscriber Icon Displayed not Displayed");
            errors++;
        }

        if(amcPageHeader.getCurrentPlan().equals(expectedPlan)){
            test.log(LogStatus.PASS,"Current plan is displayed as expected");

        } else {
            test.log(LogStatus.FAIL,"Current plan is not displayed as expected");
            errors++;
        }

        if(amcPageHeader.upgradeLinkDisplayed()){
            test.log(LogStatus.PASS,"Upgrade link Displayed");

        } else {
            test.log(LogStatus.FAIL,"Upgrade link not Displayed");
            errors++;
        }

        if(amcPageHeader.notificationIconDisplayed()){
            test.log(LogStatus.PASS,"Notification icon Displayed");

        } else {
            test.log(LogStatus.FAIL,"Notification icon not Displayed");
            errors++;
        }

        if(amcPageHeader.settingsIconDisplayed()){
            test.log(LogStatus.PASS,"Settings icon Displayed");

        } else {
            test.log(LogStatus.FAIL,"Settings icon not Displayed");
            errors++;
        }

        if(amcPageHeader.logoutIconDisplayed()){
            test.log(LogStatus.PASS,"Logout icon Displayed");

        } else {
            test.log(LogStatus.FAIL,"Logout icon not Displayed");
            errors++;
        }

        if(errors==0){
            test.log(LogStatus.PASS,"All the expected Header menu items are displayed on Create Message page");

        }else{
            test.log(LogStatus.FAIL,"Some/All of the expected Header menu items are not displayed on Create Message page");
            Assert.fail("Some/All of the expected Header menu items are not displayed on Create Message page");
        }

    }
}
