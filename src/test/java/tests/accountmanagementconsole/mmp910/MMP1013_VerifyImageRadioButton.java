package tests.accountmanagementconsole.mmp910;

import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.accountmanagementconsole.AMCHomePage;
import pages.accountmanagementconsole.CreateMessagePage;
import pages.accountmanagementconsole.ManageContentPage;
import pages.login.LoginPage;
import pages.login.TopPage;
import tests.BaseTest;
import utils.PropertyFileReader;

public class MMP1013_VerifyImageRadioButton extends BaseTest{
    @Test
    public void verifyImageRadioButton_mmp1013(){

        test.log(LogStatus.INFO,"Login Page opened in browser");

        int errors = 0;

        CreateMessagePage createMessagePage = new CreateMessagePage(driver, test);
        createMessagePage.navigateToCreateMessagePage(driver);

        createMessagePage.clickImageRadioButton();
        test.log(LogStatus.INFO,"Clicked on Image radio button on Create Message page");

        if(createMessagePage.imageLabelDisplayed()){
            test.log(LogStatus.PASS,"Image section displayed");
        } else{
            test.log(LogStatus.FAIL,"Image section not displayed");
            errors++;
        }

        if(createMessagePage.imageDropAreaDisplayed()){
            test.log(LogStatus.PASS,"Drag and drop section displayed");
        } else{
            test.log(LogStatus.FAIL,"Drag and drop section not displayed");
            errors++;
        }

        if(createMessagePage.browseButtonDisplayed()){
            test.log(LogStatus.PASS,"Browse button displayed");
        } else{
            test.log(LogStatus.FAIL,"Browse button not displayed");
            errors++;
        }

        if(errors==0){
            test.log(LogStatus.PASS,"Image section displayed with drag and drop and browse button on Create Message page after selecting image radio button");

        }else{
            test.log(LogStatus.FAIL,"Image section not displayed properly on Create Message page after selecting image radio button");
            Assert.fail("Image section not displayed properly on Create Message page after selecting image radio button");
        }

    }
}
