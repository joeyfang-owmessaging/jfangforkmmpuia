package utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.testng.Reporter;

import tests.BaseTest;

/**
 * Prints typed log information to TestNG report as well as console output.
 */
public class Logging {

	private static String getSessionID() {
		String sessionID = BaseTest.base.getDriver().toString();
		sessionID = sessionID.substring(sessionID.lastIndexOf('(') + 1);
		sessionID = sessionID.substring(0, 4);
		return sessionID;
	}

	private static void log(String logType, String logInfo) {
		String typeLabel = "";
		String sessionId = "null-session";
		String output;

		if (logType.equals("desc")) {
			typeLabel = "DESCRIPTION";
			logInfo = "[[ " + logInfo + " ]]";
		} else if (logType.equals("params")) {
			typeLabel = "PARAMS";
			logInfo = "[[ " + logInfo + " ]]";
		} else if (logType.equals("info")) {
			typeLabel = "INFO";
		} else if (logType.equals("warn")) {
			typeLabel = "WARN";
		} else if (logType.equals("error")) {
			typeLabel = "ERROR";
		} else if (logType.equals("status")) {
			typeLabel = "STATUS";
			logInfo = "[[ " + logInfo + " ]]";
		} else if (logType.equals("debug")) {
			typeLabel = "Debug";
		}

		// Get time stamp.
		LocalDateTime dt = LocalDateTime.now();
		String timeStamp = dt.format(DateTimeFormatter.ofPattern("HH:mm:ss"));

		// Get WebDriver session ID if Base has been instantiated.
		if (BaseTest.base != null) {
			sessionId = Logging.getSessionID();
		}

		output = "[" + timeStamp + "][" + sessionId + "][" + typeLabel + "] " + logInfo;
		Reporter.log(output);
		System.out.println(output);
	}

	// Log type definitions.
	public static void description(String log) {
		Logging.log("desc", log);
	}

	public static void info(String log) {
		Logging.log("info", log);
	}

	public static void error(String log) {
		Logging.log("error", log);
	}

	public static void warn(String log) {
		Logging.log("warn", log);
	}

	public static void params(String log) {
		Logging.log("params", log);
	}

	public static void status(String log) {
		Logging.log("status", log);
	}

	public static void debug(String log) {
		Logging.log("debug", log);
	}

}
