package utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertyFileReader {

	public static Properties properties;

	public static String getProperty(String filename, String key) {
		String filePath = System.getProperty("user.dir") + "/src/test/resources/" + filename;
		BufferedReader reader;
		{
			try {
				reader = new BufferedReader(new FileReader(filePath));
				properties = new Properties();
				try {
					properties.load(reader);
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

		}
		String value = properties.getProperty(key);
		return value;

	}
}
