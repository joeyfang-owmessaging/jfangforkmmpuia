package utils;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Screenshot {

    public static String capture(WebDriver driver,String screenshotName) throws IOException{

        //String date = new SimpleDateFormat("yyyy-MM-dd-hh:mm:ss").format(new Date());
        String date = new SimpleDateFormat("yyyy-MM-dd-hh_mm_ss").format(new Date());
        TakesScreenshot takesScreenshot = (TakesScreenshot) driver;
        File sourceFile = takesScreenshot.getScreenshotAs(OutputType.FILE);        
        String destinationPath = System.getProperty("user.dir")+"\\Screenshots\\ErrorScreenshot_"+screenshotName+"_"+date+".png";
        File destinationFile = new File(destinationPath);
        FileUtils.copyFile(sourceFile,destinationFile);      
        FileInputStream fis = new FileInputStream(sourceFile);
        byte byteArray[] = new byte[(int)destinationFile.length()];
        fis.read(byteArray);
        String imageDtring = Base64.encodeBase64String(byteArray);
        return imageDtring;
    }
            /*//decode Base64 String to image
            FileOutputStream fos = new FileOutputStream("H:/decode/destinationImage.png"); //change path of image according to you
            byteArray = Base64.decodeBase64(imageString);
            fos.write(byteArray);

            fis.close();
            fos.close();*/
}
