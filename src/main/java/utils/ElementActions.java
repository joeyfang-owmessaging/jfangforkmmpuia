package utils;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import tests.BaseTest;

public class ElementActions {

	public static void scrollTo(WebDriver driver, WebElement element) {

		RemoteWebDriver r = (RemoteWebDriver) driver;
		Point l = element.getLocation();
		int y = l.getY();
		float f = 567;
		String s = "window.scrollTo(0," + y + ")";
		r.executeScript(s);

	}

	public static boolean elementDisplayed(WebElement element) {
		try {
			if (element.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public static boolean click(WebElement element) {

		if (elementDisplayed(element)) {
			element.click();
			return true;
		} else {
			return false;
		}

	}

	public static boolean sendKeys(WebElement element, String input) {

		if (elementDisplayed(element)) {
			element.sendKeys(input);
			return true;
		} else {
			return false;
		}

	}

	public static String getText(WebElement element) {

		if (elementDisplayed(element)) {
			return element.getText();
		} else {
			return null;
		}

	}

	public static String getEnteredText(WebElement element) {

		if (elementDisplayed(element)) {
			return element.getAttribute("value");
		} else {
			return null;
		}

	}

	/**
	 * @Method Name : waitUntilElementDisplayedByXpath
	 * @Author : Joey Fang
	 * @Created on : 07/26/2018
	 * @Description : This method will detect and wait for the expected element to
	 *              be displayed in view with the specified timeout value.
	 * @Parameter1 : String element - XPath expression of the element
	 * @Parameter2 : int timeoutInSeconds - timeout value in seconds
	 * @Return: boolean - true: element displayed / false: element not displayed
	 */
	public static boolean waitUntilElementDisplayedByXpath(String element, int timeoutInSeconds) {
		// Set WebDriver's wait time to 1 second. So each iteration will wait for at
		// most 1 second.
		/*
		 * WebDriver driver = null ; driver.manage().timeouts().implicitlyWait(1,
		 * TimeUnit.SECONDS);
		 */
		BaseTest.base.getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

		boolean isDisplayed = false;
		try {
			Logging.info("Finding element xpath:[ " + element + "] within " + timeoutInSeconds + " seconds");
			for (int i = 0; i < timeoutInSeconds; i++) {
				try {
					WebElement elem = BaseTest.base.getDriver().findElement(By.xpath(element));
					// Element is found, check its visibility.
					if (elem.isDisplayed()) {
						isDisplayed = true;
						Logging.info(
								"Element xpath:[ " + element + "] is found within " + timeoutInSeconds + " seconds");
						break;
					} else {
						Logging.info("Waiting..");
						Thread.sleep(1000);
					}

				} catch (NoSuchElementException ex) {
					// Element not found, go to next iteration.
				} catch (StaleElementReferenceException ex) {
					// Element not attached to DOM any more, leave for checking in next iteration.
					// Sleep for 1 second in case wait was not performed in previous code.
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();

				}
			}

			if (!isDisplayed) {
				Logging.info(
						"Element still not displayed after " + timeoutInSeconds + " seconds, XPath: " + element);
			}

		} catch (WebDriverException ex) {
			// Catch all WebDriver exceptions.
			throw ex;
		} finally {
			// Reset WebDriver's wait time to default.
			BaseTest.base.getDriver().manage().timeouts().implicitlyWait(Integer.parseInt(tests.BaseTest.timeout),
					TimeUnit.SECONDS);

		}

		return isDisplayed;
	}

	/**
	 * <b>Author</b>: Joey Fang <br>
	 * <b>Date</b>: 07/26/2018 <br>
	 * <b>Description</b><br>
	 * set the following WebDriver values to be default:<br>
	 * - focused content of the locator<br>
	 * - implicit wait timeout<br>
	 * 
	 * @return void
	 */
	public static void setDriverDefaultValues() {
		try {
			BaseTest.base.getDriver().switchTo().defaultContent();
			BaseTest.base.getDriver().manage().timeouts().implicitlyWait(Long.parseLong(tests.BaseTest.timeout),
					TimeUnit.SECONDS);
		} catch (WebDriverException ex) {
			Logging.info("Fail to set WebDriver to default values");
		}
	}

	/**
	 * <b>Author: </b>Joey Fang<br>
	 * <b>Date: </b>07/26/2018<br>
	 * <b>Description: </b><br>
	 * Check or uncheck a checkbox field.
	 */
	public static void clickOnCheckbox(String checkbox, Boolean check) {
		checkOrUncheck(checkbox, check);
	}

	/**
	 * <b>Author: </b>Joey Fang<br>
	 * <b>Date: </b>07/26/2018<br>
	 * <b>Description: </b><br>
	 * Used by page objects to check or uncheck a checkbox. Note: this method can
	 * not be sued by 'checker' elements such as email item checkers preceding to an
	 * email message.
	 */
	public static void checkOrUncheck(String checkboxXpath, Boolean check) {
		Logging.info((check ? "Checking" : "Unchecking"));

		try {
			setDriverDefaultValues();

			// Checkbox is enclosed in a 'span' element, with its enclosing
			// table element has the following
			// css class pattern.
			String ancestorTable = checkboxXpath + "/input[contains(@class,'mat-checkbox-input cdk-visually-hidden')]";

			boolean isEnabled = BaseTest.base.getDriver().findElement(By.xpath(ancestorTable))
					.getAttribute("aria-checked").contains("true");

			if ((check && !isEnabled) || (!check && isEnabled)) {
				BaseTest.base.getDriver().findElement(By.xpath(checkboxXpath)).click();
			}
		} catch (WebDriverException ex) {
			Logging.info("Could not " + (check ? "Check" : "Uncheck"));
			throw ex;
		}
	}

	/**
	 * @Method Name : scrollElementIntoViewByXpath
	 * @Author : Joey Fang
	 * @Created on : 07/31/2018
	 * @Description : This method will scroll the specified web element into view by
	 *              executing javascript code.
	 * @Parameter : String element - XPath expression of the element
	 */
	public static void scrollElementIntoViewByXpath(String element) {
		try {
			int i = 0;
			while (i < 5) {
				try {
					WebElement destElement = BaseTest.base.getDriver().findElement(By.xpath(element));
					((JavascriptExecutor) BaseTest.base.getDriver()).executeScript("arguments[0].scrollIntoView(true);",
							destElement);
					// To quit loop.
					i = 5;
				} catch (StaleElementReferenceException ex) {
					i += 1;
				}
				// Wait for 500 milliseconds to let the view update.
				Thread.sleep(500);
			}

		} catch (NoSuchElementException ex) {
			Logging.info("Could not scroll element into view; element not found with xpath: " + element);
			// Do not throw exception here.
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @Method Name : scrollToBottomOfPage
	 * @Author : Joey Fang
	 * @Created on : 07/31/2018
	 * @Description : This method will scroll to the bottom of a page
	 * @Parameter :
	 */
	public static void scrollToBottomOfPage() {
		try {
			int i = 0;
			while (i < 5) {
				try {
					((JavascriptExecutor) BaseTest.base.getDriver())
							.executeScript("window.scrollTo(0,document.body.scrollHeight)");
					// To quit loop.
					i = 5;
				} catch (StaleElementReferenceException ex) {
					i += 1;
				}
				// Wait for 500 milliseconds to let the view update.
				Thread.sleep(500);
			}

		} catch (NoSuchElementException ex) {
			Logging.info("Could not scroll to the bottom of the page");
			// Do not throw exception here.
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @Method Name : scrollToTopOfPage
	 * @Author : Joey Fang
	 * @Created on : 07/31/2018
	 * @Description : This method will scroll to the top of a page
	 * @Parameter :
	 */
	public static void scrollToTopOfPage() {
		try {
			int i = 0;
			while (i < 5) {
				try {
					((JavascriptExecutor) BaseTest.base.getDriver())
							.executeScript("window.scrollTo(document.body.scrollHeight, 0)");
					// To quit loop.
					i = 5;
				} catch (StaleElementReferenceException ex) {
					i += 1;
				}
				// Wait for 500 milliseconds to let the view update.
				Thread.sleep(500);
			}

		} catch (NoSuchElementException ex) {
			Logging.info("Could not scroll to the top of the page");
			// Do not throw exception here.
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	
	public static String getContent(String element) {
		
		String content = null; 
		try {
			BaseTest.base.getDriver().switchTo().defaultContent();
			content = BaseTest.base.getDriver().findElement(By.xpath(element)).getText();
		} catch (WebDriverException ex) {
			Logging.info("Failed to get the content");
		}
		return content;
	}
	
	
	/**
	 * @Method Name : getTableValue
	 * @Author : Joey Fang
	 * @Created on : 08/06/2018
	 * @Description : This method will get the table values
	 * @Parameter :
	 */
	public static ArrayList <String> getTableValue(String moduleProperities, String tableXpath) {
		
		ArrayList <String> tableValues = new ArrayList<String>();
		
		try {
			Logging.info("Prepare to get the table values");
			String getTableXpath = PropertyFileReader.getProperty(moduleProperities,tableXpath);
			List<WebElement> rows = BaseTest.base.getDriver().findElements(By.xpath(getTableXpath));
			for(WebElement row:rows) {
				List<WebElement> cols= row.findElements(By.tagName("td"));
				  for(WebElement col:cols){
					  Logging.info("td is: "+col.getText());
					  tableValues.add(col.getText());
				  }
			  }
			BaseTest.base.getDriver().switchTo().defaultContent();
			

		} catch (NoSuchElementException ex) {
			Logging.info("Could not get the table values");
			throw ex;
		} 
		return tableValues;
	}
}