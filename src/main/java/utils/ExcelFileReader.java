package utils;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.FileInputStream;


public class ExcelFileReader {

    public static String getData(String path, String sheet, int row, int cell) {

        String data = null;
        try {
            FileInputStream fis = new FileInputStream(path);
            Workbook wb = WorkbookFactory.create(fis);
            data = wb.getSheet(sheet).getRow(row).getCell(cell).toString();

        } catch (Exception e) {
            e.printStackTrace();

        }
        return data;

    }

}
