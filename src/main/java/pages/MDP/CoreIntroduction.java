package pages.MDP;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;

import utils.ElementActions;
import utils.Logging;
import utils.PropertyFileReader;

public class CoreIntroduction {

	/**
	 * @Method Name : getIntroductionHeaderContent
	 * @author : Joey Fang
	 * @Created on : 08/02/2018
	 * @Description : This method will get the header content in Introduction
	 * @Parameter 
	 * @Return: header content
	 */
	public String getIntroductionHeaderContent(String headerNumber) {

		String headerContent = null;

		try {
			Logging.info("Prepare to get the header content in Introduction");
			
			ElementActions.setDriverDefaultValues();

			String header = PropertyFileReader.getProperty("MDPIntroduction.properties",
					"header").replace("+variable+", headerNumber);

			 headerContent = ElementActions.getContent(header);
			
			 Logging.info("The header content is: "+headerContent);
		} catch (WebDriverException ex) {
			Logging.info("Error occurs when get the header content");
			throw ex;
		}

		return headerContent;
		
	}
	
	
	/**
	 * @Method Name : getIntroductionParagraphContent
	 * @author : Joey Fang
	 * @Created on : 08/02/2018
	 * @Description : This method will get the paragraph content in Introduction
	 * @Parameter 
	 * @Return: header content
	 */
	public String getIntroductionParagraphContent(String paragraphNumber) {

		String paragraphContent = null;

		try {
			Logging.info("Prepare to get the paragraph content in Introduction");
			
			ElementActions.setDriverDefaultValues();

			String paragrap = PropertyFileReader.getProperty("MDPIntroduction.properties",
					"paragraph").replace("+variable+", paragraphNumber);
			Logging.info("paragrap  is: "+paragrap);
			paragraphContent = ElementActions.getContent(paragrap);
			
			 Logging.info("The paragraph content is: "+paragraphContent);
		} catch (WebDriverException ex) {
			Logging.info("Error occurs when get the paragraph content");
			throw ex;
		}

		return paragraphContent;
		
	}
	
	
	/**
	 * @Method Name : verifyIntroductionContainsContent
	 * @author : Joey Fang
	 * @Created on : 08/02/2018
	 * @Description : This method will verify the paragraph content in Introduction
	 * @Parameter 
	 * @Return: header content
	 */
	public boolean verifyIntroductionContainsContent(String expectContent, String actualContent) {

		boolean result = false;

		Logging.info("Prepare to verify content in Introduction");
		
		try {
			
			if(actualContent.trim().contains(expectContent.trim())) {
				
				result = true;
				
				Logging.info("Content in Introduction is same with expected");
			}


		} catch (WebDriverException ex) {
			Logging.info("Error occurs when verify the paragraph content");
			throw ex;
		}

		return result;
		
	}
	
}
