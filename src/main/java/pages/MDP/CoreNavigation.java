package pages.MDP;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;

import tests.BaseTest;
import utils.ElementActions;
import utils.Logging;
import utils.PropertyFileReader;

public class CoreNavigation {

	/**
	 * <b>Author</b>: Joey Fang<br>
	 * <b>Date</b>: 07/30/2018<br>
	 * <b>Description</b><br>
	 * Clicks on the specified navigation tab.
	 * 
	 * @param tab
	 *            tab name
	 */
	private void clickNavigationTab(String tab) {

		Logging.info("Clicking on " + tab + " tab");
		String naviCommomTab = PropertyFileReader.getProperty("MDPHomePage.properties", "naviCommomTab")
				.replace("+variable+", tab);

		try {
			ElementActions.setDriverDefaultValues();
			if (ElementActions.waitUntilElementDisplayedByXpath(naviCommomTab,
					Integer.parseInt(tests.BaseTest.timeout))) {
				BaseTest.base.getDriver().findElement(By.xpath(naviCommomTab)).click();
			}

		} catch (WebDriverException ex) {
			Logging.info("Could not click on " + tab + " tab");
			throw ex;
		}
	}

	/**
	 * @Method_Name: clickAPIsTab
	 * @Author: Joey Fang
	 * @Created_Date: 07/30/2018
	 * @Description: This method simulates the user clicking on "APIs" tab on
	 *               Navigation pane.
	 */
	public void clickAPIsTab() {
		clickNavigationTab("APIs");
		Logging.info("Clicked the APIs tab");
	}

	
	/**
	 * @Method Name : verifyUserIDContains
	 * @author : Joey Fang
	 * @Created on : 08/03/2018
	 * @Description : This method will verify the userID in Home Page
	 * @Parameter 
	 * @Return: 
	 */
	public boolean verifyUserIDContains(String expectUserID) {

		boolean result = false;
		String MDPHomeUserID = PropertyFileReader.getProperty("MDPHomePage.properties", "MDPHomeUserID");

		Logging.info("Prepare to verify the User ID in MDP Home page");
		
		try {
			ElementActions.setDriverDefaultValues();
			
			String actualUserID = BaseTest.base.getDriver().findElement(By.xpath(MDPHomeUserID)).getText();
			if(expectUserID.trim().equals(actualUserID.trim())) {
				
				result = true;
				
				Logging.info("Verified User ID in MDP Home page pass");
			}


		} catch (WebDriverException ex) {
			Logging.info("Error occurs when verify the User ID in MDP Home page");
			throw ex;
		}

		return result;
		
	}
	
	/**
	 * @Method Name : verifySubscribersContains
	 * @author : Joey Fang
	 * @Created on : 08/03/2018
	 * @Description : This method will verify the subscribersNumber in Home Page
	 * @Parameter 
	 * @Return: 
	 */
	public boolean verifySubscribersContains(String expectSubscribersNumber) {

		boolean result = false;
		String MDPHomeSubscibers = PropertyFileReader.getProperty("MDPHomePage.properties", "MDPHomeSubscibers");

		Logging.info("Prepare to verify the Subscibers in MDP Home page");
		
		try {
			ElementActions.setDriverDefaultValues();
			
			String ActualMDPHomeSubscibers = BaseTest.base.getDriver().findElement(By.xpath(MDPHomeSubscibers)).getText();
			if(expectSubscribersNumber.trim().equals(ActualMDPHomeSubscibers.trim())) {
				
				result = true;
				
				Logging.info("Verified Subscibers in MDP Home page pass");
			}


		} catch (WebDriverException ex) {
			Logging.info("Error occurs when verify the Subscibers in MDP Home page");
			throw ex;
		}

		return result;
		
	}
	
	
	/**
	 * @Method Name : verifyMDPHomePlanLoaded
	 * @author : Joey Fang
	 * @Created on : 08/06/2018
	 * @Description : This method will verify whether the MDP Home Plan loaded
	 *              correctly after login.
	 * @Parameter 1: int timeoutInSeconds - value specified in seconds to wait
	 * @Return: boolean - true: page loaded / false: page not loaded
	 */
	public boolean verifyMDPHomePlanLoaded(int timeoutInSeconds) {

		boolean result = false;
		String MDPHomePlan = PropertyFileReader.getProperty("MDPHomePage.properties", "MDPHomePlan");

		try {
			boolean flag1 = ElementActions.waitUntilElementDisplayedByXpath(MDPHomePlan, timeoutInSeconds);
			if (!flag1) {
				throw new NoSuchElementException("");
			}

			Logging.info("MDP Home plan loaded");

			// All core elements loaded.
			result = true;
		} catch (WebDriverException ex) {
			Logging.info("MDP Home plan could not be confirmed as loaded");
			throw ex;
		}

		if (result) {
			Logging.info("MDP Home plan loaded");
		}
		return result;
	}

}
