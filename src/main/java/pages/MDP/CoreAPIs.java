package pages.MDP;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import tests.BaseTest;
import utils.ElementActions;
import utils.Logging;
import utils.PropertyFileReader;

public class CoreAPIs {



	/**
	 * <b>Author</b>: Joey Fang<br>
	 * <b>Date</b>: 07/30/2018<br>
	 * <b>Description</b><br>
	 * Clicks on the specified navigation tab.
	 * 
	 * @param tab
	 *            tab name
	 */
	private void clickAPIsSection(String section) {

		Logging.info("Clicking on " + section + " tab");
		String APIsSection = PropertyFileReader.getProperty("MDPAPIsPage.properties", "APIsSection")
				.replace("+variable+", section);
		Logging.info("APIsSection is: " + APIsSection);
		try {
			ElementActions.setDriverDefaultValues();
			if (ElementActions.waitUntilElementDisplayedByXpath(APIsSection,
					Integer.parseInt(tests.BaseTest.timeout))) {
				BaseTest.base.getDriver().findElement(By.xpath(APIsSection)).click();
			}

		} catch (WebDriverException ex) {
			Logging.info("Could not click on " + section + " section");
			throw ex;
		}
	}

	/**
	 * @Method_Name: clickAccountMangerSection
	 * @Author: Joey Fang
	 * @Created_Date: 07/30/2018
	 * @Description: This method simulates the user clicking on "APIs" tab on
	 *               Navigation pane.
	 */
	public void clickAccountMangerSection() {
		clickAPIsSection("Account Manager");
		Logging.info("Clicked the Account Manager tab");
	}

	public void clickAuthenticationSection() {
		clickAPIsSection("Authentication");
		Logging.info("Clicked the Authentication tab");
	}

	public void clickCampaignManagerSection() {
		clickAPIsSection("Campaign Manager");
		Logging.info("Clicked the Campaign Manager tab");
	}

	public void clickSurveysSection() {
		clickAPIsSection("Surveys");
		Logging.info("Clicked the Surveys tab");
	}

	public void clickCouponsSection() {
		clickAPIsSection("Coupons");
		Logging.info("Clicked the Coupons tab");
	}

	public void clickErrorSection() {
		clickAPIsSection("Error");
		Logging.info("Clicked the Error tab");
	}

	/**
	 * @Method Name : verifyAccountManagerPageLoaded
	 * @author : Joey Fang
	 * @Created on : 07/30/2018
	 * @Description : This method will verify whether the AccountManager page loaded
	 *              correctly.
	 * @Parameter 1: int timeoutInSeconds - value specified in seconds to wait
	 * @Return: boolean - true: page loaded / false: page not loaded
	 */
	public boolean verifyAccountManagerHeaderLoaded(int timeoutInSeconds) {

		boolean result = false;

		try {
			String accountManagerHeader = PropertyFileReader.getProperty("MDPAPIsPage.properties",
					"accountManagerHeader");

			boolean flag1 = ElementActions.waitUntilElementDisplayedByXpath(accountManagerHeader, timeoutInSeconds);
			if (!flag1) {
				throw new NoSuchElementException("");
			}

			Logging.info("Account Manager Header loaded");

			// All core elements loaded.
			result = true;
		} catch (WebDriverException ex) {
			Logging.info("Account Manager Header could not be confirmed as loaded");
			throw ex;
		}

		if (result) {
			Logging.info("MDP Home page loaded");
		}
		return result;
	}

	/**
	 * @Method Name : verifyErrorsHeaderLoaded
	 * @author : Joey Fang
	 * @Created on : 08/07/2018
	 * @Description : This method will verify whether the Errors page loaded
	 *              correctly.
	 * @Parameter 1: int timeoutInSeconds - value specified in seconds to wait
	 * @Return: boolean - true: page loaded / false: page not loaded
	 */
	public boolean verifyErrorsHeaderLoaded(int timeoutInSeconds) {

		boolean result = false;

		try {
			String errorsHeader = PropertyFileReader.getProperty("MDPAPIsPage.properties",
					"errorsHeader");

			boolean flag1 = ElementActions.waitUntilElementDisplayedByXpath(errorsHeader, timeoutInSeconds);
			if (!flag1) {
				throw new NoSuchElementException("");
			}

			Logging.info("Errors Header loaded");

			// All core elements loaded.
			result = true;
		} catch (WebDriverException ex) {
			Logging.info("Errors Header could not be confirmed as loaded");
			throw ex;
		}

		if (result) {
			Logging.info("Errors page loaded");
		}
		return result;
	}
	
	public void scrollToAPIsTopPage() {

		try {
			Logging.info("Prepare to scroll to top of APIs page");
			ElementActions.scrollToTopOfPage();
		}

		catch (WebDriverException ex) {
			Logging.info("Could not scroll to APIs top page");
			throw ex;
		}
	}

	public void scrollToAPIsBottomPage() {

		try {
			Logging.info("Prepare to scroll to bottom of APIs page");
			ElementActions.scrollToBottomOfPage();
		}

		catch (WebDriverException ex) {
			Logging.info("Could not scroll to APIs bottom page");
			throw ex;
		}
	}

	
	/**
	 * @Method Name : getAuthenticationParagraphContent
	 * @author : Joey Fang
	 * @Created on : 08/03/2018
	 * @Description : This method will get the paragraph content in Authentication
	 * @Parameter 
	 * @Return: paragraph content
	 */
	public String getAuthenticationParagraphContent(String paragraphNumber) {

		String paragraphContent = null;

		try {
			Logging.info("Prepare to get the paragraph content in Authentication");
			
			ElementActions.setDriverDefaultValues();

			String paragrap = PropertyFileReader.getProperty("MDPAPIsPage.properties",
					"authenticationParagraph").replace("+variable+", paragraphNumber);
			Logging.info("paragrap  is: "+paragrap);
			paragraphContent = ElementActions.getContent(paragrap);
			
			 Logging.info("The paragraph content is: "+paragraphContent);
		} catch (WebDriverException ex) {
			Logging.info("Error occurs when get the paragraph content");
			throw ex;
		}

		return paragraphContent;
		
	}
	
	
	/**
	 * @Method Name : verifyAuthenticationContainsContent
	 * @author : Joey Fang
	 * @Created on : 08/02/2018
	 * @Description : This method will verify the paragraph content in Introduction
	 * @Parameter 
	 * @Return: header content
	 */
	public boolean verifyAuthenticationContainsContent(String expectContent, String actualContent) {

		boolean result = false;

		Logging.info("Prepare to verify content in Authentication");
		
		try {
			
			if(actualContent.trim().contains(expectContent.trim())) {
				
				result = true;
				
				Logging.info("Content in Authentication is same with expected");
			}


		} catch (WebDriverException ex) {
			Logging.info("Error occurs when verify the paragraph content");
			throw ex;
		}

		return result;
		
	}
	
	/**
	 * @Method Name : verifyAPIsArrayListContentSame
	 * @author : Joey Fang
	 * @Created on : 08/06/2018
	 * @Description : This method will verify if the two arraylists contents are same in APIs
	 * @Parameter 
	 * @Return: 
	 */
	public boolean verifyAPIsArrayListContentSame(ArrayList<String> expectList, ArrayList<String> actualList) {

		boolean result = false;

		Logging.info("Prepare to verify the two arraylists content in APIs");

		try {

			for (int i = 0; i <expectList.size(); i++) {
				if (!(expectList.get(i).trim().equals(actualList.get(i).trim()))) {
					
					Logging.info("expectList.size() is: " + expectList.size());
					Logging.info("expectList.get(i).trim() is: " + expectList.get(i).trim());
					Logging.info("actualList.get(i).trim() is: " + actualList.get(i).trim());
					Logging.info("The two arraylist contents are same in APIs");
					result = false;
					break;
					
				} else {
					
					result = true;
				}
				
			}

		} catch (WebDriverException ex) {
			Logging.info("Error occurs when verify the contents");
			throw ex;
		}

		return result;

	}

	
	/**
	 * @Method Name : getAuthenticationMessageContent
	 * @author : Joey Fang
	 * @Created on : 08/03/2018
	 * @Description : This method will get the message content in Authentication
	 * @Parameter 
	 * @Return: paragraph content
	 */
	public String getAuthenticationMessageContent() {

		String messageContent = getAPIsContent("authentication","message");

/*		try {
			Logging.info("Prepare to get the message content in Authentication");
			
			ElementActions.setDriverDefaultValues();

			String message = PropertyFileReader.getProperty("MDPAPIsPage.properties",
					"authenticationMessage");
			Logging.info("message  is: "+message);
			messageContent = ElementActions.getContent(message);
			
			 Logging.info("The message content is: "+messageContent);
		} catch (WebDriverException ex) {
			Logging.info("Error occurs when get the message content");
			throw ex;
		}*/

		return messageContent;
		
	}
	
	/**
	 * @Method Name : getAuthenticationWarningContent
	 * @author : Joey Fang
	 * @Created on : 08/03/2018
	 * @Description : This method will get the warning content in Authentication
	 * @Parameter 
	 * @Return: paragraph content
	 */
	public String getAuthenticationWarningContent() {

		String warningContent = getAPIsContent("authentication","warning");

/*		try {
			Logging.info("Prepare to get the warning content in Authentication");
			
			ElementActions.setDriverDefaultValues();

			String warning = PropertyFileReader.getProperty("MDPAPIsPage.properties",
					"authenticationWarning");
			Logging.info("warning  is: "+warning);
			warningContent = ElementActions.getContent(warning);
			
			 Logging.info("The warning content is: "+warningContent);
		} catch (WebDriverException ex) {
			Logging.info("Error occurs when get the warning content");
			throw ex;
		}
*/
				
		return warningContent;
		
	}
	
	
	/**
	 * @Method Name : getAPIsContent
	 * @author : Joey Fang
	 * @Created on : 08/03/2018
	 * @Description : This method will get the content in APIs
	 * @Parameter 
	 * @Return:  content
	 */
	public String getAPIsContent(String APIsModule, String warningOrMessage ) {

		String content = null;

		try {
			Logging.info("Prepare to get the "+warningOrMessage+" content in "+APIsModule);
			
			ElementActions.setDriverDefaultValues();

			String element = PropertyFileReader.getProperty("MDPAPIsPage.properties",
					"APIsWarningOrMessageContent").replace("+APIsModule+", APIsModule).replace("+warningOrMessage+", warningOrMessage);
			Logging.info("Element  is: "+element);
			content = ElementActions.getContent(element);
			
			 Logging.info("The "+warningOrMessage+" content in "+APIsModule+" is: "+content);
		} catch (WebDriverException ex) {
			Logging.info("Error occurs when get the content");
			throw ex;
		}

		return content;
		
	}
	
	
	/**
	 * @Method Name : getAccountAPIsContentsList
	 * @author : Joey Fang
	 * @Created on : 08/06/2018
	 * @Description : This method will get the contents in APIs
	 * @Parameter 
	 * @Return: array list contents
	 */
	public ArrayList<String> getAccountAPIsContentsList(String APIsModule, String className) {

		ArrayList<String> contents = new ArrayList<String>();

		try {
			Logging.info("Prepare to get the contents in APIs, APIsModule is: "+APIsModule+" and className is: "+className);
			
			ElementActions.setDriverDefaultValues();

			//ArrayList<String> contentsElements = new ArrayList<String>();
			String contentElementXpath = PropertyFileReader.getProperty("MDPAPIsPage.properties",
					"contentsElements").replace("+APIsModule+", APIsModule).replace("+className+", className);
			List<WebElement> elements = BaseTest.base.getDriver().findElements(By.xpath(contentElementXpath));
			for (WebElement el : elements) {
				contents.add(el.getText());
		      }
						
			 Logging.info("The contents of "+className+" in "+APIsModule+" is: "+contents);
		} catch (WebDriverException ex) {
			Logging.info("Error occurs when get the contents");
			throw ex;
		}

		return contents;
		
	}
	
	
	/**
	 * @Method Name : getAuthenticationCodeTypeContentsList
	 * @author : Joey Fang
	 * @Created on : 08/06/2018
	 * @Description : This method will get the code type contents in APIs
	 * @Parameter 
	 * @Return: array list contents
	 */
	public ArrayList<String> getAuthenticationCodeTypeContentsList(String codeType) {

		ArrayList<String> contents = new ArrayList<String>();

		try {
			Logging.info("Prepare to get the "+codeType+" contents in Authentication");
			
			ElementActions.setDriverDefaultValues();

			String contentElementXpath = PropertyFileReader.getProperty("MDPAPIsPage.properties",
					"authenticationCodeTypeContent").replace("+codeType+", codeType);
			List<WebElement> elements = BaseTest.base.getDriver().findElements(By.xpath(contentElementXpath));
			for (WebElement el : elements) {
				contents.add(el.getText());
		      }
						
			 Logging.info("The contents of "+codeType+" code type is: "+contents);
		} catch (WebDriverException ex) {
			Logging.info("Error occurs when get the contents");
			throw ex;
		}

		return contents;
		
	}
	
	
	
	/**
	 * @Method Name : getAccountManagerQueryParametersTable
	 * @author : Joey Fang
	 * @Created on : 08/06/2018
	 * @Description : This method will get the getAccountManagerQueryParametersTable in APIs
	 * @Parameter 
	 * @Return: array list contents
	 */
	public ArrayList<String> getAccountManagerQueryParametersTable () {

		ArrayList<String> tableContents = new ArrayList<String>();

		try {
			Logging.info("Prepare to get the table contents in Query Parameters");
			
			ElementActions.setDriverDefaultValues();

			tableContents = ElementActions.getTableValue("MDPAPIsPage.properties", "accountManagerQueryParametersTable");
			
			 Logging.info("The contents of Query Parameters table is: "+tableContents);
		} catch (WebDriverException ex) {
			Logging.info("Error occurs when get the contents");
			throw ex;
		}

		return tableContents;
		
	}
	
	
	/**
	 * @Method Name : getAccountManagerQueryParametersTable
	 * @author : Joey Fang
	 * @Created on : 08/06/2018
	 * @Description : This method will get the getAccountManagerQueryParametersTable in APIs
	 * @Parameter 
	 * @Return: array list contents
	 */
	public ArrayList<String> getAccountManagerURLParametersTable () {

		ArrayList<String> tableContents = new ArrayList<String>();

		try {
			Logging.info("Prepare to get the table contents in URL Parameters");
			
			ElementActions.setDriverDefaultValues();

			tableContents = ElementActions.getTableValue("MDPAPIsPage.properties", "accountManagerURLParametersTable");
			
			 Logging.info("The contents of URL Parameters table is: "+tableContents);
		} catch (WebDriverException ex) {
			Logging.info("Error occurs when get the contents");
			throw ex;
		}

		return tableContents;
		
	}
	
	
	/**
	 * @Method Name : getErrorsCreateOrganisationTable
	 * @author : Joey Fang
	 * @Created on : 08/07/2018
	 * @Description : This method will get the createOrganisation in APIs
	 * @Parameter 
	 * @Return: array list contents
	 */
	public ArrayList<String> getErrorsCreateOrganisationTable () {

		ArrayList<String> tableContents = new ArrayList<String>();

		try {
			Logging.info("Prepare to get the table contents in createOrganisation");
			
			ElementActions.setDriverDefaultValues();

			tableContents = ElementActions.getTableValue("MDPAPIsPage.properties", "errorsCreateOrganisationTable");
			
			 Logging.info("The contents of createOrganisation table is: "+tableContents);
		} catch (WebDriverException ex) {
			Logging.info("Error occurs when get the contents");
			throw ex;
		}

		return tableContents;
		
	}
	
	/**
	 * @Method Name : getErrorsPublishOrganisationFirstTable
	 * @author : Joey Fang
	 * @Created on : 08/07/2018
	 * @Description : This method will get the getErrorsPublishOrganisationFirstTable in APIs
	 * @Parameter 
	 * @Return: array list contents
	 */
	public ArrayList<String> getErrorsPublishOrganisationFirstTable () {

		ArrayList<String> tableContents = new ArrayList<String>();

		try {
			Logging.info("Prepare to get the table contents in publishOrganisation");
			
			ElementActions.setDriverDefaultValues();

			tableContents = ElementActions.getTableValue("MDPAPIsPage.properties", "errorsPublishOrganisationFirstTable");
			
			 Logging.info("The contents of publishOrganisation table is: "+tableContents);
		} catch (WebDriverException ex) {
			Logging.info("Error occurs when get the contents");
			throw ex;
		}

		return tableContents;
		
	}
	
	/**
	 * @Method Name : getErrorsPublishOrganisationSecondTable
	 * @author : Joey Fang
	 * @Created on : 08/07/2018
	 * @Description : This method will get the createOrganisation in APIs
	 * @Parameter 
	 * @Return: array list contents
	 */
	public ArrayList<String> getErrorsPublishOrganisationSecondTable () {

		ArrayList<String> tableContents = new ArrayList<String>();

		try {
			Logging.info("Prepare to get the table contents in publishOrganisation");
			
			ElementActions.setDriverDefaultValues();

			tableContents = ElementActions.getTableValue("MDPAPIsPage.properties", "errorsPublishOrganisationSecondTable");
			
			 Logging.info("The contents of publishOrganisation table is: "+tableContents);
		} catch (WebDriverException ex) {
			Logging.info("Error occurs when get the contents");
			throw ex;
		}

		return tableContents;
		
	}
	
	/**
	 * @Method Name : getErrorsGetOrganisationAccountsTable
	 * @author : Joey Fang
	 * @Created on : 08/07/2018
	 * @Description : This method will get the getErrorsGetOrganisationAccountsTable in APIs
	 * @Parameter 
	 * @Return: array list contents
	 */
	public ArrayList<String> getErrorsGetOrganisationAccountsTable () {

		ArrayList<String> tableContents = new ArrayList<String>();

		try {
			Logging.info("Prepare to get the table contents in getOrganisationAccounts");
			
			ElementActions.setDriverDefaultValues();

			tableContents = ElementActions.getTableValue("MDPAPIsPage.properties", "errorsGetOrganisationAccountsTable");
			
			 Logging.info("The contents of getOrganisationAccounts table is: "+tableContents);
		} catch (WebDriverException ex) {
			Logging.info("Error occurs when get the contents");
			throw ex;
		}

		return tableContents;
		
	}
	
	/**
	 * @Method Name : getErrorsGetOrganisationTable
	 * @author : Joey Fang
	 * @Created on : 08/07/2018
	 * @Description : This method will get the getErrorsGetOrganisationTable in APIs
	 * @Parameter 
	 * @Return: array list contents
	 */
	public ArrayList<String> getErrorsGetOrganisationTable () {

		ArrayList<String> tableContents = new ArrayList<String>();

		try {
			Logging.info("Prepare to get the table contents in getOrganisation");
			
			ElementActions.setDriverDefaultValues();

			tableContents = ElementActions.getTableValue("MDPAPIsPage.properties", "errorsGetOrganisationTable");
			
			 Logging.info("The contents of getOrganisation table is: "+tableContents);
		} catch (WebDriverException ex) {
			Logging.info("Error occurs when get the contents");
			throw ex;
		}

		return tableContents;
		
	}
	
	
	/**
	 * <b>Author</b>: Joey Fang<br>
	 * <b>Date</b>: 08/06/2018<br>
	 * <b>Description</b><br>
	 * Clicks on the different code type
	 * 
	 * @param tab
	 *            tab name
	 */
	public void clickCodeType(String codeType) {

		Logging.info("Clicking on " + codeType);
		String authenticationCodeType = PropertyFileReader.getProperty("MDPAPIsPage.properties", "authenticationCodeType")
				.replace("+variable+", codeType);

		try {
			ElementActions.setDriverDefaultValues();
			if (ElementActions.waitUntilElementDisplayedByXpath(authenticationCodeType,
					Integer.parseInt(tests.BaseTest.timeout))) {
				BaseTest.base.getDriver().findElement(By.xpath(authenticationCodeType)).click();
			}

		} catch (WebDriverException ex) {
			Logging.info("Could not click on " + codeType + " codeType");
			throw ex;
		}
	}
	
	public void clickAuthenticationJavaType() {
		clickCodeType("java");
		Logging.info("Clicked the Authentication Java Code Type");
	}
	
	public void clickAuthenticationShellType() {
		clickCodeType("shell");
		Logging.info("Clicked the Authentication Shell Code Type");
	}
	
	public void clickAuthenticationNodeJSType() {
		clickCodeType("node.js");
		Logging.info("Clicked the Authentication NodeJS Code Type");
	}
	
	
	/**
	 * @Method Name : verifyAPIsPageTopOrBottomElementDisplay
	 * @author : Joey Fang
	 * @Created on : 08/07/2018
	 * @Description : This method will verify if the top or bottom elements display
	 *              in the APIs page
	 * @Parameter
	 * @Return:
	 */
	public boolean verifyAPIsPageTopOrBottomElementDisplay(String TopOrBottom) {

		boolean result = false;
		String APIsPageBottomElement = PropertyFileReader.getProperty("MDPAPIsPage.properties",
				"APIsPageBottomElement");
		String APIsPageTopElement = PropertyFileReader.getProperty("MDPAPIsPage.properties", "APIsPageTopElement");
		Logging.info("Prepare to verify if the " + TopOrBottom + " page elements display in the APIs page");

		try {
			ElementActions.setDriverDefaultValues();

			if (TopOrBottom.trim().equalsIgnoreCase("top")) {

				boolean flag1 = ElementActions.waitUntilElementDisplayedByXpath(APIsPageTopElement,
						Integer.parseInt(tests.BaseTest.timeout));
				Logging.info("FFlag1 is: " + flag1);

				if (!flag1) {
					// throw new NoSuchElementException("");
					result = false;
				} else {
					result = true;
				}

			} else if (TopOrBottom.trim().equalsIgnoreCase("bottom")) {

				boolean flag1 = ElementActions.waitUntilElementDisplayedByXpath(APIsPageBottomElement,
						Integer.parseInt(tests.BaseTest.timeout));
				if (!flag1) {
					throw new NoSuchElementException("");
				}
				result = true;
			}

			else {
				Logging.info("Error parameter of top or bottom page");
			}

		} catch (WebDriverException ex) {
			Logging.info("Error occurs when verify the elements");
			throw ex;
		}
		return result;
	}

	public boolean verifyAPIsPageTopElementDisplay() {

		boolean result = false;

		try {
			if (verifyAPIsPageTopOrBottomElementDisplay("top")) {

				result = true;
			}

		}

		catch (WebDriverException ex) {
			Logging.info("Error occurs when verify the elements");
			throw ex;
		}
		Logging.info("resultresultresult is: " + result);
		return result;
	}

	public boolean verifyAPIsPageBottomElementDisplay() {
		boolean result = false;
		try {
			if (verifyAPIsPageTopOrBottomElementDisplay("bottom")) {
				result = true;
			}

		}

		catch (WebDriverException ex) {
			Logging.info("Error occurs when verify the elements");
			throw ex;
		}
		Logging.info("resultresultresult is: " + result);
		return result;
	}

}
