package pages.onboarding.business;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.ElementActions;


public class SummaryPage  {

    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[4]/span")
    private WebElement emailAddres;
    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[8]/span")
    private WebElement businessCategory;
    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[10]/span")
    private WebElement premiumId;
    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[14]/span")
    private WebElement name;
    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[16]/span")
    private WebElement businesName;
    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[18]/span[1]")
    private WebElement profilePhotoName;
    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[22]/span")
    private WebElement postCode;
    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[24]/span")
    private WebElement prefecture;
    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[26]/span")
    private WebElement city;
    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[28]/span")
    private WebElement address;
    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[30]/span")
    private WebElement businessDescription;
    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[32]/span")
    private WebElement phoneno;
    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[34]/span")
    private WebElement websiteUrl;

    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[40]/span[1]")
    private WebElement planAccType;
    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[40]/span[3]")
    private WebElement planPlanType;
    @FindBy(xpath = "//div[@class='summary-container ng-star-inserted']/div[42]/span[2]")
    private WebElement planPremiumID;

    @FindBy(xpath = "//ul[@class='steps']/li[5]")
    private WebElement summaryTab;
    @FindBy (xpath = "//button[@data-id='Summary-back-button']/span[text()='Back']")
    private WebElement backButton;
    @FindBy(xpath = "//button[@data-id='Summary-submit-button']")
    private WebElement continueButton;

    @FindBy (xpath = "//p[text()='Please select plan.']")
    private WebElement selectPlanScreen;
    @FindBy(xpath = "//p[text()='Thank you for registration in the Account Management Console.']")
    private WebElement thankYouScreenText;

    public SummaryPage (WebDriver driver){

        PageFactory.initElements(driver,this);

    }

    public String getEmailAddress(){

        String emailAddress = emailAddres.getText();
        return emailAddress;
    }

    public String getBusinessCaterory(){
        String businesscat = businessCategory.getText();
        return businesscat;
    }

    public String getPremiumId(){
        String premiumID = premiumId.getText();
        return premiumID;
    }

    public String getName(){

        String yourName = name.getText();
        return yourName;
    }

    public String getBusinessName(){
        return ElementActions.getText(businesName);

    }

    public String getProfilePhotoName(){

        return ElementActions.getText(profilePhotoName);

    }

    public String getPostalCode(){

        return ElementActions.getText(postCode);

    }

    public String getPrefecture(){

        return ElementActions.getText(prefecture);

    }

    public String getCity(){

        return ElementActions.getText(city);

    }

    public String getAddress(){

        return ElementActions.getText(address);

    }

    public String getBusinessDescription(){

        return ElementActions.getText(businessDescription);

    }

    public String getPhoneNumber(){

        return ElementActions.getText(phoneno);

    }

    public String getWebUrl(){

        return ElementActions.getText(websiteUrl);

    }

    public String getAccType(){
        return ElementActions.getText(planAccType);

    }

    public String getPlanplantype(){

        return ElementActions.getText(planPlanType);

     }

     public String getPlanPremiumId(){

         return ElementActions.getText(planPremiumID);

     }

    public String checkBreadCrumbMenu(){
        if(ElementActions.elementDisplayed(summaryTab)) {
            return summaryTab.getAttribute("class");

        } else{
            return null;
        }
    }

    public void clickBackButton(){

        ElementActions.click(backButton);
    }

    public void clickContinueBUtton(){

        ElementActions.click(continueButton);
    }

    public Boolean checkSelectPlanScreen(){
       if(selectPlanScreen.isDisplayed()){
           return true;
       }else {
           return false;
       }

    }

    public Boolean checkThankYouScreenText(){
        if(thankYouScreenText.isDisplayed()){
            return true;
        }else {
            return false;
        }
    }

}
