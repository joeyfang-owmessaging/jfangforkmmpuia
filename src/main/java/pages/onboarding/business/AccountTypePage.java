package pages.onboarding.business;

import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.ElementActions;

public class AccountTypePage {

    @FindBy(xpath="//span[@data-id='0-0' and contains(text(),'Standard Account')]")
    private WebElement standardAccount;

    @FindBy(xpath="//span[@data-id='0-1' and contains(text(),'Official Account')]")
    private WebElement officialAccount;

    @FindBy(xpath="//span[@data-id='0-2' and contains(text(),'A2P RCS Only')]")
    private WebElement a2pAccount;

    @FindBy(xpath="//a[@data-id='select-0da9d95a-5e64-44f7-81eb-43204e03de6b']")
    private WebElement stdSelectBtn;

    @FindBy(xpath="//a[@data-id='select-340d0130-e8ea-4827-8fd0-09e8a4503dd6']")
    private WebElement offSelectBtn;

    @FindBy(xpath="//a[@data-id='select-6de82218-c9ed-4bd3-ac3e-d690b3cbfcca']")
    private WebElement a2pSelectBtn;

    @FindBy(xpath="//span[@data-id='0' and contains(text(),'Business type')]")
    private WebElement businessTypeRowLabel;

    @FindBy(xpath="//span[@data-id='1' and contains(text(),'Number of Subscribers')]")
    private WebElement numberOfSubscribersRowLabel;

    @FindBy(xpath="//span[@data-id='2' and contains(text(),'Send messages using user phone number')]")
    private WebElement sendMessagesRowLabel;

    @FindBy(xpath="//span[@data-id='3' and contains(text(),'Pricing structure')]")
    private WebElement pricingStructureRowLabel;

    @FindBy(xpath="(//button[@data-id='Accounttype-back-button'])[2]")
    private WebElement backBtn;

    @FindBy(xpath = "//span[@name='01']")
    private WebElement standardAcctext;

    private @FindBy(xpath="//span[.='Continue']")
    WebElement continueBtn;

    private @FindBy(xpath="")
    WebElement breadCrumb;

    ExtentTest test;
    public AccountTypePage(WebDriver driver, ExtentTest extentTest){
        PageFactory.initElements(driver,this);
        test = extentTest;
    }

    public boolean standardAccountTypeDisplayed(){
        return ElementActions.elementDisplayed(standardAccount);
    }

    public boolean officialAccountTypeDisplayed(){
        return ElementActions.elementDisplayed(officialAccount);
    }

    public boolean a2PAccountTypeDisplayed(){
        return ElementActions.elementDisplayed(a2pAccount);
    }

    public boolean businessTypeRowLabelDisplayed(){
        return ElementActions.elementDisplayed(businessTypeRowLabel);
    }

    public boolean numberOfSubscribersRowLabelDisplayed(){
        return ElementActions.elementDisplayed(numberOfSubscribersRowLabel);
    }

    public boolean sendMessagesRowLabelDisplayed(){
        return ElementActions.elementDisplayed(sendMessagesRowLabel);
    }

    public boolean pricingStructureRowLabelDisplayed(){
        return ElementActions.elementDisplayed(pricingStructureRowLabel);
    }


    public boolean selectStdAcc(){
        return ElementActions.click(stdSelectBtn);
    }

    public boolean selectSOffAcc(){
        return ElementActions.click(offSelectBtn);
    }

    public boolean selectA2pAcc(){
        return ElementActions.click(a2pSelectBtn);
    }

    public boolean clickBackBtn(){
        return ElementActions.click(backBtn);
    }

    public boolean clickContinueBtn(){
        return ElementActions.click(continueBtn);
    }

    public String getGridText(WebDriver driver,String location){
        String location_xpath = "//span[@name='"+location+"']";
        String gridValue = driver.findElement(By.xpath(location_xpath)).getText();
        return gridValue;
    }

    public Boolean standardAccIsSelected(){
        if (stdSelectBtn.getAttribute("class").contains("button-selected")) {
            return true;
        }else{
            return false;
        }
    }

    public String getStandardAccText(){
        return ElementActions.getText(standardAccount);
    }
}
