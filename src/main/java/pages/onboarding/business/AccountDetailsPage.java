package pages.onboarding.business;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
//import sun.jvm.hotspot.debugger.posix.elf.ELFException;
import utils.ElementActions;
import utils.PropertyFileReader;

public class AccountDetailsPage {

    private String onboardingDetailsFile = "onboardinguserdetails.properties";

    @FindBy(xpath = "//input[@data-id='name']")
    private WebElement yourName;

    @FindBy(xpath = "//input[@data-id='businessName']")
    private WebElement businessName;

    @FindBy(xpath="//span[.='Select a business category']")
            //"//mat-select[@data-id='businessCategory']")
    private WebElement businessCategory;

    @FindBy(xpath="//mat-option[@data-id='869d22e2-9069-43e8-a3a4-13f4fbe815a4']")
    private WebElement selectCategory;

    @FindBy(xpath = "//input[@type='file']")
    private WebElement uploadImage;

    @FindBy(xpath="//a[@data-id='profileImage-upload-browse']")
    private WebElement uploadImageBtn;

    @FindBy(xpath = "//a[@data-id='profileImage-upload-crop']")
    private WebElement cropButton;

    @FindBy(xpath = "//a[@data-id='profileImage-upload-change']")
    private WebElement changePhotoButton;

    @FindBy(xpath = "//input[@data-id='postalCode']")
    private WebElement postalCode;

    @FindBy(xpath="//mat-select[@data-id='prefecture']")
    private WebElement prefecture;

    @FindBy(xpath="//mat-option[@data-id='Akita-ken']")
    private WebElement selectPref;

    @FindBy(xpath = "//input[@data-id='city']")
    private WebElement city;

    @FindBy(xpath = "//input[@data-id='address']")
    private WebElement address;

    @FindBy(xpath = "//input[@data-id='description']")
    private WebElement businessDesc;

    @FindBy(xpath = "//input[@data-id='phone']")
    private WebElement phoneNmber;

    @FindBy(xpath = "//input[@data-id='url']")
    private WebElement businessUrl;

    @FindBy(xpath = "//mat-checkbox[@data-id='termAndConditions-checkbox']/label/div")
    private WebElement checkbox_TCs;

    @FindBy(xpath = "(//button[@data-id='organization-back-button'])[2] ")
    private WebElement backBtn;

    @FindBy(xpath="//button[@data-id='organization-submit-button']")
    private WebElement continueBtn;

    @FindBy(xpath = "//span//a[.='Terms and Conditions']")
    private WebElement termsAndConditionslink;

    @FindBy(xpath = "//span//a[.='Privacy Policy']")
    private WebElement privacyPolicy;

    //*******************************************
    //************ Error messages ***************
    //*******************************************

    @FindBy(xpath = "//p[@data-id='name-message' and text()='Name is required.']")
    private WebElement nameErrorMessage;

    @FindBy(xpath = "//p[@data-id='businessName-message' and text()='Business name is required.']")
    private WebElement businessNameErrorMessage;

    @FindBy(xpath = "//p[@data-id='businessCategory-message' and text()='Business category is required.']")
    private WebElement businessCategoryErrorMessage;

    @FindBy(xpath = "//p[@data-id='profileImage-message' and text()='Profile photo is required.']")
    private WebElement profileImageErrorMessage;

    @FindBy(xpath = "//p[@data-id='profileImage-message' and text()='It is only supported jpg and png format files']")
    private WebElement formatErrorMessage;

    @FindBy(xpath = "//p[@data-id='postalCode-message' and text()='Postal code is required.']")
    private WebElement postalCodeErrorMessage;

    @FindBy(xpath = "//p[@data-id='prefecture-message' and text()='Prefecture is required.']")
    private WebElement prefectureErrorMessage;

    @FindBy(xpath = "//p[@data-id='city-message' and text()='City is required.']")
    private WebElement cityErrorMessage;

    @FindBy(xpath = "//p[@data-id='address-message' and text()='Address is required.']")
    private WebElement addressErrorMessage;

    @FindBy(xpath = "//p[@data-id='description-message' and text()='Description is required.']")
    private WebElement descriptionErrorMessage;

    @FindBy(xpath = "//p[@data-id='phone-message' and text()='Phone is required.']")
    private WebElement phoneErrorMessage;

    @FindBy(xpath = "//p[@data-id='termAndConditions-message' and text()='Accept Terms and Conditions.']")
    private WebElement termsAndConditionsErrorMsg;

    ExtentTest test;
    public AccountDetailsPage(WebDriver driver, ExtentTest extentTest){
        PageFactory.initElements(driver,this);
        test = extentTest;
    }

    public boolean enterYourName(String name){
        return ElementActions.sendKeys(yourName,name);
    }

    public boolean enterBusinessName(String bName){
        return ElementActions.sendKeys(businessName,bName);
    }

    public void enterBusinessCategory(){
        businessCategory.click();
        for(int i=0;i<10;i++){
            if(ElementActions.elementDisplayed(selectCategory)){
                selectCategory.click();
                break;
            } else{
                businessCategory.click();
            }
        }

    }

    public boolean selectDropdown(WebDriver driver, String dropdownLabel,String dropdownValue){

        String dropdownValue_xpath ="//span[contains(text(),'"+dropdownValue+"')]";

        if(dropdownLabel.equals("Business Category")) {
            businessCategory.click();
        } else if(dropdownLabel.equals("Prefecture")) {
            prefecture.click();
        }
        WebElement dropdownElement = driver.findElement(By.xpath(dropdownValue_xpath));
        ElementActions.scrollTo(driver,dropdownElement);

        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.elementToBeClickable(dropdownElement));

        return ElementActions.click(dropdownElement);
    }

    public void uploadFile(String path) {
        uploadImage.sendKeys(path);
    }

    public boolean clickUploadBtn() {
        return ElementActions.click(uploadImageBtn);
    }

    public boolean cropPhoto(){
        if (ElementActions.elementDisplayed(cropButton)) {
            return ElementActions.click(cropButton);
        }else{
            return false;
        }
    }

    public boolean changePhoto(){
        return ElementActions.click(changePhotoButton);
    }

    public boolean formatErrorPresent(){
        return ElementActions.elementDisplayed(formatErrorMessage);
    }

    public boolean cropButtonPresent(){
        return ElementActions.elementDisplayed(cropButton);
    }

    public boolean enterPostalCode(String pCode){
        return ElementActions.sendKeys(postalCode,pCode);

    }
    public boolean openprefectureDropdown(){
        return ElementActions.click(prefecture);
    }

    public void selectPrefecture(){
        prefecture.click();
        selectPref.click();
    }

    public boolean enterCity(String cty){
        return ElementActions.sendKeys(city,cty);
    }

    public boolean enterAddress(String addr){
        return ElementActions.sendKeys(address,addr);
    }

    public boolean enterDescription(String desc){
        return ElementActions.sendKeys(businessDesc,desc);
    }

    public boolean enterPhoneNumber(String pNumber) {
        return ElementActions.sendKeys(phoneNmber,pNumber);
    }

    public boolean enterUrl(String webUrl){
        return ElementActions.sendKeys(businessUrl,webUrl);
    }

    public boolean clickCheckbox(){
        return ElementActions.click(checkbox_TCs);
    }

    public boolean isTCErrorMessageDisplayed(){
        return ElementActions.elementDisplayed(termsAndConditionsErrorMsg);
    }

    public boolean clickBackBtn() {
        return ElementActions.click(backBtn);
    }

    public boolean clickContinueBtn(){
        return ElementActions.click(continueBtn);
    }

    public boolean clickTermsAndConditions(){
        return ElementActions.click(termsAndConditionslink);
    }

    public boolean clickPrivacyPolicy(){
        return ElementActions.click(privacyPolicy);
    }

    // Method to fill account details
    public void submitAccountDetails(WebDriver driver){

        enterYourName(PropertyFileReader.getProperty(onboardingDetailsFile,"name"));

        enterBusinessName(PropertyFileReader.getProperty(onboardingDetailsFile,"business"));

        enterBusinessCategory();

        //selectDropdown(driver,"Business Category", PropertyFileReader.getProperty(onboardingDetailsFile,"category"));

        uploadFile(System.getProperty("user.dir") + PropertyFileReader.getProperty(onboardingDetailsFile,"image.jpg"));

        cropPhoto();

        enterPostalCode(PropertyFileReader.getProperty(onboardingDetailsFile,"postalcode"));

        selectDropdown(driver,"Prefecture",PropertyFileReader.getProperty(onboardingDetailsFile,"prefecture"));

        enterCity(PropertyFileReader.getProperty(onboardingDetailsFile,"city"));

        enterAddress(PropertyFileReader.getProperty(onboardingDetailsFile,"address"));

        enterDescription(PropertyFileReader.getProperty(onboardingDetailsFile,"description"));

        enterPhoneNumber(PropertyFileReader.getProperty(onboardingDetailsFile,"phonenumber"));

        enterUrl(PropertyFileReader.getProperty(onboardingDetailsFile,"website"));

        clickCheckbox();

        clickContinueBtn();

        }

        // Method to fill account details and submit.

    public void AccountDetailFill(WebDriver driver, String name, String business, String businessCategory, String pincode, String prefecture,
                                  String city, String address, String description, String phone, String link) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        enterYourName(name);

        enterBusinessName(business);

        selectDropdown(driver,"Business Category",businessCategory);

        uploadFile(System.getProperty("user.dir") + "/src/test/resources/pictures/Picture.jpg");

        cropPhoto();

        enterPostalCode(pincode);

        selectDropdown(driver, "Prefecture", prefecture);

        enterCity(city);

        enterAddress(address);

        enterDescription(description);

        enterPhoneNumber(phone);

        enterUrl(link);

        clickCheckbox();

        clickContinueBtn();

    }

    public Boolean nameErrorDisplayed() {

        return ElementActions.elementDisplayed(nameErrorMessage);
    }

    public Boolean businessNameErrorDisplayed() {

        return ElementActions.elementDisplayed(businessNameErrorMessage);
    }

    public Boolean businessCategoryErrorDisplayed() {

        return ElementActions.elementDisplayed(businessCategoryErrorMessage);
    }

    public Boolean photoErrorDisplayed() {

        return ElementActions.elementDisplayed(profileImageErrorMessage);
    }

    public Boolean postalCodeErrorDisplayed() {

        return ElementActions.elementDisplayed(postalCodeErrorMessage);
    }

    public Boolean prefectureErrorDisplayed() {

        return ElementActions.elementDisplayed(prefectureErrorMessage);
    }

    public Boolean cityErrorDisplayed() {

        return ElementActions.elementDisplayed(cityErrorMessage);
    }

    public Boolean addressErrorDisplayed() {

        return ElementActions.elementDisplayed(addressErrorMessage);
    }

    public Boolean descriptionNameErrorDisplayed() {

        return ElementActions.elementDisplayed(descriptionErrorMessage);
    }

    public Boolean phoneNumberErrorDisplayed() {

        return ElementActions.elementDisplayed(phoneErrorMessage);
    }



}
