package pages.onboarding.business;

import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.ElementActions;

public class LoginDetailsPage {

    @FindBy(xpath = "//input[@data-id='email']")
    private WebElement emailID;
    @FindBy(xpath = "//input[@data-id='repeatEmail']")
    private WebElement reenterEmailID;
    @FindBy(xpath = "//input[@data-id='password']")
    private WebElement password;
    @FindBy(xpath = "//input[@data-id='repeatPassword']")
    private WebElement reenterPassword;
    @FindBy(xpath = "//mat-radio-button[@data-id='Business only-radio-group']")
    private WebElement businessAccountType;
    @FindBy(xpath = "//mat-radio-button[@data-id='Bussines w/Developer-radio-group']")
    private WebElement businessDeveloperAccountType;
    @FindBy(xpath = "//mat-radio-button[@data-id='Developer only-radio-group']")
    private WebElement developerOnlyAccountType;
    @FindBy(xpath="//button[@data-id='credentials-submit-button']")
    private WebElement ContinueBtn;
    @FindBy(xpath = "//p[@data-id='password-message']/ul/li[1]")
    private WebElement minimumcharMsg;
    @FindBy(xpath = "//p[@data-id='password-message']/ul/li[2]")
    private WebElement uppercaseMsg;
    @FindBy(xpath = "//p[@data-id='password-message']/ul/li[3]")
    private WebElement lowercaseMsg;
    @FindBy(xpath = "//p[@data-id='password-message']/ul/li[4]")
    private WebElement numberMsg;
    @FindBy(xpath = "//p[@data-id='password-message']/ul/li[5]")
    private WebElement specialCharMsg;
    @FindBy(xpath = "//p[@data-id='password-message']/ul/li[6]")
    private WebElement maximumCharMsg;
    @FindBy(xpath = "//ul[@class='steps']/li[2]")
    private WebElement accountDetailsTab;
    @FindBy(xpath = "//sncr-input[@id=\"password\"]/div/sncr-form-field/span/div/div/span")
    private  WebElement showhidePassword;
    @FindBy(xpath = "//p[@data-id='email-message']")
    private WebElement emailErrormsg;
    @FindBy(xpath = "//p[@data-id='repeatEmail-message']")
    private WebElement repeatEmailErrorMsg;
    @FindBy(xpath = "//p[@data-id='email-message']")
    private WebElement emptyEmailErrorMsg;
    @FindBy(xpath = "//p[@data-id='repeatEmail-message']")
    private WebElement repeatEmptyEmailErrorMsg;
    @FindBy(xpath = "//p[@data-id='repeatPassword-message']")
    private WebElement repeatPasswordErrorMsg;
    @FindBy(xpath = "//p[@data-id='password-message']")
    private WebElement emptyPasswordErrorMsg;
    @FindBy(xpath = "//span[@data-id='password-password-show-hide-text']")
    private WebElement showPasswordOption;
    @FindBy(xpath = "//a[text()='Download Bussines only Application Form']")
    private WebElement downloadBusinessApplicationlink;
    @FindBy(xpath = "//a[text()='Download Bussines w/Developer Application Form']")
    private WebElement downloadBusinessDeveloperApplicationlink;
    @FindBy(xpath = "//a[text()='Download Developer only Application Form']")
    private WebElement downloadDeveloperApplicationForm;
    @FindBy(xpath = "//small[text()='© 2018 Synchronoss. All Rights Reserved.']")
    private WebElement copyRightDetail;
    @FindBy(xpath = "//p[@data-id='accountType-message']")
    private WebElement accountTypeErrorMsg;
    @FindBy(xpath = "//a[text()='Privacy Policy']")
    private WebElement privacyPolicyLink;
    @FindBy(xpath = "//a[text()='Terms and Conditions']")
    private WebElement termsAndConditionLink;
    @FindBy(xpath = "//header//img")
    private WebElement companyLogoText;
    @FindBy(xpath = "//h1[text()='Open New Account']")
    private WebElement headingText;
    @FindBy(xpath = "//ul[@class='steps']")
    private WebElement breadcrumbMenu;


    ExtentTest test;
    public LoginDetailsPage(WebDriver driver, ExtentTest extentTest){
        PageFactory.initElements(driver,this);
        test = extentTest;
    }

    public void enterEmail1(String email)  {
//        email1.click();
        emailID.sendKeys(email);


    }

    public void enterEmail2(String email)  {
//        email2.click();
        reenterEmailID.sendKeys(email);

    }

    public void enterPwd1(String pwd){
//        pwd1.click();
        password.sendKeys(pwd);

    }

    public void enterPwd1forvalidation(String pwd){
//        pwd1.click()
        password.sendKeys(pwd);

    }

    public void enterPwd2(String pwd){
//        pwd2.click();
        reenterPassword.sendKeys(pwd);

    }

    public void selectBusinessAccountType(){
        businessAccountType.click();
        // businessAccountType.click();

    }

    public void selectBusinessDeveloperAccountType() {
        businessDeveloperAccountType.click();
        // businessDeveloperAccountType.click();

    }

    public void selectDeveloperOnlyAccountType() {

        developerOnlyAccountType.click();
        // developerOnlyAccountType.click();
    }

    public String getBusinessOnlyApplicationLink() {

        String downloadBusinessLink = downloadBusinessApplicationlink.getText();
        return downloadBusinessLink;
    }

    public String getBusinessDeveloperApplicationLink() {

        String downloadBusinessDeveloperLink = downloadBusinessDeveloperApplicationlink.getText();
        return downloadBusinessDeveloperLink;
    }

    public String getDeveloperApplicationLink() {

        String downloadDeveloperLink = downloadDeveloperApplicationForm.getText();
        return downloadDeveloperLink;

    }

    public void submitDetails(){

        ContinueBtn.click();
    }

    public String checkminimumchar(){
        String mini = minimumcharMsg.getAttribute("class");

        return mini;
    }

    public String checkuppercase(){

        String upper = uppercaseMsg.getAttribute("class");
        return upper;
    }

    public String checklowercase(){

        String lower = lowercaseMsg.getAttribute("class");

        return lower;
    }

    public String checknumericvalue(){
        String numeric = numberMsg.getAttribute("class");
        return numeric;

    }

    public String checkspecialchar(){

        String special = specialCharMsg.getAttribute("class");

        return special;
    }

    public String checkmaximumchar(){

        String maximum = maximumCharMsg.getAttribute("class");

        return maximum;

    }

    public String checkAccountDetailScreen(){

        String heading = accountDetailsTab.getAttribute("class");

        return heading;
    }

    public String checkemailerrormsg(){

        String errormsg = emailErrormsg.getText();
        return errormsg;
    }

    public String checkRepeatemailerrromsg(){

        String repeaterrormsg = repeatEmailErrorMsg.getText();

        return repeaterrormsg;
    }

    public String checkEmptyemailerrormsg(){

        String emptyerrormsg = emptyEmailErrorMsg.getText();
        return emptyerrormsg;
    }

    public String checkRepeatEmptyemailerrormsg(){

        String repeatemptyerrormsg = repeatEmptyEmailErrorMsg.getText();
        return repeatemptyerrormsg;
    }

    public String checkRepeatPassworderrormsg(){

        String repeatpwderrormsg = repeatPasswordErrorMsg.getText();
        return repeatpwderrormsg;
    }

    public String checkEmptyPassworderrormsg(){

        String emptypasserrormsg = emptyPasswordErrorMsg.getText();
        return emptypasserrormsg;
    }

    public String checkPasswordMask() {

        String type = password.getAttribute("type");
        return type;
    }

    public void clickOnShowPasswordOption() {

        showPasswordOption.click();
    }

    public String getCopyRightDetail() {
        String copyRghtDetail = copyRightDetail.getText();
        return copyRghtDetail;
    }

    public String getAccountTypeErrorMsg() {

        String accTypeError = accountTypeErrorMsg.getText();
        return accTypeError;
    }

    public void submitLoginDetails(String email, String pwd) {

        enterEmail1(email);
        enterEmail2(email);
        enterPwd1(pwd);
        enterPwd2(pwd);
        selectBusinessAccountType();
        submitDetails();

    }

    public String getEmailEntered() {

        return emailID.getAttribute("value");
    }

    public String getRepeatEmailEntered() {

        return reenterEmailID.getAttribute("value");
    }

    public String getPasswordEntered() {

        return password.getAttribute("value");
    }

    public String getRepeatPasswordEntered() {

        return reenterPassword.getAttribute("value");
    }

    public void clickOnPrivacyPolicyLink() {

        privacyPolicyLink.click();
    }

    public void clickOnTermsAndConditionLink() {

        termsAndConditionLink.click();
    }

    public String checkAccountTypeSelectedOrNot() {

        String className = businessAccountType.getAttribute("class");

        return className;


    }

    public String checkDevelopervsBusinessAccountTypeSelected() {

        String className2 = businessDeveloperAccountType.getAttribute("class");
        return className2;
    }

    public String checkDeveloperOnlyAccountTypeSelected() {

        String className3 = developerOnlyAccountType.getAttribute("class");
        return className3;
    }

    public void clickOnAccountDetailTab() {

        accountDetailsTab.click();
    }

    public boolean checkCompanyLogoandText() {

        return ElementActions.elementDisplayed(companyLogoText);
    }

    public boolean checkHeadingText() {

        return ElementActions.elementDisplayed(headingText);
    }

    public boolean checkBreadCrumbMenu() {

        return ElementActions.elementDisplayed(breadcrumbMenu);
    }



}
