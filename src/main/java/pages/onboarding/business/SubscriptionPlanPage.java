package pages.onboarding.business;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class SubscriptionPlanPage {

    @FindBy(xpath = "//span[contains(text(),'Free')]")
    private WebElement freePlan;
    @FindBy(xpath = "//span[contains(text(),'Basic')]")
    private WebElement basicPlan;
    @FindBy(xpath = "//span[contains(text(),'Pro')]")
    private WebElement proPlan;
    @FindBy(xpath = "//a[@id=0]")
    private WebElement selectFreebtn;
    @FindBy(xpath = "//a[@id=1]")
    private WebElement selectBasicbtn;
    @FindBy(xpath = "//a[@id=2]")
    private WebElement selectProbtn;
    @FindBy()
    private WebElement premiumID;
    @FindBy()
    private WebElement premiumIDchkbox;
    @FindBy(xpath = "//button[@data-id='Plantype-back-button']")
    private WebElement backbtn;
    @FindBy(xpath = "//button[@data-id='plan-submit-button']")
    private WebElement continuebtn;
    @FindBy()
    private WebElement premiumIDField;
    @FindBy(xpath = "//ul[@class='steps']/li[5]")
    private WebElement summaryTab;

    @FindBy(xpath = "//span[@name='01']")
    private WebElement freePlanText;


    public SubscriptionPlanPage(WebDriver driver){

        PageFactory.initElements(driver,this);

    }

    public Boolean CheckFreePlan(){

        if(freePlan.isDisplayed()){
            return true;
        }else
            {
            return false;
        }

    }

    public Boolean CheckBasicPlan(){

        if(basicPlan.isDisplayed()){
            return true;
        }else
        {
            return false;
        }

    }

    public Boolean CheckProPlan(){
        if(proPlan.isDisplayed()){
            return true;
        }else
        {
            return false;
        }
    }

    public String GetFreePlanText(){

        String freePlanTxt = freePlanText.getText();
        return freePlanTxt;
    }

    public void Back(){

        backbtn.click();
    }

    public void Continue(){

        continuebtn.click();
    }

    public void SelectFreePlan(){

        selectFreebtn.click();
    }

    public void SelectBasicPlan(){

        selectBasicbtn.click();
    }

    public void SelectProPlan(){

      selectProbtn.click();
    }


    public Boolean CheckPremiumID(){

        if(premiumIDchkbox.isEnabled()){

            return true;
        }
        else {
            return false;
        }
    }

    public String checkSummaryScreen(){

        String heading = summaryTab.getAttribute("class");

        return heading;
    }

    public void SelectPremiumIdcheckbox(){

        premiumIDchkbox.click();
    }

    public Boolean CheckPremiumIDField(){

        if(premiumIDField.isDisplayed()){

            return true;
        }
        else {
            return false;
        }
        }

    public void enterPremiumID(String ID){
        premiumIDField.sendKeys(ID);
        }

}
