package pages.login;

import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.ElementActions;

public class TopPage {

    @FindBy(xpath = "//header//img")
    private WebElement syncronossLogo;

    @FindBy(xpath = "//h1[@data-id='page-title' and .='MINE Manager']")
    private WebElement mineHeader;

    @FindBy(xpath = "//li[@data-id='user-toolbar-logout']")
    private WebElement logoutIcon;

    @FindBy(xpath = "//h1[@data-id='top-title' and .='Choose an account below']")
    private WebElement chooseAccountText;

    @FindBy(xpath = "//mat-icon[@data-id='mat-iconmat-icon']")
    private WebElement searchIcon;

    @FindBy(xpath = "//span[@data-id='top-name']")
    private WebElement businessName;

    @FindBy(xpath = "//span[@data-id='top-uniqueId']")
    private WebElement uniqueID;

    @FindBy(xpath = "//span[@data-id='top-plan']")
    private WebElement accountTypeAndPlan;

    @FindBy(xpath = "//span[@data-id='top-status']/span")
    private WebElement accountStatusIcon;

    @FindBy(xpath = "//span[@data-id='top-status']")
    private WebElement accountStatus;

    @FindBy(xpath = "//span[@data-id='top-subscribers']/mat-icon")
    private WebElement subscribersIcon;

    @FindBy(xpath = "//span[@data-id='top-subscribers']")
    private WebElement numberOfSubscribers;

    @FindBy(xpath = "//a[@data-id='top-select']")
    private WebElement selectButton;

    @FindBy(xpath = "//button[@data-id='top-new-entity']")
    private WebElement createNewEntityButton;

    ExtentTest test;

    public TopPage(WebDriver driver, ExtentTest extentTest){
        PageFactory.initElements(driver,this);
        test = extentTest;
    }

    public boolean synchronossLogoPresent(){
        return ElementActions.elementDisplayed(syncronossLogo);
    }

    public boolean headerPresent(){
        return ElementActions.elementDisplayed(mineHeader);
    }

    public boolean logoutIconPresent(){
        return ElementActions.elementDisplayed(logoutIcon);
    }

    public boolean chooseAccountTextPresent(){
        return ElementActions.elementDisplayed(chooseAccountText);
    }

    public boolean searchIconPresent(){
        return ElementActions.elementDisplayed(searchIcon);
    }

    public String getBusnessName(){
        return ElementActions.getText(businessName);
    }

    public String getUniqueID(){
        return ElementActions.getText(uniqueID);
    }

    public String getAccountTypeAndPlan(){
        return ElementActions.getText(accountTypeAndPlan);
    }

    public boolean accountStatusIconDisplayed() {
        return ElementActions.elementDisplayed(accountStatusIcon);
    }

    public String getAccountStatus(){
        return ElementActions.getText(accountStatus);
    }

    public boolean subscriberIconDisplayed() {
        return ElementActions.elementDisplayed(subscribersIcon);
    }

    public String getNumberOfSubscribers(){
        return ElementActions.getText(numberOfSubscribers);
    }

    public boolean selectButtonPresent(){
        return ElementActions.elementDisplayed(selectButton);
    }

    public boolean createNewEntityButtonPresent(){
        return ElementActions.elementDisplayed(createNewEntityButton);
    }

    public boolean clickSelectButton(){
        return ElementActions.click(selectButton);
    }


}
