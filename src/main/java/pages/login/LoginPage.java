package pages.login;


import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.ElementActions;

public class LoginPage {
    @FindBy(xpath = "//input[@data-id='email']")
    private WebElement emailTxtBox;
    @FindBy(xpath = "//input[@data-id='password']")
    private WebElement passwordTxtBox;
    @FindBy(xpath = "//button[@data-id='login-submit-button']")
    private WebElement loginBtn;
    @FindBy(xpath = "//a[text()='Forgot Password?']")
    private WebElement forgotPassLink;
    @FindBy(xpath = "//a[text()='Create your account now.']")
    private WebElement createAccLink;

    /*@FindBy(xpath = "//p[@data-id='email-message']")
    private WebElement emptyEmailMsg;
    @FindBy(xpath = "//p[@data-id='password-message']")
    private WebElement emptyPasswordMsg;*/

    @FindBy(xpath = "//p[@data-id='login-general-error']")
    private WebElement invalidEmailidPaswordsMsg;
    @FindBy(xpath = "//h1[text()='MINE Manager']")
    private WebElement mineHeader;
    @FindBy(xpath = "//header//img")
    private WebElement companyLogoText;
    @FindBy(xpath = "//h1[text()='Login to your account']")
    private WebElement headingText;
    @FindBy (xpath = "//span[text()='Email address']")
    private WebElement emailAddressLabel;
    @FindBy(xpath = "//span[text()='Password']")
    private WebElement passwordLabel;
    @FindBy (xpath = "//div[@class='mat-checkbox-inner-container']")
    private WebElement keepMeLoginCheckBox;
    @FindBy(xpath = "//span[text()='Keep me logged in']")
    private WebElement keepMeLoggedinText;
    @FindBy(xpath = "//a[text()='Create your account now.']")
    private WebElement createYoutAccLink;
    @FindBy(xpath = "//small[text()='© 2018 Synchronoss. All Rights Reserved.']")
    private WebElement copyRightDetail;
    @FindBy(xpath = "//a[text()='Terms and Conditions']")
    private WebElement termsAndCondition;
    @FindBy(xpath = "//a[text()='Privacy Policy']")
    private WebElement privacyPolicy;

    ExtentTest test;

    public LoginPage(WebDriver driver, ExtentTest extentTest) {
        PageFactory.initElements(driver, this);
        test = extentTest;
    }

    public void enterEmailId(String Emailid) {

        emailTxtBox.sendKeys(Emailid);
    }

    public void enterPassword(String Password) {

        passwordTxtBox.sendKeys(Password);
    }

    public void SubmitLoginDetails() {
        loginBtn.click();
    }

    public String checkerrormsgforinvalidEmailPass(){

        String invalidIDPassErrormsg = invalidEmailidPaswordsMsg.getText();
        return invalidIDPassErrormsg;
    }

    public String checkerrormsgforEmptyEmail(){

        String emptyEmailmsg = invalidEmailidPaswordsMsg.getText();
        return emptyEmailmsg;
    }

    public String checkerrormsgforEmptyPass(){

        String emptyPassmsg = invalidEmailidPaswordsMsg.getText();
        return emptyPassmsg;
    }

    public String verifyHeader(){

        String header = mineHeader.getText();
        return header;
    }

    public void userLogin(String Id, String Password){
        emailTxtBox.sendKeys(Id);
        passwordTxtBox.sendKeys(Password);
        loginBtn.click();

    }

    public boolean checkCompanyLogoandText(){

        return ElementActions.elementDisplayed(companyLogoText);
    }

    public boolean checkHeadingText(){

        return ElementActions.elementDisplayed(headingText);
    }

    public boolean checkEmailAddressLabel(){

        return ElementActions.elementDisplayed(emailAddressLabel);
    }

    public boolean checkEmailAddressTxtBox(){

        return ElementActions.elementDisplayed(emailTxtBox);
    }

    public boolean checkPasswordLabel(){

        return ElementActions.elementDisplayed(passwordLabel);
    }

    public boolean checkPasswordTextBox(){

        return ElementActions.elementDisplayed(passwordTxtBox);
    }

    public boolean checkKeepMeLoginCheckBox(){

        return ElementActions.elementDisplayed(keepMeLoginCheckBox);

    }

    public boolean checkKeepMeLoggedinText(){

        return ElementActions.elementDisplayed(keepMeLoggedinText);
    }

    public boolean checkForgotPassLink(){

        return ElementActions.elementDisplayed(forgotPassLink);
    }

    public boolean checkLoginButton() {

        return ElementActions.elementDisplayed(loginBtn);
    }

    public boolean checkCreateYourAccLink() {

        return ElementActions.elementDisplayed(createAccLink);
    }

    public boolean checkCopyRightDetail() {

        return ElementActions.elementDisplayed(copyRightDetail);
    }

    public boolean checkPrivacyPolicy() {

        return ElementActions.elementDisplayed(privacyPolicy);
    }

    public boolean checkTermsAndCondition() {

        return ElementActions.elementDisplayed(termsAndCondition);
    }

    public String checkPasswordType() {

        String type = passwordTxtBox.getAttribute("type");
        return type;
    }

}