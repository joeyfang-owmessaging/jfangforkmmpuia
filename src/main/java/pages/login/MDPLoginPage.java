package pages.login;

import com.relevantcodes.extentreports.ExtentTest;

import tests.BaseTest;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import utils.ElementActions;
import utils.Logging;
import utils.PropertyFileReader;

public class MDPLoginPage {

	String emailTxtBox = PropertyFileReader.getProperty("MDPLoginPage.properties", "emailTxtBox");
	String passwordTxtBox = PropertyFileReader.getProperty("MDPLoginPage.properties", "passwordTxtBox");
	String loginBtn = PropertyFileReader.getProperty("MDPLoginPage.properties", "loginBtn");
	String keepMeLoggedinText = PropertyFileReader.getProperty("MDPLoginPage.properties", "keepMeLoggedinText");
	String keepMeLoginCheckBox = PropertyFileReader.getProperty("MDPLoginPage.properties", "keepMeLoginCheckBox");
	String loginCMCText = PropertyFileReader.getProperty("MDPLoginPage.properties", "loginCMCText");
	String loginCMC = PropertyFileReader.getProperty("MDPLoginPage.properties", "loginCMC");
	String companyLogoText = PropertyFileReader.getProperty("MDPLoginPage.properties", "companyLogoText");
	String headingText = PropertyFileReader.getProperty("MDPLoginPage.properties", "headingText");
	String emailAddressLabel = PropertyFileReader.getProperty("MDPLoginPage.properties", "emailAddressLabel");
	String passwordLabel = PropertyFileReader.getProperty("MDPLoginPage.properties", "passwordLabel");
	String forgotPassLink = PropertyFileReader.getProperty("MDPLoginPage.properties", "forgotPassLink");
	String createAccLink = PropertyFileReader.getProperty("MDPLoginPage.properties", "createAccLink");
	String termsAndCondition = PropertyFileReader.getProperty("MDPLoginPage.properties", "termsAndCondition");
	String privacyPolicy = PropertyFileReader.getProperty("MDPLoginPage.properties", "privacyPolicy");
	String MDPHomeLogo = PropertyFileReader.getProperty("MDPHomePage.properties", "MDPHomeLogo");
	String MDPHomeLogout = PropertyFileReader.getProperty("MDPHomePage.properties", "MDPHomeLogout");
	String accountSelectButtons = PropertyFileReader.getProperty("MDPHomePage.properties", "accountSelectButtons");
	String accountSelectButton = PropertyFileReader.getProperty("MDPHomePage.properties", "accountSelectButton");
	String MDPHomeIntroduction = PropertyFileReader.getProperty("MDPHomePage.properties", "MDPHomeIntroduction");

	ExtentTest test;

	public MDPLoginPage(WebDriver driver, ExtentTest extentTest) {
		PageFactory.initElements(driver, this);
		test = extentTest;
	}

	/**
	 * @Method Name : verifyMDPLoginPageLoaded
	 * @author : Joey Fang
	 * @Created on : 07/26/2018
	 * @Description : This method will verify whether login page loaded correctly.
	 * @Parameter 1: int timeoutInSeconds - value specified in seconds to wait
	 * @Return: boolean - true: page loaded / false: page not loaded
	 */
	public boolean verifyMDPLoginPageLoaded(int timeoutInSeconds) {

		boolean result = false;

		try {
			boolean flag1 = ElementActions.waitUntilElementDisplayedByXpath(emailTxtBox, timeoutInSeconds);
			if (!flag1) {
				throw new NoSuchElementException("");
			}

			boolean flag2 = ElementActions.waitUntilElementDisplayedByXpath(passwordTxtBox, timeoutInSeconds);
			if (!flag2) {
				throw new NoSuchElementException("");
			}

			boolean flag3 = ElementActions.waitUntilElementDisplayedByXpath(loginBtn, timeoutInSeconds);
			if (!flag3) {
				throw new NoSuchElementException("");
			}

			boolean flag4 = ElementActions.waitUntilElementDisplayedByXpath(keepMeLoggedinText, timeoutInSeconds);
			if (!flag4) {
				throw new NoSuchElementException("");
			}

			boolean flag5 = ElementActions.waitUntilElementDisplayedByXpath(keepMeLoginCheckBox, timeoutInSeconds);
			if (!flag5) {
				throw new NoSuchElementException("");
			}

			/*
			 * boolean flag6 = ElementActions.waitUntilElementDisplayedByXpath(loginCMCText,
			 * timeoutInSeconds); if (!flag6) { throw new NoSuchElementException(""); }
			 * 
			 * boolean flag7 = ElementActions.waitUntilElementDisplayedByXpath(loginCMC,
			 * timeoutInSeconds); if (!flag7) { throw new NoSuchElementException(""); }
			 */

			boolean flag8 = ElementActions.waitUntilElementDisplayedByXpath(companyLogoText, timeoutInSeconds);
			if (!flag8) {
				throw new NoSuchElementException("");
			}

			boolean flag9 = ElementActions.waitUntilElementDisplayedByXpath(headingText, timeoutInSeconds);
			if (!flag9) {
				throw new NoSuchElementException("");
			}

			boolean flag10 = ElementActions.waitUntilElementDisplayedByXpath(emailAddressLabel, timeoutInSeconds);
			if (!flag10) {
				throw new NoSuchElementException("");
			}

			boolean flag11 = ElementActions.waitUntilElementDisplayedByXpath(passwordLabel, timeoutInSeconds);
			if (!flag11) {
				throw new NoSuchElementException("");
			}

			boolean flag12 = ElementActions.waitUntilElementDisplayedByXpath(forgotPassLink, timeoutInSeconds);
			if (!flag12) {
				throw new NoSuchElementException("");
			}

			boolean flag13 = ElementActions.waitUntilElementDisplayedByXpath(createAccLink, timeoutInSeconds);
			if (!flag13) {
				throw new NoSuchElementException("");
			}

			boolean flag14 = ElementActions.waitUntilElementDisplayedByXpath(termsAndCondition, timeoutInSeconds);
			if (!flag14) {
				throw new NoSuchElementException("");
			}

			boolean flag15 = ElementActions.waitUntilElementDisplayedByXpath(privacyPolicy, timeoutInSeconds);
			if (!flag15) {
				throw new NoSuchElementException("");
			}

			// All core elements loaded.
			result = true;
		} catch (WebDriverException ex) {
			Logging.info("Login page could not be confirmed as loaded");
			throw ex;
		}

		if (result) {
			Logging.info("Login page loaded");
		}
		return result;
	}

	/**
	 * @Method Name : typeUsername
	 * @author : Joey Fang
	 * @Created on : 07/26/2018
	 * @Description
	 * @Parameter
	 * @Return
	 */

	public void typeUsername(String username) {
		Logging.info("Typing login user name: " + username);

		try {
			ElementActions.setDriverDefaultValues();
			BaseTest.base.getDriver().findElement(By.xpath(emailTxtBox)).sendKeys(username.trim());
		} catch (WebDriverException ex) {
			Logging.info("Could not type login username: " + username);
			throw ex;
		}
	}

	/**
	 * @Method Name : typePassword
	 * @author : Joey Fang
	 * @Created on : 07/26/2018
	 * @Description
	 * @Parameter
	 * @Return
	 */

	public void typePassword(String password) {
		Logging.info("Typing login password: " + password);

		try {
			ElementActions.setDriverDefaultValues();
			BaseTest.base.getDriver().findElement(By.xpath(passwordTxtBox)).sendKeys(password.trim());
		} catch (WebDriverException ex) {
			Logging.info("Could not type login password: " + password);
			throw ex;
		}
	}

	/**
	 * @Method Name : clickLoginButton
	 * @author : Joey Fang
	 * @Created on : 07/26/2018
	 * @Description
	 * @Parameter
	 * @Return
	 */

	public void clickLoginButton() {
		Logging.info("Clicking on Login Button");
		try {
			ElementActions.setDriverDefaultValues();
			if (ElementActions.waitUntilElementDisplayedByXpath(loginBtn, Integer.parseInt(tests.BaseTest.timeout))) {
				BaseTest.base.getDriver().findElement(By.xpath(loginBtn)).click();
				// verifyMDPHomePageLoaded(19);
			}

		} catch (WebDriverException ex) {
			Logging.info("Could not click on login button");
			throw ex;
		}
	}

	/**
	 * @Method Name : clickLoginButton
	 * @author : Joey Fang
	 * @Created on : 07/26/2018
	 * @Description
	 * @Parameter
	 * @Return
	 */

	public void tickCMCCheckbox() {
		Logging.info("Ticking on CMC checkbox");
		try {
			ElementActions.setDriverDefaultValues();
			ElementActions.clickOnCheckbox(loginCMC, true);
			// verifyMDPHomePageLoaded(20);

		} catch (WebDriverException ex) {
			Logging.info("Could not click on login button");
			throw ex;
		}
	}

	/**
	 * @Method Name : userLogin
	 * @author : Joey Fang
	 * @Created on : 07/26/2018
	 * @Description
	 * @Parameter
	 * @Return
	 */

	public void userLogin() {
		Logging.info("Prepare to login MDP");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		typeUsername(tests.BaseTest.username);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		typePassword(tests.BaseTest.password);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// tickCMCCheckbox();
		clickLoginButton();
	}

	/**
	 * @Method Name : userLogin
	 * @author : Joey Fang
	 * @Created on : 07/30/2018
	 * @Description
	 * @Parameter
	 * @Return
	 */

	public void loginMDPPortal() {
		Logging.info("Prepare to login MDP Portal");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (verifyMDPLoginPageLoaded(300)) {
			userLogin();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Assert.assertTrue(verifyPortalSwitchPageLoaded(30));
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		selectAccount("0");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void selectAccount(String num) {
		Logging.info("Selecting the account");
		try {
			ElementActions.setDriverDefaultValues();
			accountSelectButton = accountSelectButton.replace("+variable+", num);
			Logging.info("accountSelectButton is: " + accountSelectButton);
			if (ElementActions.waitUntilElementDisplayedByXpath(accountSelectButton,
					Integer.parseInt(tests.BaseTest.timeout))) {
				BaseTest.base.getDriver().findElement(By.xpath(accountSelectButton)).click();
				// verifyMDPHomePageLoaded(19);
			}

		} catch (WebDriverException ex) {
			Logging.info("Could not select the account");
			throw ex;
		}
	}

	/**
	 * @Method Name : verifyPortalSwitchPageLoaded
	 * @author : Joey Fang
	 * @Created on : 07/26/2018
	 * @Description : This method will verify whether the portal switch page loaded
	 *              correctly.
	 * @Parameter 1: int timeoutInSeconds - value specified in seconds to wait
	 * @Return: boolean - true: page loaded / false: page not loaded
	 */
	public boolean verifyPortalSwitchPageLoaded(int timeoutInSeconds) {

		boolean result = false;

		try {
			boolean flag1 = ElementActions.waitUntilElementDisplayedByXpath(accountSelectButtons, timeoutInSeconds);
			if (!flag1) {
				throw new NoSuchElementException("");
			}

			Logging.info("Portal switch page loaded");

			// All core elements loaded.
			result = true;
		} catch (WebDriverException ex) {
			Logging.info("Portal switch page could not be confirmed as loaded");
			throw ex;
		}

		if (result) {
			Logging.info("Portal switch page loaded");
		}
		return result;
	}

	/**
	 * @Method Name : verifyMDPHomePageLoaded
	 * @author : Joey Fang
	 * @Created on : 07/26/2018
	 * @Description : This method will verify whether the MDP Home page loaded
	 *              correctly after login.
	 * @Parameter 1: int timeoutInSeconds - value specified in seconds to wait
	 * @Return: boolean - true: page loaded / false: page not loaded
	 */
	public boolean verifyMDPHomePageLoaded(int timeoutInSeconds) {

		boolean result = false;

		try {
			boolean flag1 = ElementActions.waitUntilElementDisplayedByXpath(MDPHomeIntroduction, timeoutInSeconds);
			if (!flag1) {
				throw new NoSuchElementException("");
			}

			Logging.info("MDP Home page loaded");

			// All core elements loaded.
			result = true;
		} catch (WebDriverException ex) {
			Logging.info("MDP Home page could not be confirmed as loaded");
			throw ex;
		}

		if (result) {
			Logging.info("MDP Home page loaded");
		}
		return result;
	}

}