package pages.accountmanagementconsole;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.sun.org.apache.regexp.internal.RE;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.login.LoginPage;
import pages.login.TopPage;
import utils.ElementActions;
import utils.PropertyFileReader;

public class CreateMessagePage {

    @FindBy(xpath = "")
    private WebElement createMessageHeader;

    @FindBy(xpath = "")
    private WebElement cancelOption;

    @FindBy(xpath = "")
    private WebElement previewButton;

    @FindBy(xpath = "")
    private WebElement audienceLabel;

    @FindBy(xpath = "")
    private WebElement selectAudienceDropdown;

    @FindBy(xpath = "")
    private WebElement messageTitleLabel;

    @FindBy(xpath = "")
    private WebElement titleTextField;

    @FindBy(xpath = "")
    private WebElement sendNowRadioButton;

    @FindBy(xpath = "")
    private WebElement scheduleRadioButton;

    @FindBy(xpath = "")
    private WebElement imageRadioButton;

    @FindBy(xpath = "")
    private WebElement imageLabel;

    @FindBy(xpath = "")
    private WebElement imageDropArea;

    @FindBy(xpath = "")
    private WebElement browseButton;

    @FindBy(xpath = "")
    private WebElement textRadioButton;

    @FindBy(xpath = "//textarea[@data-id='messageText']")
    private WebElement textInputField;

    @FindBy(xpath = "")
    private WebElement couponsAndPromotionsRadioButton;

    @FindBy(xpath = "")
    private WebElement surveyRadioButton;

    @FindBy(xpath = "")
    private WebElement richCardRadioButton;

    @FindBy(xpath = "")
    private WebElement carouselRadioButton;

    @FindBy(xpath = "")
    private WebElement saveDraftButton;

    @FindBy(xpath = "")
    private WebElement sendButton;

    ExtentTest test;
    public CreateMessagePage(WebDriver driver, ExtentTest extentTest){
        PageFactory.initElements(driver, this);
        test = extentTest;
    }

    public boolean createMessageHeaderDisplayed(){
        return ElementActions.elementDisplayed(createMessageHeader);
    }

    public boolean cancelOptionDisplayed(){
        return ElementActions.elementDisplayed(cancelOption);
    }

    public boolean previewButtonDisplayed(){
        return ElementActions.elementDisplayed(previewButton);
    }

    public boolean audienceLabelDisplayed(){
        return ElementActions.elementDisplayed(audienceLabel);
    }

    public boolean selectAudienceDropdownDisplayed(){
        return ElementActions.elementDisplayed(selectAudienceDropdown);
    }

    public boolean messageTitleLabelDisplayed(){
        return ElementActions.elementDisplayed(messageTitleLabel);
    }

    public boolean titleTextFieldDisplayed(){
        return ElementActions.elementDisplayed(titleTextField);
    }

    public boolean sendNowRadioButtonDisplayed(){
        return ElementActions.elementDisplayed(sendNowRadioButton);
    }

    public boolean scheduleRadioButtonDisplayed(){
        return ElementActions.elementDisplayed(scheduleRadioButton);
    }

    public boolean imageRadioButtonDisplayed(){
        return ElementActions.elementDisplayed(imageRadioButton);
    }

    public boolean textRadioButtonDisplayed(){
        return ElementActions.elementDisplayed(textRadioButton);
    }

    public boolean couponsAndPromotionsRadioButtonDisplayed(){
        return ElementActions.elementDisplayed(couponsAndPromotionsRadioButton);
    }

    public boolean surveyRadioButtonDisplayed(){
        return ElementActions.elementDisplayed(surveyRadioButton);
    }

    public boolean richCardRadioButtonDisplayed(){
        return ElementActions.elementDisplayed(richCardRadioButton);
    }

    public boolean carouselRadioButtonDisplayed(){
        return ElementActions.elementDisplayed(carouselRadioButton);
    }

    public boolean saveDraftButtonDisplayed(){
        return ElementActions.elementDisplayed(saveDraftButton);
    }

    public boolean sendButtonDisplayed(){
        return ElementActions.elementDisplayed(sendButton);
    }

    public boolean imageLabelDisplayed(){
        return ElementActions.elementDisplayed(imageLabel);
    }

    public boolean imageDropAreaDisplayed(){
        return ElementActions.elementDisplayed(imageDropArea);
    }

    public boolean browseButtonDisplayed(){
        return ElementActions.elementDisplayed(browseButton);
    }

    public boolean clickImageRadioButton(){
        return ElementActions.click(imageRadioButton);
    }

    public boolean clickTextRadioButton(){
        return ElementActions.click(textRadioButton);
    }

    public boolean clickCouponsAndPromotionsRadioButton(){
        return ElementActions.click(couponsAndPromotionsRadioButton);
    }

    public boolean clickSurveyRadioButton(){
        return ElementActions.click(surveyRadioButton);
    }

    public boolean clickRichCardRadioButton(){
        return ElementActions.click(richCardRadioButton);
    }

    public boolean clickCarouselRadioButton(){
        return ElementActions.click(carouselRadioButton);
    }

    public boolean enterTextMessage(String message,ExtentTest test){
        return ElementActions.sendKeys(textInputField,message);
    }

    public String getMessageText(){
        return textInputField.getAttribute("value");
    }

    public void navigateToCreateMessagePage(WebDriver driver){
        String propertyFileName = "global.properties";
        String userName = PropertyFileReader.getProperty(propertyFileName,"qa.login.username");
        String password = PropertyFileReader.getProperty(propertyFileName,"qa.login.password");

        LoginPage loginPage = new LoginPage(driver, test);
        loginPage.userLogin(userName,password);
        test.log(LogStatus.INFO,"Logged in to Application");

        TopPage topPage = new TopPage(driver, test);
        if(topPage.clickSelectButton()){
            test.log(LogStatus.INFO,"Selected one account on Top Page");
        }else{
            test.log(LogStatus.INFO,"Cannot Select account on Top Page");
        }


        AMCHomePage amcHomePage = new AMCHomePage(driver, test);
        amcHomePage.clickManageContent();
        test.log(LogStatus.INFO,"Clicked Manage Content button on AMC home page");

        ManageContentPage manageContentPage = new ManageContentPage(driver);
        manageContentPage.clickCreateMessage();
        test.log(LogStatus.INFO,"Clicked on Create Message button on Manage content page");

    }


}
