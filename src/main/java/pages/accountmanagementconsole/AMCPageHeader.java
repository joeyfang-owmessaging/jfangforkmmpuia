package pages.accountmanagementconsole;

import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.ElementActions;

public class AMCPageHeader {

    @FindBy(xpath = "//img[@id='logo-Synchronoss}']")
    private WebElement synchronossLogo;

    @FindBy(xpath = "//h3[@data-id='content-header-title' and .='Account Management Console']")
    private WebElement header;

    @FindBy(xpath = "//li[@data-id='content-header-unique-id']")
    private WebElement uniqueID;

    @FindBy(xpath = "//li[@data-id='content-header-subscribers']/mat-icon")
    private WebElement subscriberIcon;

    @FindBy(xpath = "//li[@data-id='content-header-subscribers']")
    private WebElement subscriberCount;

    @FindBy(xpath = "//li[@data-id='content-header-plan']")
    private WebElement currentPlan;

    @FindBy(xpath = "//li[@data-id='content-header-upgrade']")
    private WebElement upgradeLink;

    @FindBy(xpath = "//a[@data-id='user-toolbar-notifications']//mat-icon")
    private WebElement notificationIcon;

    @FindBy(xpath = "//a[@data-id='user-toolbar-settings']//mat-icon")
    private WebElement settingsIcon;

    @FindBy(xpath = "//li[@data-id='user-toolbar-logout']//mat-icon")
    private WebElement logoutIcon;

    ExtentTest test;

    public AMCPageHeader(WebDriver driver, ExtentTest extentTest){
        PageFactory.initElements(driver,this);
        test = extentTest;
    }


    public Boolean synchronossLogoDisplayed(){
        return ElementActions.elementDisplayed(synchronossLogo);
    }

    public Boolean headerDisplayed(){
        return ElementActions.elementDisplayed(header);
    }

    public Boolean uniqueIDDisplayed(){
        return ElementActions.elementDisplayed(uniqueID);
    }

    public Boolean subscriberIconDisplayed(){
        return ElementActions.elementDisplayed(subscriberIcon);
    }

    public String getSubscriberCount(){
        return subscriberCount.getText();
    }

    public String getCurrentPlan(){
        return currentPlan.getText();
    }

    public Boolean upgradeLinkDisplayed(){
        return ElementActions.elementDisplayed(upgradeLink);
    }

    public Boolean notificationIconDisplayed(){
        return ElementActions.elementDisplayed(notificationIcon);
    }

    public Boolean settingsIconDisplayed(){
        return ElementActions.elementDisplayed(settingsIcon);
    }

    public Boolean logoutIconDisplayed(){
        return ElementActions.elementDisplayed(logoutIcon);
    }

    public boolean clickLogoutIcon(){
         return ElementActions.click(logoutIcon);
    }



}
