package pages.accountmanagementconsole;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.ElementActions;

public class ManageContentPage {

    @FindBy(xpath = "")
    private WebElement createMessageButton;

    public ManageContentPage(WebDriver driver){
        PageFactory.initElements(driver,this);
    }

    public boolean clickCreateMessage(){
        return ElementActions.click(createMessageButton);
    }


}
