package pages.accountmanagementconsole;

import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.ElementActions;

public class AMCPageNavigationMenu {

    @FindBy(xpath = "//a[@data-id='nav-link-dashboard']")
    private WebElement dashboardMenu;

    @FindBy(xpath = "//a[@data-id='nav-link-manage-dashboard']")
    private WebElement manageMenu;

    @FindBy(xpath = "//a[@data-id='nav-link-sub-manage-profile']")
    private WebElement profilePageMenu;

    @FindBy(xpath = "//a[@data-id='nav-link-sub-manage-content']")
    private WebElement contentMenu;

    @FindBy(xpath = "//a[@data-id='nav-link-sub-manage-chatbot']")
    private WebElement chatBotMenu;

    @FindBy(xpath = "nav-link-analyze")
    private WebElement analyzeMenu;

    ExtentTest test;
    public AMCPageNavigationMenu(WebDriver driver, ExtentTest extentTest){
        PageFactory.initElements(driver,this);
        test = extentTest;
    }

    public boolean clickDashboardMenu(){
        return ElementActions.click(dashboardMenu);
    }
    public boolean clickManageMenu(){
        return ElementActions.click(manageMenu);
    }
    public boolean clickProfilePageMenu(){
        return ElementActions.click(profilePageMenu);
    }
    public boolean clickContentMenu(){
        return ElementActions.click(contentMenu);
    }
    public boolean clickChatBotMenu(){
        return ElementActions.click(chatBotMenu);
    }
    public boolean clickAnalyzeMenu(){
        return ElementActions.click(analyzeMenu);
    }
}
