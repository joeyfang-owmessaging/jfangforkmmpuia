package pages.accountmanagementconsole;

import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.ElementActions;

public class AMCHomePage {

    @FindBy(xpath = "//a[.='Get started']")
    private WebElement getStartedButton;

    @FindBy(xpath = "//a[.='Manage Content']")
    private WebElement manageContentButton;

    @FindBy(xpath = "//a[.='Manage Bots']")
    private WebElement manageBotsButton;

    ExtentTest test;

    public AMCHomePage(WebDriver driver, ExtentTest extentTest){
        PageFactory.initElements(driver,this);
        test = extentTest;
    }

    public boolean clickGetStartedButton(){
        return ElementActions.click(getStartedButton);
    }

    public boolean clickManageContent(){
        return ElementActions.click(manageContentButton);
    }

    public boolean clickManageBotsButton(){
        return ElementActions.click(manageBotsButton);
    }

}
